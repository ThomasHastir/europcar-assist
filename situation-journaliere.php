<?PHP
session_name('SESSION1');
session_start();

include('inc/dictionnary.php');
?>
<?PHP
if ($_SESSION['connected'] == 1) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<?PHP if ($_SESSION['zeType'] == 'assistant' || $_SESSION['zeType'] == 'admin') { ?><li><a href="/"><?=$dic_chercherdepanneur?></a></li><?PHP }else{ ?><li><a href="/"><?=$dic_accueil?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] != 'assistant') { ?><li><a href="mdp.php"><?=$dic_modifierpwd?></a></li><?PHP }; ?>
		<li><a href="contrats.php"><?=$prolonger?></a></li>
		<?PHP if ($_SESSION['zeType'] == 'depanneur' || $_SESSION['zeType'] == 'admin') { ?><li><a href="search-contract.php"><?=$dic_print_contrat_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'depanneur') { ?><li><a href="situation-journaliere.php"><?=$dic_daily_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'admin') { ?><li><a href="imatlist.php"><?=$dic_imatlist?></a></li><?PHP }?>
		<?PHP	
		
		}
		?>
		<li><a href="mailto:BE-h24@europcar.com"><?=$dic_contacteznous?></a></li>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<li><a href="logout.php"><?=$dic_sedeconnecter?></a></li>
		<?PHP	
		}
		?>
	</ul>
</div>
<div id="container">
<div id="content">
<h1><?=$dic_daily_title?></h1>
<?PHP


	
if (!empty($_POST)) {
	

	
	
	require 'inc/phpmailer/PHPMailerAutoload.php';
	
	
	include('inc/connexion-pdo.php');
	
	 try {
	    $db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$statement = $db->prepare("SELECT `dep_code`, `dep_nom` FROM `assist_depaneurs` WHERE `id` = ".$_SESSION['myid']);
		$statement->execute();
		
		
		
	    foreach($statement as $row) {
		    
		 
		  $subject = '[ASSIST] '.$row['dep_code'].' - Dépannage '.$row['dep_nom'].' - Situation du '.date('d/m/Y');
 
	    }
	    	$db = null;
		} catch (PDOException $e) {
		    print "Erreur !: " . $e->getMessage() . "<br/>";
		    die();
		}

 	$message = '';

	$search = array('dispo', 'shop_', 'shop', 'vente_', 'vente', 'route');
	$replace = array('Véhicules disponibles', 'Véhicule SHOP n°', 'Véhicules SHOP', 'Véhicule VENTE n°', 'Véhicules VENTE', 'Véhicules sur la route');

	foreach ($_POST as $key => $value) {
		if ($value >= 0 && $key != 'send') {
			$message .= str_replace($search, $replace, $key).' : '.$value.'<br>';
			}
		}
	
	
	
	$mail = new PHPMailer;
	
	$mail->Encoding = 'base64';
	$mail->CharSet = 'UTF-8';
	
	
	$mail->From = 'europcar@mediaa.be';
	$mail->FromName = 'Europcar Assist';
	
	$mail->Sender = 'europcar@mediaa.be';
	
	$mail->addReplyTo('BE-h24@europcar.com');
	$mail->addAddress('BE-h24@europcar.com');
	
	
	
	$mail->isHTML(true);
	
	
	
	$mail->Subject = $subject;
	$mail->Body    = $message;
	$mail->AltBody = strip_tags($message);
	
	
	
	if(!$mail->send()) {
	    echo '<p>Message could not be sent.</p>';
	    echo '<p>Mailer Error: ' . $mail->ErrorInfo.'</p>';
	}else{
		echo '<p>'.$dic_daily_confirm.'</p>';
	}
	
	$mail->clearAllRecipients();
	$mail->clearReplyTos();
	
	
	

	

	
	
	
	

}else{


?>
<form name="form1" id="form1" method="post" action="situation-journaliere.php">
	<fieldset>
		<p>
			<label for="" class="cellLike" style="width:300px;"><?=$dic_daily_q1?></label>
			<select name="dispo" id="dispo">
				<option></option>
				<?php for ($i=0;$i<25;$i++) { ?>
				<option value="<?=$i?>"><?=$i?></option>
				<?php } ?>
			</select>
		</p>
	</fieldset>
	<fieldset>
		<p>
			<label for="" class="cellLike" style="width:300px;"><?=$dic_daily_q2?></label>
			<select name="shop" id="shop">
				<option></option>
				<?php for ($i=0;$i<25;$i++) { ?>
				<option value="<?=$i?>"><?=$i?></option>
				<?php } ?>
			</select>
		</p>
	</fieldset>
	<fieldset>
		<p>
			<label for="" class="cellLike" style="width:300px;"><?=$dic_daily_q3?></label>
			<select name="vente" id="vente">
				<option></option>
				<?php for ($i=0;$i<25;$i++) { ?>
				<option value="<?=$i?>"><?=$i?></option>
				<?php } ?>
			</select>
		</p>
	</fieldset>
	<fieldset>
		<p>
			<label for="" class="cellLike" style="width:300px;"><?=$dic_daily_q4?></label>
			<select name="route" id="route">
				<option></option>
				<?php for ($i=0;$i<25;$i++) { ?>
				<option value="<?=$i?>"><?=$i?></option>
				<?php } ?>
			</select>
		</p>
	</fieldset>
	<input type="button" name="send" id="send" value="<?=$dic_daily_send?>" onclick="sendForm()" />
</form>
<?PHP
}
?>
<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
<script>
	
	

	
	$(document).on('change', '#shop, #vente', function() {
		
		$(this).parent().find('div').remove();
		
		if ($(this).val() > 0) {	
			$(this).parent().append('<div><p><?=$dic_daily_veuillez?></p></div>');
			
			for (i = 1; i <= $(this).val(); i++) {
				$(this).parent().find('div').append('<p><?=$dic_daily_vehicule?> ' + i + ' : <input type="text" name="' + $(this).attr('id') + '_' + i + '" id="" class="immatvalidation" style="text-transform:uppercase;" /></p>');
			}
			$(".immatvalidation").mask("9-aaa-999");
		}

	});
	
	
	
function sendForm() {
	
	var error = 0;
	var errorMsg = '';

	
	if ($('#dispo').val() == '') {
		error = 1;
		errorMsg += "<?=$dic_daily_err1?>\r\n";
		}
	if ($('#shop').val() == '') {
		error = 1;
		errorMsg += "<?=$dic_daily_err2?>\r\n";
		}
	if ($('#vente').val() == '') {
		error = 1;
		errorMsg += "<?=$dic_daily_err3?>\r\n";
		}
	if ($('#route').val() == '') {
		error = 1;
		errorMsg += "<?=$dic_daily_err4?>\r\n";
		}
	
	


	//console.log(error);
	if (error == 0) {
		document.form1.submit();
	}else{
		alert(errorMsg);
	}
}
	
	
</script>
</body>
</html>
<?PHP	
}
?>