<?PHP
session_name('SESSION1');
session_start();

$secretKey = 'h0olhe4D!#sal';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	
		
		$hash = md5($_POST['id'].$secretKey);
		if ($_POST['hashgdpr'] != $hash) {
			echo 'ERROR';
			exit();
		}
		
	}else{
		
		
		$hash = md5($_GET['id'].$secretKey);
		if ($_GET['hashgdpr'] != $hash) {
			echo 'ERROR';
			exit();
		}
		
	}

include('inc/dictionnary.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
<script src="../js/parsley.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="#">Welcome</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
<h1><?=_('Mise à jour de votre mot de passe requise.')?></h1>
<h2><?=_("Cher utilisateur,<br><br>Veuillez modifier dès maintenant votre mot de passe d'accès afin de pouvoir continuer à utiliser notre plateforme en toute sécurité :")?></h2>
<?PHP
if(isset($_POST['mdp_send']) && ($_POST['mdp_new1'] == $_POST['mdp_new2'])) {
	
	include('inc/connexion.php');
	
	$sql = "UPDATE assist_depaneurs";
	$sql .= " SET dep_pwd='".md5($_POST['mdp_new1'])."'";
	$sql .= ", gdpr_pwd_check = NOW()";
	$sql .= " WHERE id=".$_POST['id']."";
	
	//echo $sql;

	
	$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	mysql_close($link);

	echo '<p>', _("Votre mot de passe a été modifié avec succès. Vous pouvez maintenant vous connecter à la plateforme en utilisant ce nouveau mot de passe. <a href=\"/\">Cliquez ici</a> pour vous connecter."), '</p>';
	

}else{
?>

	<?PHP
	if (isset($_POST['mdp_send']) && ($_POST['mdp_new1'] != $_POST['mdp_new2'])) {
	?>
	<p><?=$dic_erreurpwd?></p>
	<?PHP
	}
	?>
<form name="form1" data-parsley-validate id="form1" method="post" action="gdpr_pwd_check.php">
<fieldset>
	<legend><?=$dic_choosenewpwd?></legend>
	<p><label for="mdp_new1" class="cellLike" style="width:200px;"><?=$dic_votrenouveaumdp?> </label> <input type="password" name="mdp_new1" id="mdp_new1" required="required" data-parsley-minlength="8" data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1" data-parsley-special="1" value="" /></p>
	<p><label for="mdp_new2" class="cellLike" style="width:200px;"><?=$dic_retapezmdp?> </label> <input type="password" name="mdp_new2" id="mdp_new2" required="required" data-parsley-equalto="#mdp_new1" data-parsley-validate-if-empty="true" value="" /></p>
</fieldset>
<input type="hidden" name="hashgdpr" value="<?=$_GET['hashgdpr']?>">
<input type="hidden" name="id" value="<?=$_GET['id']?>">
<input type="submit" name="mdp_send" id="mdp_send" value="<?=$dic_modifier?>" />
</form>
<?PHP
}
?>
<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
<!-- Parsley -->
	    <script>
	
	      $(document).ready(function() {
		      
		      //has uppercase
window.Parsley.addValidator('uppercase', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var uppercases = value.match(/[A-Z]/g) || [];
    return uppercases.length >= requirement;
  },
  messages: {
    en: '<?=_("Votre mot de passe doit contenir au minimum 1 lettre majuscule.")?>'
  }
});

//has lowercase
window.Parsley.addValidator('lowercase', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var lowecases = value.match(/[a-z]/g) || [];
    return lowecases.length >= requirement;
  },
  messages: {
    en: '<?=_("Votre mot de passe doit contenir au minimum 1 lettre minuscule.")?>'
  }
});

//has number
window.Parsley.addValidator('number', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var numbers = value.match(/[0-9]/g) || [];
    return numbers.length >= requirement;
  },
  messages: {
    en: '<?=_("Votre mot de passe doit contenir au minimum 1 chiffre.")?>'
  }
});

//has special char
window.Parsley.addValidator('special', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var specials = value.match(/[^a-zA-Z0-9]/g) || [];
    return specials.length >= requirement;
  },
  messages: {
    en: '<?=_("Votre mot de passe doit contenir au minimum 1 caractère spécial ($ ! %, etc..).")?>'
  }
});
		      

		      
	        window.Parsley.on('parsley:field:validate', function() {
	          validateFront();
	        });
	        $('#form1 input[type="submit"]').on('click', function() {
	          $('#form1').parsley().validate();
	          validateFront();
	        });
	        var validateFront = function() {
	          if (true === $('#form1').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');
	          } else {
	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');
	          }
	        };
	        
	        
	        
	        
	  
	        
	        
	        
	      });
	      try {
	        hljs.initHighlightingOnLoad();
	      } catch (err) {}
	    </script>
	    <!-- /Parsley -->
</body>
</html>