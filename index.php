<?PHP
session_name('SESSION1');
session_start();

if ($_GET['autoconnect']) {
	
	$_SESSION['connected'] = 0;
	
	// if autoconnect from admin then disconnect current session to prefill form
	session_destroy();
		
	setcookie("rememberme", 0, time()-3600);
	setcookie("login_usr", "", time()-3600);
	setcookie("login_pwd", "", time()-3600);
	setcookie("login_who", "", time()-3600);
	setcookie("login_lg", "", time()-3600);
	
	//header('Location: /?autoconnect=1&dep_assist='.$_GET['dep_assist'].'&id='.$_GET['id'].'&hash='.$_GET['hash']);
	
	
	}


include('inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}	

include('inc/functions.php');
include('inc/dictionnary.php');

$errorMsg = "";

if (isset($_POST['submit']) or $_COOKIE['rememberme'] == 1) {
	
	if (isset($_POST['submit'])) {
		$_SESSION['zeLogin'] = $_POST['login_usr'];
		if (isset($_POST['autoconnect'])) {
			$_SESSION['zePwd'] = $_POST['login_pwd'];
		}else{
			$_SESSION['zePwd'] = md5($_POST['login_pwd']);
		}
	}else{
		$_SESSION['zeLogin'] = $_COOKIE['login_usr'];
		$_SESSION['zePwd'] = $_COOKIE['login_pwd'];
	}
	

		
	$statement = $db->prepare("SELECT * FROM `assist_depaneurs` WHERE `dep_mail` = :dep_mail AND `dep_pwd` = :dep_pwd AND `dep_assist` != 3;");
	$statement->execute(array('dep_mail' => $_SESSION['zeLogin'], 'dep_pwd' => $_SESSION['zePwd']));
	
	if ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
		
			// temp: Forcer le changement du password pour GDPR
			if ($row['gdpr_pwd_check'] == '0000-00-00 00:00:00') {
				
				$secretKeyGDPR = 'h0olhe4D!#sal';
				$hashGDPR = md5($row['id'].$secretKeyGDPR);
				
				header('Location: /gdpr_pwd_check.php?hashgdpr='.$hashGDPR.'&id='.$row['id']);
				exit();
			}
			
			$_SESSION['connected'] = 1;
			$_SESSION['myid'] = $row['id'];
			
			switch ($row['dep_assist']) {
			case 0:
				$_SESSION['zeType'] = 'depanneur';
				break;
			case 1:
				$_SESSION['zeType'] = 'assistant';
				break;
			case 2:
				$_SESSION['zeType'] = 'admin';
				break;
			}
			
			// Remember me
			if (isset($_POST['submit'])) {
				if ($_POST['rememberme'] == 1) {
					setcookie("rememberme", 1, time()+60*60*24*30);
					setcookie("login_usr", $_POST['login_usr'], time()+60*60*24*30);
					setcookie("login_pwd", md5($_POST['login_pwd']), time()+60*60*24*30);
				}else{
					setcookie("rememberme", "");
				}
			}
			
	}else{
		$_SESSION['connected'] = 0;
		session_destroy();
		setcookie("rememberme", "");
		$errorMsg = $dic_loginerror;
	}
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<meta name="robots" content="noindex,nofollow" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<?PHP if ($_SESSION['connected'] == 1) { ?>
<script type="text/javascript" src="js/send.js"></script>
<?php } ?>
<script type="text/javascript">

function showDossier(num_dossier) {
	$("#"+num_dossier).show();
}
function showBuyback(buyback) {
	$("#"+buyback).show();
}

function form_validate() {
	$error = 0;
	
	if ($('fieldset input:text').val() == '') {
		$('fieldset input:text').parent().css({ backgroundColor: "#fef0ef", border:"2px solid #ca3d38" });
		$error = 1;
	}else{
		$('fieldset input:text').parent().css({ backgroundColor: "#fff", border:"none" });
		$error = 0;
	}
	if ($('fieldset input:password').val() == '') {
		$('fieldset input:password').parent().css({ backgroundColor: "#fef0ef", border:"2px solid #ca3d38" });
		$error = 1;
	}else{
		$('fieldset input:password').parent().css({ backgroundColor: "#fff", border:"none" });
		$error = 0;
	}
	
	
	if ($error == 0) {
		document.forms['login_form'].submit();
	}
}

<?PHP
if (strlen($errorMsg) > 0 ) {
	echo 'alert("', $errorMsg, '");';
	?>
	$(document).ready(function() {
 $('fieldset input:text').parent().css({ backgroundColor: "#fef0ef", border:"2px solid #ca3d38" });
	$('fieldset input:password').parent().css({ backgroundColor: "#fef0ef", border:"2px solid #ca3d38" });
});
	
	<?PHP
}
?>
</script>
</head>
<body>
<div id="header">
	<?PHP
	if ($_SESSION['connected'] == 0) {
		echo $dic_switchlg;
	}
	?>
		
	<ul>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<?PHP if ($_SESSION['zeType'] == 'assistant' || $_SESSION['zeType'] == 'admin') { ?><li><a href="/"><?=$dic_chercherdepanneur?></a></li><?PHP }else{ ?><li><a href="/"><?=$dic_accueil?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] != 'assistant') { ?><li><a href="mdp.php"><?=$dic_modifierpwd?></a></li><?PHP }; ?>
		<li><a href="contrats.php"><?=$prolonger?></a></li>
		<?PHP if ($_SESSION['zeType'] == 'depanneur' || $_SESSION['zeType'] == 'admin') { ?><li><a href="search-contract.php"><?=$dic_print_contrat_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'depanneur') { ?><li><a href="situation-journaliere.php"><?=$dic_daily_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'admin') { ?><li><a href="imatlist.php"><?=$dic_imatlist?></a></li><?PHP }?>
		<?PHP	
		
		}
		?>
		<?php if ($_SESSION['zeType'] != 'admin') { ?><li><a href="mailto:BE-h24@europcar.com"><?=$dic_contacteznous?></a></li><?php } ?>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<li><a href="logout.php"><?=$dic_sedeconnecter?></a></li>
		<?PHP	
		}
		?>
	</ul>
</div>
<div id="container">
<div id="content">
<?PHP
if ($_SESSION['connected'] == 1) {

/*
----------------
A chaque connexion sur le site, on update tous les véhicules bloqué depuis plus de XXh
----------------
*/

include('inc/connexion.php');
$sql = "SELECT id, bondecommande, contrat FROM assist_cars WHERE ADDTIME(car_date, car_block_time) < NOW() AND car_status = 1;";
$result = mysql_query($sql) 
	or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	
$IDdebloqueAuto = "";
$deleteBDC = "";
$deleteCTR = "";

while ($row = mysql_fetch_array($result)) {
	$IDdebloqueAuto .= " OR id = ".$row['id'];
	
	$deleteBDC .= " OR bdc_id = '".$row['bondecommande']."'";
	$deleteCTR .= " OR id = ".$row['contrat'];
	
}

$IDdebloqueAuto = substr($IDdebloqueAuto, 3);
$deleteBDC = substr($deleteBDC, 3);
$deleteCTR = substr($deleteCTR, 3);

if (strlen($IDdebloqueAuto) > 0) {

	$sql = "UPDATE assist_cars SET car_status = 4, car_date = NOW(), bondecommande = NULL, contrat = NULL, car_dossier = NULL, car_assistance = NULL, car_date_out = NULL WHERE ".$IDdebloqueAuto.";";
	$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		
	// On supprime le BDC correspondant (nettoyage DB)
	$sql = "DELETE FROM `bondecommande` WHERE ".$deleteBDC.";";
	$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		
	// On supprime le contrat correspondant (nettoyage DB)
	$sql = "DELETE FROM `contrats` WHERE ".$deleteCTR.";";
	$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	
	$sql = "INSERT INTO assist_queries (car_immatriculation, car_date, car_assistance, car_dossier, car_status, car_depaneur) ";
	$sql .= "SELECT car_immatriculation, car_date, car_assistance, car_dossier, car_status, car_depaneur ";
	$sql .= "FROM assist_cars ";
	$sql .= "WHERE ".$IDdebloqueAuto.";";
	$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		
		
}

mysql_close($link);



//echo '<p>Vous êtes connecté avec succès en ', $lgstring, '</p>';

?>
<?PHP
switch ($_SESSION['zeType']) {
	case 'depanneur':
		// Pour les dépanneurs on va directement sur la fiche détail
		include('assistance_depanneur.php');
		break;
	case 'assistant':
	case 'admin':
		// Pour les assistants & admins, on passe d'abord via l'outil de recherche de dépanneurs.
		include('assistance.php');
		break;
	}
?>



<?PHP
} 

else if (isset($_SESSION['kill_id'])) {
?>
<h1><?=$dic_welcome?></h1>
<p><?=$dic_veuillezcompleterkill?></p>
<form name="login_form" id="login_form" action="kill.php?id=<?=$_SESSION['kill_id']?>" method="post" onsubmit="form_validate(this);return false;">
<fieldset>
	<legend><?=$dic_mesidentifiants?></legend>
	<p><label for="login_usr" class="cellLike"><?=$dic_email?></label> <input type="text" name="login_usr" id="login_usr" value="<?PHP if(isset($_POST['login_usr'])) { ?><?=$_POST['login_usr']?><?PHP } ?>" /></p>
	<p><label for="login_pwd" class="cellLike"><?=$dic_pwd?></label> <input type="password" name="login_pwd" id="login_pwd" value="<?PHP if(isset($_POST['login_pwd'])) { ?><?=$_POST['login_pwd']?><?PHP } ?>" /></p>
</fieldset>
<fieldset>
	<legend><?=$dic_mesoptions?></legend>
	<p><input type="checkbox" name="rememberme" id="rememberme" value="1" /> <label for="rememberme"><?=$dic_gardermasession?></label></p>
</fieldset>
<p>
	<input type="submit" name="submit" value="<?=$dic_seconnecter?>" class="login_btn" />
</p>
</form>
<?PHP
}

else if (isset($_SESSION['contrat_id'])) {
?>
<h1><?=$dic_welcome?></h1>
<p><?=$dic_veuillezcompletercontrat?></p>
<form name="login_form" id="login_form" action="contrat.php?contratid=<?=$_SESSION['contrat_id']?>" method="post" onsubmit="form_validate(this);return false;">
<fieldset>
	<legend><?=$dic_mesidentifiants?></legend>
	<p><label for="login_usr" class="cellLike"><?=$dic_email?></label> <input type="text" name="login_usr" id="login_usr" value="<?PHP if(isset($_POST['login_usr'])) { ?><?=$_POST['login_usr']?><?PHP } ?>" /></p>
	<p><label for="login_pwd" class="cellLike"><?=$dic_pwd?></label> <input type="password" name="login_pwd" id="login_pwd" value="<?PHP if(isset($_POST['login_pwd'])) { ?><?=$_POST['login_pwd']?><?PHP } ?>" /></p>
</fieldset>
<fieldset>
	<legend><?=$dic_mesoptions?></legend>
	<p><input type="checkbox" name="rememberme" id="rememberme" value="1" /> <label for="rememberme"><?=$dic_gardermasession?></label></p>
</fieldset>
<p>
	<input type="submit" name="submit" value="<?=$dic_seconnecter?>" class="login_btn" />
</p>
</form>
<?PHP
}

else {
	
	// auto connect from admin
	if ($_GET['autoconnect']) {
		
		// check hash
		$secretKey = 'h0olhe4D!#sal';
		$hash = md5($_GET['dep_assist'].$_GET['id'].$secretKey);
		if ($_GET['hash'] == $hash) {
			// Hash OK, we can retrieve login and password from DB to pre-fill connexion form
			
			$statement = $db->prepare("SELECT `dep_mail`, `dep_pwd` FROM `assist_depaneurs` WHERE `id`  = :id");
			$statement->execute(array('id' => $_GET['id']));
			
			$row = $statement->fetch(PDO::FETCH_ASSOC);
			$dep_mail = $row['dep_mail'];
			$dep_pwd = $row['dep_pwd'];
		}
		
		
	}
	
?>
<h1><?=$dic_welcome?></h1>
<p><?=$dic_veuillezcompleter?></p>
<form name="login_form" id="login_form" action="index.php" method="post" onsubmit="form_validate(this);return false;">
<fieldset>
	<legend><?=$dic_mesidentifiants?></legend>
	<p><label for="login_usr" class="cellLike"><?=$dic_email?></label> <input type="text" name="login_usr" id="login_usr" value="<?php if ($dep_mail) { echo $dep_mail; }else{ ?><?PHP if(isset($_POST['login_usr'])) { ?><?=$_POST['login_usr']?><?PHP } ?><?php } ?>" /></p>
	<p><label for="login_pwd" class="cellLike"><?=$dic_pwd?></label> <input type="password" name="login_pwd" id="login_pwd" value="<?php if ($dep_pwd) { echo $dep_pwd; }else{ ?><?PHP if(isset($_POST['login_pwd'])) { ?><?=$_POST['login_pwd']?><?PHP } ?><?php } ?>" /></p>
</fieldset>
<?php if (!$_GET['autoconnect']) { ?>
<fieldset>
	<legend><?=$dic_mesoptions?></legend>
	<p><input type="checkbox" name="rememberme" id="rememberme" value="1" /> <label for="rememberme"><?=$dic_gardermasession?></label></p>
</fieldset>
<?php }else{ ?>
	<input type="hidden" name="autoconnect" id="autoconnect" value="1">
	
<?php 	} ?>
<p>
	<input type="submit" name="submit" value="<?=$dic_seconnecter?>" class="login_btn" />
</p>
</form>
<?PHP
};
?>
<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
<?php $statement = null; $db = null; ?>
</body>
</html>