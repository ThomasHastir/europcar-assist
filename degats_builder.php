<?php
session_name('SESSION1');
session_start();

include('inc/dictionnary.php');	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>

</head>
<body>
<select id="step1" style="width:100%;">
	<option><?=$dic_selectionnez?></option>
<?php
include('inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

$statement = $db->prepare("SELECT `degats_areas_1`.`dam`, `degats_areas_1`.`dam_$lgstring` AS `dam_lgstring` FROM `degats_areas_1` ORDER BY `degats_areas_1`.`dam_$lgstring` ASC;");
$statement->execute();



foreach($statement as $row) {
	echo '	<option value="'.$row['dam'].'">'.ucFirst($row['dam_lgstring']).' ['.$row['dam'].']</option>'."\n";
}
?>
</select>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$( document ).on('change', '#step1', function() {
	var degat = $(this).val();
	var thisSelect = $(this);
	$.ajax({
		method: "GET",
		url: "/ajax/degats_builder.php",
		dataType: "html",
		data: { degat : degat, step: 1, language: '<?=$lgstring?>' },
		success: function (responseText) {
			if($("#step2").length == 0) {
				thisSelect.after(responseText);
			}else{
				$("#step2").replaceWith(responseText);
			}
		}
	});
});

$( document ).on('change', '#step2', function() {
	var thisSelect = $(this);
	$.ajax({
		method: "GET",
		url: "/ajax/degats_builder.php",
		dataType: "html",
		data: { step: 2, language: '<?=$lgstring?>' },
		success: function (responseText) {
			if($("#step3").length == 0) {
				thisSelect.after(responseText);
			}else{
				$("#step3").replaceWith(responseText);
			}
		}
	});
});

$( document ).on('change', '#step3', function() {
	var thisSelect = $(this);
	if($("#step4").length == 0) {
			thisSelect.after('<input type="button" value="OK" id="step4">');
		}else{
			$("#step4").replaceWith('<input type="button" value="OK" id="step4">');
		}
});

$( document ).delegate('#step4', 'click', function() {
	
	$(this).remove();
	$('.featherlight-close', window.parent.document).click();
	
	
	<?php
		if($_GET['s'] == 'back') {
			$destination = 'damage_back2';
		}else{
			$destination = 'damage_start';
		}
		
	?>

	$("#<?=$destination?>", window.parent.document).append($('#step1 option:selected').text() + ' - ' + $('#step2 option:selected').text() + ' - ' + $('#step3 option:selected').text() + '\n');
});

</script>
</body>
</html>
<?php $statement = null; $db = null; ?>