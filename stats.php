<?php
session_name('SESSION1');
session_start();

include('inc/dictionnary.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<meta name="robots" content="noindex,nofollow" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script type="text/javascript" src="js/send.js"></script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript">
function showDossier(num_dossier) {
   $("#"+num_dossier).show();
}
</script>
</head>
<body>
<div id="header">
   <?PHP
   if ($_SESSION['connected'] == 0) {
      echo $dic_switchlg;
   }
   ?>
      
   <ul>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<?PHP if ($_SESSION['zeType'] == 'assistant' || $_SESSION['zeType'] == 'admin') { ?><li><a href="/"><?=$dic_chercherdepanneur?></a></li><?PHP }else{ ?><li><a href="/"><?=$dic_accueil?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] != 'assistant') { ?><li><a href="mdp.php"><?=$dic_modifierpwd?></a></li><?PHP }; ?>
		<li><a href="contrats.php">Chercher contrat</a></li>
		<?PHP if ($_SESSION['zeType'] == 'admin') { ?><li><a href="imatlist.php"><?=$dic_imatlist?></a></li><?PHP }?>
		<?PHP	
		
		}
		?>
		<li><a href="mailto:BE-h24@europcar.com"><?=$dic_contacteznous?></a></li>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<li><a href="logout.php"><?=$dic_sedeconnecter?></a></li>
		<?PHP	
		}
		?>
	</ul>
</div>
<div id="container">
<div id="content">

<?php
include('inc/connexion.php');
$count = 0;
$now = strtotime(date("Y-m-d H:i:s"));
//echo $now;
echo "";
	try 
	{
		$data = $conn->query ("SELECT * FROM assist_depaneurs WHERE dep_assist = 0");
	}
  	catch(PDOException $e)
	{
		// echo $sql . "<br>" . $e->getMessage();
	}

	foreach ($data as $dep)
	{
		//$message .= "Dépaneur : ".$dep['dep_nom']. "\r\n";
		try 
		{
			$data2 = $conn->query ("SELECT * FROM assist_cars WHERE car_depaneur = '{$dep['id']}' AND (car_status = 0 OR car_status = 2)");
		}
	  	catch(PDOException $e)
		{
			// echo $sql . "<br>" . $e->getMessage();
		}
		$imats = "";
		$imatcount = 0;
		$car_imat = array();
		$car_acriss = array();
		$car_id = array();
		foreach ($data2 as $car)
		{
		 //echo "Dépaneur : ".$dep['dep_nom'];
		 $car_imat[] = $car['car_immatriculation'];
		 $car_acriss[] = $car['car_acriss'];
		 $car_id[] = $car['id'];
		 $car_date_in[] = strtotime($car['car_date_in']);
		 $car_date_out[] = strtotime($car['car_date_out']);
		 $count++;
		 $imatcount++;
		}
		if ($imatcount > 0)
		{
			$out_duration = 0;
			$outcounter = 0;
			$in_duration = 0;
			$incounter = 0;
			echo "<h1>".$dep['dep_nom']."</h1>";
			foreach ($car_imat as $imat)
			{
				if ($car_date_out[$counter] > 0 && $car_date_in[$counter] > 0)
				{
					$in_duration += ($car_date_out[$counter] - $car_date_in[$counter]);
					$incounter++;
				}
				else if ($car_date_in[$counter] > 0)
				{
					$in_duration += ($now - $car_date_in[$counter]);
					$incounter++;
				}
				$counter++;
			}
			if($in_duration >0)
			{
				$in_average = ($in_duration / $imatcount)/86400;
				echo "average IN : ".intval($in_average)."<br>";
			}
			
			echo $imatcount." véhicules";
		}
		
	}
	echo "<br><br>";

?>
</div>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>
<?php

?>