<?php

session_name('SESSION1');
session_start();

include('inc/dictionnary.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<meta name="robots" content="noindex,nofollow" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/send.js"></script>
<script type="text/javascript">
function showProlo(num_dossier) {
   $("#"+num_dossier).show();
}
</script>
</head>
<body>
<div id="header">
	<?PHP
   if ($_SESSION['connected'] == 0) {
      echo $dic_switchlg;
   }
   ?>
      
   <ul>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<?PHP if ($_SESSION['zeType'] == 'assistant' || $_SESSION['zeType'] == 'admin') { ?><li><a href="/"><?=$dic_chercherdepanneur?></a></li><?PHP }else{ ?><li><a href="/"><?=$dic_accueil?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] != 'assistant') { ?><li><a href="mdp.php"><?=$dic_modifierpwd?></a></li><?PHP }; ?>
		<li><a href="contrats.php"><?=$prolonger?></a></li>
		<?PHP if ($_SESSION['zeType'] == 'depanneur' || $_SESSION['zeType'] == 'admin') { ?><li><a href="search-contract.php"><?=$dic_print_contrat_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'depanneur') { ?><li><a href="situation-journaliere.php"><?=$dic_daily_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'admin') { ?><li><a href="imatlist.php"><?=$dic_imatlist?></a></li><?PHP }?>
		<?PHP	
		
		}
		?>
		<?php if ($_SESSION['zeType'] != 'admin') { ?><li><a href="mailto:BE-h24@europcar.com"><?=$dic_contacteznous?></a></li><?php } ?>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<li><a href="logout.php"><?=$dic_sedeconnecter?></a></li>
		<?PHP	
		}
		?>
	</ul>
</div>
<div id="container" style="height:auto;">
<div id="content">
<div id="leftcol" style="width:600px;">
   <dl>
   	<h1><?php echo $chercher_contrat; ?></h1>
   	<form name="searchcontrat" action="contrats.php" method="POST">
   	<?php echo $no_ref; ?> : <input type="text" name="numref"/><br><br>
   	<!--
   	<?php echo $par_imat; ?> : <input type="text" name="imat"/><br><br>
   	<?php echo $par_nom; ?> : <input type="text" name="nom"/><br><br>
   	-->
   	<input type="submit" name="searchcontrat" value="<?=$dic_chercher?>"/><br><br>
   </form>
<?php
// AJOUTER N° CONTRAT SI OUT
include('inc/connexion.php');

function SQLDatetoDDMMYY($date) {
	// from : 2015-08-31
	// to	: 31/08/2015
	$date = explode('-', $date);
	return $date[2].'/'.$date[1].'/'.$date[0];
	}
function SQLDateTimetoDDMMYYHHMM($date) {
	// from : 2015-08-31 12:00
	// to	: 31/08/2015 12:00
	global $lgstring;
	
	$explode = explode(' ', $date);
	$date = explode('-', $explode[0]);
	$time = explode(':', $explode[1]); 
	
	if ($lgstring == 'fr') {
		$separateur_date = 'à';
	}else{
		$separateur_date = 'om';
	}
	
	return $date[2].'/'.$date[1].'/'.$date[0].' '.$separateur_date.' '.$time[0].':'.$time[1];
	
	}

$count = 0;

	//if ($_SESSION['zeType'] == 'assistant' OR $_SESSION['zeType'] == 'admin')
	//{
		if (isset($_POST['numref']) && $_POST['numref'] != '')
		{
			
			$linkedSQL = '';
			// Si c'est une assistance qui cherche un contrat, alors ne faire une recherche que dans ses contrats liés à lui (pour éviter qu'il aille prolonger un contrat qui appartient à quelqu'un d'autre)
			if ($_SESSION['zeType'] == 'assistant') {
				$linkedSQL .= ' AND `assist_id` = '.$_SESSION['myid'];
				}
			// Si c'est un dépanneur qui cherche un contrat, alors ne faire une recherche que dans ses contrats liés à lui (pour éviter qu'il aille prolonger un contrat qui appartient à quelqu'un d'autre)
			if ($_SESSION['zeType'] == 'depanneur') {
				$linkedSQL .= ' AND `dep_id` = '.$_SESSION['myid'];
				}
				
		
				
			try 
			{
				$data = $conn->query ("SELECT dep_code, contrat_id, imat, model, client_name, id, date_start, date_fin FROM contrats WHERE numref = '{$_POST['numref']}'$linkedSQL ORDER BY id DESC");
			}
		  	catch(PDOException $e)
			{
				 echo $sql . "<br>" . $e->getMessage();
			}

			foreach ($data as $contrat)
			{
				
				try {
					$data2 = $conn->query ("SELECT log_prolongations.log_date, log_prolongations.log_days, log_prolongations.log_remarks, log_prolongations.log_new_date, assist_depaneurs.dep_nom FROM log_prolongations LEFT JOIN assist_depaneurs ON log_prolongations.log_who = assist_depaneurs.id WHERE log_contrat_id = ".$contrat['id']." ORDER BY log_prolongations.id DESC");
				}catch(PDOException $e){
					//echo $sql . "<br>" . $e->getMessage();
				}
				$log_prolo = '';
				foreach ($data2 as $log) {
					$log_prolo .= $dic_prolo_historique_log_1." ".SQLDateTimetoDDMMYYHHMM($log['log_date'])." ".$dic_prolo_historique_log_2." ".$log['log_days']." ".$dic_prolo_historique_log_3." ".SQLDatetoDDMMYY($log['log_new_date'])." ".$dic_prolo_historique_log_4." ".$log['dep_nom']."<br>";
					if ($log['log_remarks']) {
					$log_prolo .= "Remarques: ".$log['log_remarks']."<br>";
					}
					$log_prolo .= "<br>";
				}
				
					
				


				echo '<dt><span>'.$contratno.' N°'.$contrat['dep_code'].' '.$contrat['contrat_id'].'</span> / '.$contrat['imat'].' - '.$contrat['model'].' / '.$client.' : '.$contrat['client_name'].' / <span id="dateretour'.$contrat['id'].'">'.$creation_date.' : '.SQLDateTimetoDDMMYYHHMM($contrat['date_start']).'</span></dt><dd><span id="lien', $contrat['id'], '">';
		        echo $date_retour.' : '.SQLDateTimetoDDMMYYHHMM($contrat['date_fin']).'<br><a href="javascript:showProlo(\'contrat', $contrat['id'], '\');">', $dic_prolocontrat, '</a><br /><span style="display:none;" id="contrat', $contrat['id'], '">'.$dic_preciser_jours_prolongation.':<br><input type="text" name="prolo', $contrat['id'], '" id="prolo', $contrat['id'], '" value="" class="prolodate"> <br>Remarques:<br><input type="text" name="log_remarks_', $contrat['id'], '" id="log_remarks_', $contrat['id'], '" value="" maxlength="255" size="70"><br><input type="button" value="', $dic_valider, '" onclick="prolo(document.getElementById(\'prolo', $contrat['id'], '\').value, '.$contrat['id'].');" /></br></span>';
		        echo '</span>';
		        if ($log_prolo != '') { 
			        echo '<br><strong>'.$dic_prolo_historique.':</strong><br>';
		        	echo $log_prolo;
		        }
		        echo '</dd>';
		        
			}
		}
		/*
		else if (isset($_POST['imat']) && $_POST['imat'] != '')
		{
			try 
			{
				$data = $conn->query ("SELECT * FROM contrats WHERE imat REGEXP '{$_POST['imat']}' ORDER BY id DESC");
			}
		  	catch(PDOException $e)
			{
				// echo $sql . "<br>" . $e->getMessage();
			}

			foreach ($data as $contrat)
			{
				?>
				<script type="text/javascript">
				$(function() {
				    $('#prolo<?php echo $contrat["id"]; ?>').datetimepicker({
				   timeFormat: 'HH:mm',
   					dateFormat: 'yy-mm-dd',
					});
				});
				</script>
				<?php


				echo '<dt><span>'.$contratno.' N°'.$contrat['contrat_id'].'</span> / '.$contrat['imat'].' - '.$contrat['model'].' / '.$client.' : '.$contrat['nom_client'].' / <span id="dateretour'.$contrat['id'].'">'.$creation_date.' : '.$contrat['date_start'].'</span></dt><dd><span id="lien', $contrat['id'], '">';
		        echo $date_retour.' : '.$contrat['date_fin'].'<br><a href="javascript:showProlo(\'contrat', $contrat['id'], '\');">', $dic_prolocontrat, '</a><br /><span style="display:none;" id="contrat', $contrat['id'], '">', $dic_veuillezpreciserprolo, ' <input type="text" name="prolo', $contrat['id'], '" id="prolo', $contrat['id'], '" value=""/> <input type="button" value="', $dic_valider, '" onclick="prolo(document.getElementById(\'prolo', $contrat['id'], '\').value, '.$contrat['id'].');" /></br></span>';
		        echo '</span></dd>'; 
			}
		}

		else if (isset($_POST['nom']) && $_POST['nom'] != '')
		{
			try 
			{
				$data = $conn->query ("SELECT * FROM contrats WHERE nom_client REGEXP '{$_POST['nom']}' ORDER BY id DESC");
			}
		  	catch(PDOException $e)
			{
				// echo $sql . "<br>" . $e->getMessage();
			}

			foreach ($data as $contrat)
			{
				?>
				<script type="text/javascript">
				$(function() {
				    $('#prolo<?php echo $contrat["id"]; ?>').datetimepicker({
				   timeFormat: 'HH:mm',
   					dateFormat: 'yy-mm-dd',
					});
				});
				</script>
				<?php


				echo '<dt><span>'.$contratno.' N°'.$contrat['contrat_id'].'</span> / '.$contrat['imat'].' - '.$contrat['model'].' / '.$client.' : '.$contrat['nom_client'].' / <span id="dateretour'.$contrat['id'].'">'.$creation_date.' : '.$contrat['date_start'].'</span></dt><dd><span id="lien', $contrat['id'], '">';
		        echo $date_retour.' : '.$contrat['date_fin'].'<br><a href="javascript:showProlo(\'contrat', $contrat['id'], '\');">', $dic_prolocontrat, '</a><br /><span style="display:none;" id="contrat', $contrat['id'], '">', $dic_veuillezpreciserprolo, ' <input type="text" name="prolo', $contrat['id'], '" id="prolo', $contrat['id'], '" value=""/> <input type="button" value="', $dic_valider, '" onclick="prolo(document.getElementById(\'prolo', $contrat['id'], '\').value, '.$contrat['id'].');" /></br></span>';
		        echo '</span></dd>';  
			}
		}
		*/
		
		
	//}
	

?>
<p id="loadingMsg"></p>
</div>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>