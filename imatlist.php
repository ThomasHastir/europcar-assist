<?php
session_name('SESSION1');
session_start();

include('inc/dictionnary.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<meta name="robots" content="noindex,nofollow" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script type="text/javascript" src="js/send.js"></script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="js/jquery-ui-fr.js"></script>
<link href="css/jquery-ui.min.css" rel="stylesheet">
<link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet">
<style>
.ui-widget-header 
{
  border: 1px solid #037b00;
  background: #037b00;
  color: #fff;
  font-weight: bold;
}
</style>
<script type="text/javascript">
function showDossier(num_dossier) {
   $("#"+num_dossier).show();
}
function showBuyback(buyback) {
   $("#"+buyback).show();
}
</script>

<script type="text/javascript">
$(document).ready(function() {
   $('.getlink').click(function() {
     // alert('Ce véhicule existe déjà dans la base de données!');
         if(this.innerHTML == 'Voir contrats')
          {
            this.innerHTML = 'Fermer';
            setTimeout((function() {this.setAttribute("href", "attributeValue");}), 800);
          }
          else
          {
            this.innerHTML = 'Voir contrats';
            setTimeout((function() {this.setAttribute("href", "attributeValue");}), 800);
          }
         });
});
</script>
</head>
<body>
<div id="header">
   <?PHP
   if ($_SESSION['connected'] == 0) {
      echo $dic_switchlg;
   }
   ?>
      
   <ul>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<?PHP if ($_SESSION['zeType'] == 'assistant' || $_SESSION['zeType'] == 'admin') { ?><li><a href="/"><?=$dic_chercherdepanneur?></a></li><?PHP }else{ ?><li><a href="/"><?=$dic_accueil?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] != 'assistant') { ?><li><a href="mdp.php"><?=$dic_modifierpwd?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] == 'admin') { ?><li><a href="contrats.php"><?=$prolonger?></a></li><li><a href="search-contract.php"><?=$dic_print_contrat_title?></a></li><li><a href="imatlist.php"><?=$dic_imatlist?></a></li><?PHP }?>
		<?PHP	
		
		}
		?>
		<?php if ($_SESSION['zeType'] != 'admin') { ?><li><a href="mailto:BE-h24@europcar.com"><?=$dic_contacteznous?></a></li><?php } ?>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<li><a href="logout.php"><?=$dic_sedeconnecter?></a></li>
		<?PHP	
		}
		?>
	</ul>
</div>
<div id="container">
<div id="content">

<?php

include('inc/connexion.php');
$count = 0;
$counter0 = 0;
$counter1 = 0;
$counter2 = 0;

   ?>
   <div id="leftcol">
   <dl>
      <form name="search" method="POST" action="imatlist.php">
         <br>Chercher une imat : <input type="text" name="imat"/>
         <input type="submit" value="Chercher" name = "search"/>
      </form>
   <?php

   if (isset($_POST['search']) && $_POST['imat'] != '')
   {
      try 
      {
         $data2 = $conn->query ("SELECT * FROM assist_cars WHERE car_immatriculation REGEXP '{$_POST['imat']}' AND (car_status = 0 OR car_status = 1 OR car_status = 2 OR car_status = 3 OR car_status = 4)"); 
      }
      catch(PDOException $e)
      {
         // echo $sql . "<br>" . $e->getMessage();
      }
      $imats = "";
      $imatcount = 0;
      $car_imat = array();
      $car_acriss = array();
      $car_id = array();
      $car_status = array();
      $car_status_BB = array();
      $car_date_BB = array();
      foreach ($data2 as $car)
      {
         include('inc/connexion.php');
         $sql = "SELECT dep_nom FROM assist_depaneurs WHERE id = ".$car['car_depaneur'].";";
         $result = mysql_query($sql) 
         or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
         mysql_close($link);
         $row = mysql_fetch_array($result);
         $car_dep[] = $row['dep_nom'];
         //echo "Dépaneur : ".$dep['dep_nom'];
         $car_imat[] = $car['car_immatriculation'];
         $car_acriss[] = $car['car_acriss'];
         $car_id[] = $car['id'];
         $car_status[] = $car['car_status'];
         $car_status_BB[] = $car['car_status_BB'];
         $car_date_BB[] = $car['car_date_BB'];
         $count++;
         $imatcount++;
      }
      if ($imatcount > 0)
      {
         $counter = 0;
         foreach ($car_imat as $imat)
            {
               $status_style = "";
               if ($car_status[$counter] == 2)
               {
                  $status_style = "style='color:red;'";
                  $counter2++;
               }

               if ($car_status[$counter] == 1)
               {
                  $counter1++;
               }

               if ($car_status[$counter] == 0 OR $car_status[$counter] == 3 OR $car_status[$counter] == 4)
               {
                  $counter0++;
               }
               if ($car_status_BB[$counter] == 0)
               {
                  echo '<div id="'.$car_id[$counter].'"><dt><span '.$status_style.'>', $imat, '</span> ', $car_acriss[$counter], '<dd><span id="lien', $car_id[$counter], '">';
                  echo "<script type='text/javascript'>
                           $(function() {
                               $('#BB_date".$car_id[$counter]."').datepicker({
                              dateFormat: 'yy-mm-dd'
                              });
                           });
                           </script>";
                  echo '<a href="javascript:showBuyback(\'buyback', $car_id[$counter], '\');">', $dic_blocbb, '</a><br /><span style="display:none;" id="buyback', $car_id[$counter], '">', $dic_veuillezpreciserdate, ' <input type="text" name="BB_date', $car_id[$counter], '" id="BB_date', $car_id[$counter], '" value="" /> <input type="button" value="', $dic_valider, '" onclick="buyBackImat(document.getElementById(\'BB_date', $car_id[$counter], '\').value, '.$car_id[$counter].');" /></br></span>';
                  // echo '<br /><a class="getlink" id="get'.$car_id[$counter].'" data="javascript:get_contrats(', $car_id[$counter], ');" href="javascript:get_contrats(', $car_id[$counter], ');">Voir contrats</a><br />';
                  echo '</span></dd></div>';
               }
               if ($car_status_BB[$counter] == 1)
               {
                  echo '<div id="'.$car_id[$counter].'"><dt><span '.$status_style.'>', $imat, '</span> ', $car_acriss[$counter], '<dd><span id="lien', $car_id[$counter], '">';
                  echo "<script type='text/javascript'>
                           $(function() {
                               $('#BB_date".$car_id[$counter]."').datepicker({
                              dateFormat: 'yy-mm-dd'
                              });
                           });
                           </script>";
                  echo $dic_bloqued.$car_date_BB[$counter]."<br>";
                  echo '<a href="javascript:showBuyback(\'buyback', $car_id[$counter], '\');">', $dic_blocbbedit, '</a><br /><span style="display:none;" id="buyback', $car_id[$counter], '">', $dic_veuillezpreciserdate, ' <input type="text" name="BB_date', $car_id[$counter], '" id="BB_date', $car_id[$counter], '" value="" /> <input type="button" value="', $dic_valider, '" onclick="buyBackImat(document.getElementById(\'BB_date', $car_id[$counter], '\').value, '.$car_id[$counter].');" /></br></span>';
                  // echo '<br /><a class="getlink" id="get'.$car_id[$counter].'" data="javascript:get_contrats(', $car_id[$counter], ');" href="javascript:get_contrats(', $car_id[$counter], ');">Voir contrats</a><br />';
                  echo '</span></dd></div>';
               }
               $counter++; 
            }
      }  
   }

   else
   {
      try 
      {
         $data = $conn->query ("SELECT * FROM assist_depaneurs WHERE dep_assist = 0");
      }
      catch(PDOException $e)
      {
         // echo $sql . "<br>" . $e->getMessage();
      }
      foreach ($data as $dep)
      {
         //$message .= "Dépaneur : ".$dep['dep_nom']. "\r\n";
         try 
         {
            $data2 = $conn->query ("SELECT * FROM assist_cars WHERE car_depaneur = '{$dep['id']}' AND (car_status = 0 OR car_status = 1 OR car_status = 2 OR car_status = 3 OR car_status = 4)"); 
         }
         catch(PDOException $e)
         {
            // echo $sql . "<br>" . $e->getMessage();
         }
         
         //echo "Dépaneur : ".$dep['dep_nom'];
         $imats = "";
         $imatcount = 0;
         $car_imat = array();
         $car_acriss = array();
         $car_id = array();
         $car_status = array();
         $car_status_BB = array();
         $car_date_BB = array();
         foreach ($data2 as $car)
         {
            //echo "Dépaneur : ".$dep['dep_nom'];
            $car_imat[] = $car['car_immatriculation'];
            $car_acriss[] = $car['car_acriss'];
            $car_id[] = $car['id'];
            $car_status[] = $car['car_status'];
            $car_status_BB[] = $car['car_status_BB'];
            $car_date_BB[] = $car['car_date_BB'];
            $count++;
            $imatcount++;
         }
         if ($imatcount > 0)
         {
            echo "<br>";
            echo "<h1>".$dep['dep_nom']."</h1>";
            $counter = 0;
            foreach ($car_imat as $imat)
            {
               $status_style = "";
               if ($car_status[$counter] == 2)
               {
                  $status_style = "style='color:red;'";
                  $counter2++;
               }

               if ($car_status[$counter] == 1)
               {
                  $counter1++;
               }

               if ($car_status[$counter] == 0 OR $car_status[$counter] == 3 OR $car_status[$counter] == 4)
               {
                  $counter0++;
               }
               if ($car_status_BB[$counter] == 0)
               {
                  echo '<div id="'.$car_id[$counter].'"><dt><span '.$status_style.'>', $imat, '</span> ', $car_acriss[$counter], '<dd><span id="lien', $car_id[$counter], '">';
                  echo "<script type='text/javascript'>
                           $(function() {
                               $('#BB_date".$car_id[$counter]."').datepicker({
                              dateFormat: 'yy-mm-dd'
                              });
                           });
                           </script>";
                  echo '<a href="javascript:showBuyback(\'buyback', $car_id[$counter], '\');">', $dic_blocbb, '</a><br /><span style="display:none;" id="buyback', $car_id[$counter], '">', $dic_veuillezpreciserdate, ' <input type="text" name="BB_date', $car_id[$counter], '" id="BB_date', $car_id[$counter], '" value="" /> <input type="button" value="', $dic_valider, '" onclick="buyBackImat(document.getElementById(\'BB_date', $car_id[$counter], '\').value, '.$car_id[$counter].');" /></br></span>';
                  // echo '<br /><a class="getlink" id="get'.$car_id[$counter].'" data="javascript:get_contrats(', $car_id[$counter], ');" href="javascript:get_contrats(', $car_id[$counter], ');">Voir contrats</a><br />';
                  echo '</span></dd></div>';
               }
               if ($car_status_BB[$counter] == 1)
               {
                  echo '<div id="'.$car_id[$counter].'"><dt><span '.$status_style.'>', $imat, '</span> ', $car_acriss[$counter], '<dd><span id="lien', $car_id[$counter], '">';
                  echo "<script type='text/javascript'>
                           $(function() {
                               $('#BB_date".$car_id[$counter]."').datepicker({
                              dateFormat: 'yy-mm-dd'
                              });
                           });
                           </script>";
                  echo $dic_bloqued.$car_date_BB[$counter]."<br>";
                  echo '<a href="javascript:showBuyback(\'buyback', $car_id[$counter], '\');">', $dic_blocbbedit, '</a><br /><span style="display:none;" id="buyback', $car_id[$counter], '">', $dic_veuillezpreciserdate, ' <input type="text" name="BB_date', $car_id[$counter], '" id="BB_date', $car_id[$counter], '" value="" /> <input type="button" value="', $dic_valider, '" onclick="buyBackImat(document.getElementById(\'BB_date', $car_id[$counter], '\').value, '.$car_id[$counter].');" /></br></span>';
                  // echo '<br /><a class="getlink" id="get'.$car_id[$counter].'" data="javascript:get_contrats(', $car_id[$counter], ');" href="javascript:get_contrats(', $car_id[$counter], ');">Voir contrats</a><br />';
                  echo '</span></dd></div>';
               }
               $counter++; 
            }
         }
      }
      echo "<p><br><b>Total</b> : ".$count." / <b>Disponibles</b> : ".$counter0." / <b>Bloqués</b> : ".$counter1." / <b>Sur la route</b> : ".$counter2."</p>";
   }
   
?>
<p id="loadingMsg"></p>
</div>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>
<?php

?>