<?PHP
session_name('SESSION1');
session_start();

include('inc/dictionnary.php');
?>
function select_innerHTML(objeto,innerHTML){
/******
* select_innerHTML - corrige o bug do InnerHTML em selects no IE
* Veja o problema em: http://support.microsoft.com/default.aspx?scid=kb;en-us;276228
* Versão: 2.1 - 04/09/2007
* Autor: Micox - Náiron José C. Guimarães - micoxjcg@yahoo.com.br
* @objeto(tipo HTMLobject): o select a ser alterado
* @innerHTML(tipo string): o novo valor do innerHTML
*******/
    objeto.innerHTML = ""
    var selTemp = document.createElement("micoxselect")
    var opt;
    selTemp.id="micoxselect1"
    document.body.appendChild(selTemp)
    selTemp = document.getElementById("micoxselect1")
    selTemp.style.display="none"
    if(innerHTML.toLowerCase().indexOf("<option")<0){//se não é option eu converto
        innerHTML = "<option>" + innerHTML + "</option>"
    }
    //innerHTML = innerHTML.toLowerCase().replace(/<option/g,"<span").replace(/<\/option/g,"</span")
    innerHTML = innerHTML.replace(/<option/g,"<span").replace(/<\/option/g,"</span")
    selTemp.innerHTML = innerHTML
      
    
    for(var i=0;i<selTemp.childNodes.length;i++){
  var spantemp = selTemp.childNodes[i];
  
        if(spantemp.tagName){     
            opt = document.createElement("OPTION")
    
   if(document.all){ //IE
    objeto.add(opt)
   }else{
    objeto.appendChild(opt)
   }       
    
   //getting attributes
   for(var j=0; j<spantemp.attributes.length ; j++){
    var attrName = spantemp.attributes[j].nodeName;
    var attrVal = spantemp.attributes[j].nodeValue;
    if(attrVal){
     try{
      opt.setAttribute(attrName,attrVal);
      opt.setAttributeNode(spantemp.attributes[j].cloneNode(true));
     }catch(e){}
    }
   }
   //getting styles
   if(spantemp.style){
    for(var y in spantemp.style){
     try{opt.style[y] = spantemp.style[y];}catch(e){}
    }
   }
   //value and text
   opt.value = spantemp.getAttribute("value")
   opt.text = spantemp.innerHTML
   //IE
   opt.selected = spantemp.getAttribute('selected');
   opt.className = spantemp.className;
  } 
 }    
 document.body.removeChild(selTemp)
 selTemp = null
}
<?PHP
$myid = $_SESSION['myid'];
$in_depaneur = $_POST['in_depaneur'];


include('inc/connexion.php');

// comptage du nombre de véhicule pour ce depanneur
if (($_SESSION['zeType'] == 'assistant') && ($_SESSION['myid'] != 40)) {
	$sqlstring = " AND assist_cars.car_immatriculation NOT IN (SELECT ea_immatriculation FROM assist_ea)";
}else{
	$sqlstring = "";
}
$sql = "SELECT * FROM assist_depaneurs LEFT JOIN assist_cars ON assist_cars.car_depaneur = assist_depaneurs.id WHERE (assist_cars.car_status = 0 OR (ADDTIME(assist_cars.car_date, '4:00:00') < NOW() AND assist_cars.car_status = 1) OR assist_cars.car_status = 3 OR assist_cars.car_status = 4 OR (assist_cars.car_status = 1 AND assist_cars.car_assistance = ".$myid.")) AND assist_depaneurs.id =".$in_depaneur."".$sqlstring.";";

$result = mysql_query($sql) 
	or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
mysql_close($link);

if (mysql_num_rows($result) > 0 || $_SESSION['zeType'] == 'admin') {
	// le depaneur a des voitures de stock alors on affiche le bouton GO
	echo 'document.getElementById(\'sub_depaneurs\').style.display= "";';
}else{
	// le depaneur n'a pas de voiture. On affiche pas le bouton GO et on indique un message d'erreur
	
	echo 'document.getElementById(\'sub_depaneurs\').style.display= "none";';
	
	
	//envoyer une alert DB pour signaler que le depanneur selectionné est vide
	include('inc/connexion.php');
	
	$sql_refus = "INSERT INTO assist_refus (refus_assistance, refus_depaneur, refus_date)";
	$sql_refus .= "VALUES (".$_SESSION['myid'].", ".$in_depaneur.", NOW())";
	
	$result_refus = mysql_query($sql_refus) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	mysql_close($link);
	
	
	
	include('inc/connexion.php');
	

	
	if ($_SESSION['zeType'] == 'assistant') {
		$sqlstringOpeiningHours = " AND ((dep_".strtolower(date('D'))."_".date('a')."_start < CURTIME() AND dep_".strtolower(date('D'))."_".date('a')."_end > CURTIME() AND (assist_depaneurs.id NOT IN (SELECT horaire_dep FROM assist_horaires WHERE horaire_jour = CURDATE()))) AND (dep_relassistance LIKE '".$_SESSION['myid'].",%' or dep_relassistance LIKE '%,".$_SESSION['myid']."' or dep_relassistance LIKE '%,".$_SESSION['myid'].",%' or dep_relassistance  = '".$_SESSION['myid']."'))";
	}else{
		$sqlstringOpeiningHours = "";
	}
	
	$sql = "SELECT DISTINCT(assist_depaneurs.id), assist_depaneurs.dep_nom, assist_depaneurs.dep_cp, assist_depaneurs.dep_city, COUNT(assist_cars.id) AS vehicules, COUNT(CASE WHEN assist_cars.car_immatriculation NOT IN (SELECT ea_immatriculation FROM assist_ea) THEN 1 END) AS vehiculesminusEA FROM assist_depaneurs LEFT JOIN assist_cars ON assist_cars.car_depaneur = assist_depaneurs.id WHERE (assist_cars.car_status = 0 OR (ADDTIME(assist_cars.car_date, '4:00:00') < NOW() AND assist_cars.car_status = 1) OR assist_cars.car_status = 3 OR assist_cars.car_status = 4 OR (assist_cars.car_status = 1 AND assist_cars.car_assistance = ".$myid.") AND dep_assist = 0)".$sqlstringOpeiningHours." GROUP BY assist_depaneurs.id ORDER BY assist_depaneurs.dep_nom ASC";
	
	//echo $sql;
	
	$result = mysql_query($sql) 
	or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	mysql_close($link);
	
	
	echo 'document.getElementById(\'alert_depaneurs\').style.display = "";';
	echo 'alert("', $dic_plusdevehiculealert, '");';
	echo 'var testinner = "<option selected=\"selected\"></option>';

	while ($row = mysql_fetch_array($result)) {
		if ($row['vehiculesminusEA'] > 0 || $_SESSION['zeType'] == 'admin' || $_SESSION['myid'] == 40) {
		  	echo '<option value=\"', $row['id'], '\">', $row['dep_nom'], ' (', $row['dep_cp'], ' ', utf8_encode($row['dep_city']), ')</option>';
		}
		}
	echo '";';

	echo 'select_innerHTML(document.getElementById("in_depaneur"),testinner);';

}
?>