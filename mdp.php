<?PHP
session_name('SESSION1');
session_start();

include('inc/dictionnary.php');
?>
<?PHP
if (($_SESSION['connected'] == 1) && ($_SESSION['zeType'] != 'assistant')) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script type="text/javascript" src="js/send.js"></script>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
<script src="../js/parsley.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<?PHP if ($_SESSION['zeType'] == 'assistant' || $_SESSION['zeType'] == 'admin') { ?><li><a href="/"><?=$dic_chercherdepanneur?></a></li><?PHP }else{ ?><li><a href="/"><?=$dic_accueil?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] != 'assistant') { ?><li><a href="mdp.php"><?=$dic_modifierpwd?></a></li><?PHP }; ?>
		<li><a href="contrats.php"><?=$prolonger?></a></li>
		<?PHP if ($_SESSION['zeType'] == 'depanneur' || $_SESSION['zeType'] == 'admin') { ?><li><a href="search-contract.php"><?=$dic_print_contrat_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'depanneur') { ?><li><a href="situation-journaliere.php"><?=$dic_daily_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'admin') { ?><li><a href="imatlist.php"><?=$dic_imatlist?></a></li><?PHP }?>
		<?PHP	
		
		}
		?>
		<?php if ($_SESSION['zeType'] != 'admin') { ?><li><a href="mailto:BE-h24@europcar.com"><?=$dic_contacteznous?></a></li><?php } ?>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<li><a href="logout.php"><?=$dic_sedeconnecter?></a></li>
		<?PHP	
		}
		?>
	</ul>
</div>
<div id="container">
<div id="content">
<h1><?=$dic_modifiermdp?></h1>
<?PHP
if(isset($_POST['mdp_send']) && ($_POST['mdp_new1'] == $_POST['mdp_new2'])) {
	
	include('inc/connexion.php');
	
	$sql = "UPDATE assist_depaneurs";
	$sql .= " SET dep_pwd='".md5($_POST['mdp_new1'])."'";
	$sql .= " WHERE id=".$_SESSION['myid']."";

	
	$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	mysql_close($link);

	echo '<p>', $dic_modifiersucces, '</p>';
	

}else{
?>

	<?PHP
	if (isset($_POST['mdp_send']) && ($_POST['mdp_new1'] != $_POST['mdp_new2'])) {
	?>
	<p><?=$dic_erreurpwd?></p>
	<?PHP
	}
	?>
<form name="form1" data-parsley-validate id="form1" method="post" action="mdp.php">
<fieldset>
	<legend><?=$dic_choosenewpwd?></legend>
	<p><label for="mdp_new1" class="cellLike" style="width:200px;"><?=$dic_votrenouveaumdp?> </label> <input type="password" name="mdp_new1" id="mdp_new1" required="required" data-parsley-minlength="8" data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1" data-parsley-special="1" value="" /></p>
	<p><label for="mdp_new2" class="cellLike" style="width:200px;"><?=$dic_retapezmdp?> </label> <input type="password" name="mdp_new2" id="mdp_new2" required="required" data-parsley-equalto="#mdp_new1" data-parsley-validate-if-empty="true" value="" /></p>
</fieldset>
<input type="submit" name="mdp_send" id="mdp_send" value="<?=$dic_modifier?>" />
</form>
<?PHP
}
?>
<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
<!-- Parsley -->
	    <script>
	
	      $(document).ready(function() {
		      
		      //has uppercase
window.Parsley.addValidator('uppercase', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var uppercases = value.match(/[A-Z]/g) || [];
    return uppercases.length >= requirement;
  },
  messages: {
    en: '<?=_("Votre mot de passe doit contenir au minimum 1 lettre majuscule.")?>'
  }
});

//has lowercase
window.Parsley.addValidator('lowercase', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var lowecases = value.match(/[a-z]/g) || [];
    return lowecases.length >= requirement;
  },
  messages: {
    en: '<?=_("Votre mot de passe doit contenir au minimum 1 lettre minuscule.")?>'
  }
});

//has number
window.Parsley.addValidator('number', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var numbers = value.match(/[0-9]/g) || [];
    return numbers.length >= requirement;
  },
  messages: {
    en: '<?=_("Votre mot de passe doit contenir au minimum 1 chiffre.")?>'
  }
});

//has special char
window.Parsley.addValidator('special', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var specials = value.match(/[^a-zA-Z0-9]/g) || [];
    return specials.length >= requirement;
  },
  messages: {
    en: '<?=_("Votre mot de passe doit contenir au minimum 1 caractère spécial ($ ! %, etc..).")?>'
  }
});
		      

		      
	        window.Parsley.on('parsley:field:validate', function() {
	          validateFront();
	        });
	        $('#form1 input[type="submit"]').on('click', function() {
	          $('#form1').parsley().validate();
	          validateFront();
	        });
	        var validateFront = function() {
	          if (true === $('#form1').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');
	          } else {
	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');
	          }
	        };
	        
	        
	        
	        
	  
	        
	        
	        
	      });
	      try {
	        hljs.initHighlightingOnLoad();
	      } catch (err) {}
	    </script>
	    <!-- /Parsley -->
</body>
</html>
<?PHP	
}
?>