var http_request = false;
function makePOSTRequest(url, parameters) {
  http_request = false;
  if (window.XMLHttpRequest) { // Mozilla, Safari,...
	 http_request = new XMLHttpRequest();
	 if (http_request.overrideMimeType) {
		// set type accordingly to anticipated content type
		//http_request.overrideMimeType('text/xml');
		http_request.overrideMimeType('text/html');
	 }
  } else if (window.ActiveXObject) { // IE
	 try {
		http_request = new ActiveXObject("Msxml2.XMLHTTP");
	 } catch (e) {
		try {
		   http_request = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
	 }
  }
  if (!http_request) {
	 alert('Cannot create XMLHTTP instance');
	 return false;
  }
  
  http_request.onreadystatechange = alertContents;
  http_request.open('POST', url, true);
  http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  http_request.setRequestHeader("Content-length", parameters.length);
  http_request.setRequestHeader("Connection", "close");
  http_request.send(parameters);
}

function alertContents() {
  if (http_request.readyState == 4) {
	 if (http_request.status == 200) {
		document.getElementById('loadingMsg').style.display = "none";
		eval(http_request.responseText);
	 } else {
		alert('There was a problem with the request.');
	 }
  }else{
	document.getElementById('loadingMsg').style.display = "";
	document.getElementById('loadingMsg').innerHTML = "<img src=\"/img/loader.gif\" />";
  }
}

if (lgJS == 'nl') {
	etesvoussur = "Bent u zeker?";
}else{
	etesvoussur = "Etes vous sûr?";
}

function get(obj, filtre) {
  var poststr = "in_address=" + encodeURI( document.getElementById("in_address").value ) + "&filtre=" + encodeURI( filtre )
  makePOSTRequest('geoloc.php', poststr);
}
function get_all(obj) {
  var poststr = "in_depaneur=" + encodeURI( document.getElementById("in_depaneur").value )
  makePOSTRequest('nogeoloc.php', poststr);
}
function get_contrats(car_id) {
  var poststr = "car_id=" + encodeURI( car_id )
  makePOSTRequest('/ajax/get_contrats.php', poststr);
}
function cloture(id) {
  if(confirm(etesvoussur)) {
    var poststr = "id=" + encodeURI( id )
    makePOSTRequest('/ajax/cloture.php', poststr);
  }
}
function prolo(date, id) {
  if(confirm(etesvoussur)) {
  var poststr = "date=" + encodeURI( date ) + "&id=" + encodeURI( id ) + "&log_remarks=" + encodeURI( document.getElementById("log_remarks_" + id).value )
  makePOSTRequest('/ajax/prolo.php', poststr);
  }
}
function interv(id, current, start, end) {
  if(confirm(etesvoussur)) {
  var poststr = "id=" + encodeURI( id ) + "&current=" + encodeURI( current ) + "&start=" + encodeURI( start ) + "&end=" + encodeURI( end )
  makePOSTRequest('/ajax/interv.php', poststr);
  }
}
function blok(car_id, num_dossier, jours, theaction, ba, assistid) {
	if(confirm(etesvoussur)) {
  var poststr = "car_id=" + encodeURI( car_id ) + "&num_dossier=" + encodeURI( num_dossier ) + "&jours=" + encodeURI( jours ) + "&theaction=" + encodeURI( theaction ) + "&ba=" + encodeURI( ba ) + "&assistid=" + encodeURI( assistid )
  makePOSTRequest('/ajax/blok.php', poststr);
  }
}
function buyBack(date, id) {
  if(confirm(etesvoussur)) {
  var poststr = "date=" + encodeURI( date ) + "&id=" + encodeURI( id )
  makePOSTRequest('/ajax/buyback.php', poststr);
  }
}
function buyBackImat(date, id) {
  if(confirm(etesvoussur)) {
  var poststr = "date=" + encodeURI( date ) + "&id=" + encodeURI( id )
  makePOSTRequest('/ajax/buybackimat.php', poststr);
  }
}
function addCar(obj) {
  var poststr = "add_immatriculation=" + encodeURI( document.getElementById("add_immatriculation").value ) + "&add_acriss=" + encodeURI( document.getElementById("add_acriss").value ) + "&add_marque=" + encodeURI( document.getElementById("add_marque").value ) + "&add_model=" + encodeURI( document.getElementById("add_model").value ) + "&add_depanneur=" + encodeURI( document.getElementById("add_depanneur").value ) + "&add_km=" + encodeURI( document.getElementById("add_km").value )
  makePOSTRequest('/ajax/addcar.php', poststr);
}
function deleteCar(car_id, zetype) {
	if(confirm(etesvoussur)) {
  var poststr = "car_id=" + encodeURI( car_id ) + "&zetype=" + encodeURI( zetype )
  makePOSTRequest('/ajax/deletecar.php', poststr);
	}
}
function getStats(obj) {
  var poststr = "depanneur=" + encodeURI( document.getElementById("depanneurs").value )
  makePOSTRequest('stats.php', poststr);
}
function addClosingDay(obj, zeid, zeaction) {
  var poststr = "jour=" + encodeURI( document.getElementById("closingday").value ) + "&zeid=" + zeid + "&zeaction=" + zeaction
  makePOSTRequest('/ajax/admin-closingday.php', poststr);
}


function bondecommande(id, action, url) {
  if(confirm(etesvoussur)) {
 	var poststr = "id=" + encodeURI( id ) + "&action=" + encodeURI( action ) + "&url=" + encodeURI( url ) + "&client_lastname=" + encodeURI( document.getElementById("client_lastname").value ) + "&client_firstname=" + encodeURI( document.getElementById("client_firstname").value ) + "&client_address=" + encodeURI( document.getElementById("client_address").value ) + "&client_cp=" + encodeURI( document.getElementById("client_cp").value ) + "&client_city=" + encodeURI( document.getElementById("client_city").value ) + "&client_tel=" + encodeURI( document.getElementById("client_tel").value ) + "&client_plate=" + encodeURI( document.getElementById("client_plate").value ) + "&rental_days=" + encodeURI( document.getElementById("rental_days").value ) + "&rental_start=" + encodeURI( document.getElementById("rental_start").value ) + "&rental_remarks=" + encodeURI( document.getElementById("rental_remarks").value )
  
	$('form').validate({
		rules: {
			client_lastname: {
				required: true
			},
			client_firstname: {
				required: true
			},
			client_tel: {
				required: true
			},
			client_plate: {
				required: true
			}
		}
	});
	// Si on sauvegarde provisoirement, on ne fait pas de validation JS. Si on sauvegarde définitivement il faut valider les champs requis
	if (action == 'back' || $("form").valid()) { makePOSTRequest('/ajax/bondecommande.php', poststr); }

	}
}
function bondecommandeAgency() {
	if(confirm(etesvoussur)) {
		
		var poststr = "agency_id=" + encodeURI( document.getElementById("agency_id").value ) + "&accriss=" + encodeURI( document.getElementById("accriss").value ) + "&num_dossier=" + encodeURI( document.getElementById("num_dossier").value ) + "&assist_id=" + encodeURI( document.getElementById("assist_id").value ) + "&client_lastname=" + encodeURI( document.getElementById("client_lastname").value ) + "&client_firstname=" + encodeURI( document.getElementById("client_firstname").value ) + "&client_address=" + encodeURI( document.getElementById("client_address").value ) + "&client_cp=" + encodeURI( document.getElementById("client_cp").value ) + "&client_city=" + encodeURI( document.getElementById("client_city").value ) + "&client_tel=" + encodeURI( document.getElementById("client_tel").value ) + "&client_plate=" + encodeURI( document.getElementById("client_plate").value ) + "&rental_days=" + encodeURI( document.getElementById("rental_days").value ) + "&rental_start=" + encodeURI( document.getElementById("rental_start").value ) + "&rental_remarks=" + encodeURI( document.getElementById("rental_remarks").value )
		
		$('form').validate({
			rules: {
				client_lastname: {
					required: true
				},
				client_firstname: {
					required: true
				},
				client_tel: {
					required: true
				},
				client_plate: {
					required: true
				}
			}
		});
		
		if ($("form").valid()) { makePOSTRequest('/ajax/bondecommande.php', poststr); }
		
		}
}


function loadBA(id) {
  var poststr = "id=" + encodeURI( id ) + "&assistid=" + encodeURI( document.getElementById("assistance_"+id).value )
  makePOSTRequest('/ajax/loadba.php', poststr);
}