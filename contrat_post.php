<?php
session_name('SESSION1');
session_start();

//ini_set("log_errors", 1);
//ini_set("error_log", "log/contrat_post.txt");

include('inc/functions.php');
include('inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}


// On active le decryptage que pour les contrats créés après la mise en place du cryptage
$decrypt = 'none';
if ($_POST['contrat_id'] >= 15182) {
   $decrypt = 'encrypt';
	}

	
	
$date_start = convertToSQLDateTime($_POST['date_start']);
$date_fin = convertToSQLDateTime($_POST['date_fin']);


$expdate = $_POST['expdate'];
$client_birthdate = DDMMYYtoSQLDate($_POST['client_birthdate']);
$client_idexp = DDMMYYtoSQLDate($_POST['client_idexp']);
$client_assexp = DDMMYYtoSQLDate($_POST['client_assexp']);
$client_permdate = DDMMYYtoSQLDate($_POST['client_permdate']);
$client_iddate = DDMMYYtoSQLDate($_POST['client_iddate']);

$client_iddate2 = DDMMYYtoSQLDate($_POST['client_iddate2']);
$client_idexp2 = DDMMYYtoSQLDate($_POST['client_idexp2']);
$client_birthdate2 = DDMMYYtoSQLDate($_POST['client_birthdate2']);
$client_permdate2 = DDMMYYtoSQLDate($_POST['client_permdate2']);
$client_assexp2 = DDMMYYtoSQLDate($_POST['client_assexp2']);

$backdate_fin = convertToSQLDateTime($_POST['backdate_fin']);

include('inc/dictionnary.php');

// On vérifie si c'est une sauvegarde temporaire ou bien une validation définitive (= véhicule livré au client)
if($_POST['definitivement'] == 1) {
	
	// Bugfix: Si on bloque un véhicule A puis un autre véhicule B sans avoir clôturé le contrat A, alors on a un soucis avec le num de contrat: A et B auront le même numéro de contrat définitif issus du numéro de contrat provisoire. Pour régler le problème, à chaque ouverture (édition) du contrat, on revérifie quel est le dernier numéro de contrat dispo chez le dépanneur
	
	$statement = $db->prepare("SELECT * FROM assist_depaneurs WHERE id = :dep_id;");
	$statement->execute(array('dep_id' => $_POST['dep_id']));
	
	$row = $statement->fetch(PDO::FETCH_ASSOC);
	$dep_current = $row['nocontrat_current'];
      
    $contrat_id = $dep_current += 1;
	
	$contrat_id_string = 'contrat_id = '.$contrat_id.', ';
	 
}else if($_POST['definitivement'] == 0) {
	$contrat_id_string = 'contrat_id = 0, ';
}else{
	$contrat_id_string = '';
}





if (!empty($_POST))
{

  if (isset($_POST['prolo']) AND $_POST['prolo'] == 1)
  {
	  
	  $statement = $db->prepare("UPDATE contrats SET date_fin_old = date_fin;");
	  $statement->execute();
	  

      
  }
  if ($_POST['damage_back2'] == '' && $_POST['damage_start'] != '' OR $_POST['damage_back2'] == '•' && $_POST['damage_start'] != '•' OR $_POST['damage_back2'] == '' && $_POST['damage_start'] != '•' OR $_POST['damage_back2'] == '•' && $_POST['damage_start'] != '')
  {
    $damage_back = $_POST['damage_start'];
  }
  else if ($_POST['damage_back2'] != '' OR $_POST['damage_back2'] != '•')
  {
    $damage_back = $_POST['damage_back1']."\n".$_POST['damage_back2'];
    //echo $_POST['damage_back1'];
  }
  




      

   $statement = $db->prepare("UPDATE contrats SET $contrat_id_string contrat_id_lang = :contrat_id_lang, contrat_id_prov = :contrat_id_prov, nom_client = :nom_client, adresse_client = :adresse_client, imat = :imat, date_fin = :date_fin, date_start = :date_start, model = :model, km_start = :km_start, prolo = :prolo, start_remarques = :start_remarques, startfuel = :startfuel, damage_start = :damage_start, damage_back = :damage_back, numref = :numref, cat = :cat, jours = :jours, credit = :credit, montant = :montant, cca = :cca, expdate = :expdate, garantie = :garantie, autor = :autor, franchise = :franchise, client_idnum = :client_idnum, client_prenom = :client_prenom, client_permnum = :client_permnum, client_iddate = :client_iddate, client_idplace = :client_idplace, client_permdate = :client_permdate, client_permplace = :client_permplace, client_idexp = :client_idexp, client_birthplace = :client_birthplace, client_assexp = :client_assexp, client_CP = :client_CP, client_pays = :client_pays, client_birthdate = :client_birthdate, client_birthpays = :client_birthpays, client_tel = :client_tel, 2ndchauff = :2ndchauff, backdate_fin = :backdate_fin, dep_nom_back = :dep_nom_back, backdep_nom = :backdep_nom, km_retour = :km_retour, backcaution = :backcaution, backcautionmontant = :backcautionmontant, remarques = :remarques, backfuel = :backfuel, backdep_name = :backdep_name, client_name = :client_name, remarques2 = :remarques2, nom_client2 = :nom_client2, client_prenom2 = :client_prenom2, adresse_client2 = :adresse_client2, client_CP2 = :client_CP2, client_pays2 = :client_pays2, client_idnum2 = :client_idnum2, client_idplace2 = :client_idplace2, client_iddate2 = :client_iddate2, client_idexp2 = :client_idexp2, client_birthdate2 = :client_birthdate2, client_tel2 = :client_tel2, client_permnum2 = :client_permnum2, client_permplace2 = :client_permplace2, client_permdate2 = :client_permdate2, client_assexp2 = :client_assexp2, client_birthplace2 = :client_birthplace2, client_birthpays2 = :client_birthpays2 WHERE id = :id");
	$statement->execute(array(
		'contrat_id_lang' => $_POST['contratidlang'],
		'contrat_id_prov' => $_POST['contratid'],
		'nom_client' => encrypt_decrypt($decrypt, $_POST['nom_client']),
		'adresse_client' => encrypt_decrypt($decrypt, $_POST['adresse_client']),
		'imat' => $_POST['imat'],
		'date_fin' => $date_fin,
		'date_start' => $date_start,
		'model' => $_POST['model'],
		'km_start' => $_POST['km_start'],
		'prolo' => (!$_POST['prolo'] ? NULL : (int)$_POST['prolo']),
		'start_remarques' => $_POST['start_remarques'],
		'startfuel' => $_POST['startfuel'],
		'damage_start' => $_POST['damage_start'],
		'damage_back' => $damage_back,
		'numref' => $_POST['numref'],
		'cat' => $_POST['cat'],
		'jours' => $_POST['jours'],
		'credit' => $_POST['credit'],
		'montant' => $_POST['montant'],
		'cca' => encrypt_decrypt($decrypt, $_POST['cca']),
		'expdate' => encrypt_decrypt($decrypt, $expdate),
		'garantie' => $_POST['garantie'],
		'autor' => encrypt_decrypt($decrypt, $_POST['autor']),
		'franchise' => $_POST['franchise'],
		'client_idnum' => encrypt_decrypt($decrypt, $_POST['client_idnum']),
		'client_prenom' => encrypt_decrypt($decrypt, $_POST['client_prenom']),
		'client_permnum' => encrypt_decrypt($decrypt, $_POST['client_permnum']),
		'client_iddate' => encrypt_decrypt($decrypt, $client_iddate),
		'client_idplace' => encrypt_decrypt($decrypt, $_POST['client_idplace']),
		'client_permdate' => encrypt_decrypt($decrypt, $client_permdate),
		'client_permplace' => encrypt_decrypt($decrypt, $_POST['client_permplace']),
		'client_idexp' => encrypt_decrypt($decrypt, $client_idexp),
		'client_birthplace' => encrypt_decrypt($decrypt, $_POST['client_birthplace']),
		'client_assexp' => encrypt_decrypt($decrypt, $client_assexp),
		'client_CP' => $_POST['client_CP'],
		'client_pays' => $_POST['client_pays'],
		'client_birthdate' => encrypt_decrypt($decrypt, $client_birthdate),
		'client_birthpays' => encrypt_decrypt($decrypt, $_POST['client_birthpays']),
		'client_tel' => encrypt_decrypt($decrypt, $_POST['client_tel']),
		'2ndchauff' => $_POST['2ndchauff'],
		'backdate_fin' => $backdate_fin,
		'dep_nom_back' => $_POST['dep_nom_back'],
		'backdep_nom' => $_POST['backdep_nom'],
		'km_retour' => (!$_POST['km_retour'] ? NULL : (int)$_POST['km_retour']),
		'backcaution' => $_POST['backcaution'],
		'backcautionmontant' => $_POST['backcautionmontant'],
		'remarques' => $_POST['remarques'],
		'backfuel' => $_POST['backfuel'],
		'backdep_name' => $_POST['backdep_name'],
		'client_name' => encrypt_decrypt($decrypt, $_POST['client_name']),
		'remarques2' => $_POST['remarques2'],
		'nom_client2' => encrypt_decrypt($decrypt, $_POST['nom_client2']),
		'client_prenom2' => encrypt_decrypt($decrypt, $_POST['client_prenom2']),
		'adresse_client2' => encrypt_decrypt($decrypt, $_POST['adresse_client2']),
		'client_CP2' => (!$_POST['client_CP2'] ? NULL : (int)$_POST['client_CP2']),
		'client_pays2' => $_POST['client_pays2'],
		'client_idnum2' => encrypt_decrypt($decrypt, $_POST['client_idnum2']),
		'client_idplace2' => encrypt_decrypt($decrypt, $_POST['client_idplace2']),
		'client_iddate2' => encrypt_decrypt($decrypt, $_POST['client_iddate2']),
		'client_idexp2' => encrypt_decrypt($decrypt, $_POST['client_idexp2']),
		'client_birthdate2' => encrypt_decrypt($decrypt, $_POST['client_birthdate2']),
		'client_tel2' => encrypt_decrypt($decrypt, $_POST['client_tel2']),
		'client_permnum2' => encrypt_decrypt($decrypt, $_POST['client_permnum2']),
		'client_permplace2' => encrypt_decrypt($decrypt, $_POST['client_permplace2']),
		'client_permdate2' => encrypt_decrypt($decrypt, $_POST['client_permdate2']),
		'client_assexp2' => $_POST['client_assexp2'],
		'client_birthplace2' => encrypt_decrypt($decrypt, $_POST['client_birthplace2']),
		'client_birthpays2' => $_POST['client_birthpays2'],
		'id' => $_POST['contrat_id']
	));
    

      
      
  

    
     $statement = $db->prepare("UPDATE assist_cars SET damage = :damage_back WHERE id = :car_id");
	 $statement->execute(array('damage_back' => $damage_back, 'car_id' => $_SESSION['car_id']));
    
    
    
    //Update dernier id contrat utilisé
    if($_POST['definitivement'] == 1) {

	    
		$statement = $db->prepare("UPDATE assist_depaneurs SET nocontrat_current = :contrat_id WHERE id = :dep_id;");
		$statement->execute(array('contrat_id' => $contrat_id, 'dep_id' => $_POST['dep_id']));
	    
	    
    }
    
    
    
  
    
    

//echo $_POST['start_remarques'];




	// Si c'est une validation définitive, il faut fair een sorte que ça flag le véhicule comme livré au client
	
	if($_POST['definitivement'] == 3) {
		if ($_SESSION['zeType'] == 'admin') {
			$sqlstring = " car_assistance = ".$_SESSION['myid'].",";
		}

		
		$statement = $db->prepare("UPDATE assist_cars SET car_status = '2',".$sqlstring." car_date_out = NOW() WHERE id= :car_id;");
		$statement->execute(array('car_id' => $_POST['car_id']));
		
		

		$sql = "INSERT INTO assist_queries (car_immatriculation, car_date, car_assistance, car_dossier, car_status, car_depaneur) ";
		$sql .= "SELECT car_immatriculation, car_date_out, car_assistance, car_dossier, car_status, car_depaneur ";
		$sql .= "FROM assist_cars ";
		$sql .= "WHERE id = :car_id;";
		
		$statement = $db->prepare($sql);
		$statement->execute(array('car_id' => $_POST['car_id']));
		

	
	}
	
	if($_POST['definitivement'] == 4) {
		// Retour du véhicule, on remet le véhicule dans la flotte.
		
		$statement = $db->prepare("UPDATE assist_cars SET car_status = 0, damage = NULL, car_date_in = NOW(), car_date_out = NULL, car_date_BB = NULL, car_status_BB = 0, car_assistance = NULL, car_dossier = NULL, contrat = 0, bondecommande = NULL, car_km = :car_km  WHERE id = :car_id;");
		$statement->execute(array('car_id' => $_POST['car_id'], 'car_km' => $_POST['km_retour']));
			
		
	}

}

$statement = null; $db = null;

if($_POST['definitivement'] == 1) {
	header("Location: /contrat.php?contratid=".$_POST['contrat_id']."&print_after_validation=1");
	
}else if($_POST['definitivement'] == 3 || $_POST['definitivement'] == 4) {
	header("Location: depanneur_details.php?id=".$_POST['dep_id']);
}else{
	//header("Location: depanneur_details.php?id=".$_POST['dep_id']);
	header("Location: /contrat.php?contratid=".$_POST['contrat_id']."&retour=".$_POST['retour']);
}
?>