<?php

session_name('SESSION1');
session_start();

include('inc/dictionnary.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<meta name="robots" content="noindex,nofollow" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="js/send.js"></script>
<script type="text/javascript">
jQuery(function($){
   $("#immatriculation").mask("9-aaa-999");
});
</script>
</head>
<body>
<div id="header">
	<?PHP
   if ($_SESSION['connected'] == 0) {
      echo $dic_switchlg;
   }
   ?>
      
   <ul>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<?PHP if ($_SESSION['zeType'] == 'assistant' || $_SESSION['zeType'] == 'admin') { ?><li><a href="/"><?=$dic_chercherdepanneur?></a></li><?PHP }else{ ?><li><a href="/"><?=$dic_accueil?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] != 'assistant') { ?><li><a href="mdp.php"><?=$dic_modifierpwd?></a></li><?PHP }; ?>
		<li><a href="contrats.php"><?=$prolonger?></a></li>
		<?PHP if ($_SESSION['zeType'] == 'depanneur' || $_SESSION['zeType'] == 'admin') { ?><li><a href="search-contract.php"><?=$dic_print_contrat_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'depanneur') { ?><li><a href="situation-journaliere.php"><?=$dic_daily_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'admin') { ?><li><a href="imatlist.php"><?=$dic_imatlist?></a></li><?PHP }?>
		<?PHP	
		
		}
		?>
		<li><a href="mailto:BE-h24@europcar.com"><?=$dic_contacteznous?></a></li>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<li><a href="logout.php"><?=$dic_sedeconnecter?></a></li>
		<?PHP	
		}
		?>
	</ul>
</div>
<div id="container" style="height:auto;">
<div id="content">
<div id="leftcol" style="width:600px;">
   <dl>
   	<h1>Gestion des dégâts</h1>
   	<form name="searchcontrat" action="gestion-degats.php" method="POST">
   	Recherche via son immatriculation : <input type="text" name="immatriculation" id="immatriculation" style="text-transform:uppercase;" /><br><br>
   	<input type="submit" name="searchcontrat" value="<?=$dic_chercher?>"/><br><br>
   </form>
   
   <dl>
   <?php
	  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		  
		  function SQLDatetoDDMMYY($date) {
			// from : 2015-08-31
			// to	: 31/08/2015
			$date = explode('-', $date);
			return $date[2].'/'.$date[1].'/'.$date[0];
			}
		  
		  // 1-XUC-125
		  include('inc/connexion-pdo.php');
		  
		  
		  $immatriculation = $_POST['immatriculation'];
		  
		     try {
			    $db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
			    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				$statement = $db->prepare("SELECT `degats_log`.`car_immat`, `degats_log`.`car_degats` FROM `degats_log` WHERE `degats_log`.`car_immat` = '$immatriculation'");
				$statement->execute();
				
				
			    foreach($statement as $row) {
			 ?>
			 	<dt>
			 		<span><?=$row['car_immat']?></span></dt>
			 	<dd>
			 		<br><?=$row['car_degats']?><br><br>
			 		<a href="#" target="_blank">Editer la liste des dégâts</a><br />
			 	</dd>
			 <?php
			    }
			    	$db = null;
				} catch (PDOException $e) {
				    print "Erreur !: " . $e->getMessage() . "<br/>";
				    die();
				}
		  
		  
		  
		  } 
	   
	  ?>
	</dl>

<p id="loadingMsg"></p>
</div>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>