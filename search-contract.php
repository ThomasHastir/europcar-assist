<?php

session_name('SESSION1');
session_start();

include('inc/dictionnary.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='css/layout.css' rel='stylesheet' type='text/css'>
<meta name="robots" content="noindex,nofollow" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="js/send.js"></script>
<script type="text/javascript">
jQuery(function($){
   $("#immatriculation").mask("9-aaa-999");
});
</script>
</head>
<body>
<div id="header">
	<?PHP
   if ($_SESSION['connected'] == 0) {
      echo $dic_switchlg;
   }
   ?>
      
   <ul>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<?PHP if ($_SESSION['zeType'] == 'assistant' || $_SESSION['zeType'] == 'admin') { ?><li><a href="/"><?=$dic_chercherdepanneur?></a></li><?PHP }else{ ?><li><a href="/"><?=$dic_accueil?></a></li><?PHP }; ?>
		<?PHP if ($_SESSION['zeType'] != 'assistant') { ?><li><a href="mdp.php"><?=$dic_modifierpwd?></a></li><?PHP }; ?>
		<li><a href="contrats.php"><?=$prolonger?></a></li>
		<?PHP if ($_SESSION['zeType'] == 'depanneur' || $_SESSION['zeType'] == 'admin') { ?><li><a href="search-contract.php"><?=$dic_print_contrat_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'depanneur') { ?><li><a href="situation-journaliere.php"><?=$dic_daily_title?></a></li><?PHP }?>
		<?PHP if ($_SESSION['zeType'] == 'admin') { ?><li><a href="imatlist.php"><?=$dic_imatlist?></a></li><?PHP }?>
		<?PHP	
		
		}
		?>
		<?php if ($_SESSION['zeType'] != 'admin') { ?><li><a href="mailto:BE-h24@europcar.com"><?=$dic_contacteznous?></a></li><?php } ?>
		<?PHP
		if ($_SESSION['connected'] == 1) {
		?>
		<li><a href="logout.php"><?=$dic_sedeconnecter?></a></li>
		<?PHP	
		}
		?>
	</ul>
</div>
<div id="container" style="height:auto;">
<div id="content">
<div id="leftcol" style="width:600px;">
   <dl>
   	<h1><?php echo $chercher_contrat; ?></h1>
   	<form name="searchcontrat" action="search-contract.php" method="POST">
   	<?=$dic_print_contrat_plaque?> : <input type="text" name="immatriculation" id="immatriculation" style="text-transform:uppercase;" /><br><br>
   	
   	<?php if ($_SESSION['zeType'] == 'admin') { ?>
   	<strong><?=_('OU')?></strong><br><br>
   	<?=_('Numéro de contrat')?> : <input type="text" name="contractnr" id="contractnr" style="text-transform:uppercase;" /><br><br>
   	<?php } ?>
   	
   	<input type="submit" name="searchcontrat" value="<?=$dic_chercher?>"/><br><br>
   </form>
   
   <dl>
   <?php
	  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		  
		  function SQLDatetoDDMMYY($date) {
			// from : 2015-08-31
			// to	: 31/08/2015
			$date = explode('-', $date);
			return $date[2].'/'.$date[1].'/'.$date[0];
			}
		function SQLDateTimetoDDMMYYHHMM($date) {
			// from : 2015-08-31 12:00:00
			// to	: 31/08/2015 12:00
			$explode = explode(' ', $date);
			$date = explode('-', $explode[0]);
			$time = explode(':', $explode[1]); 
			return $date[2].'/'.$date[1].'/'.$date[0].' '.$time[0].':'.$time[1];
			}
		  
		  // 1-XUC-125
		  include('inc/connexion-pdo.php');
		  
		  
		  $immatriculation = $_POST['immatriculation'];
		  $contractnr = $_POST['contractnr'];
		  
		  if ($immatriculation || $contractnr) {
		  
		  // Accessible uniquement à l'admin ou au dépanneur. Si dépanneur, alors il ne peut voir que ses véhicules.
		  $SQLWhere = '';
		  if ($_SESSION['zeType'] == 'depanneur') {
			  $SQLWhere = ' AND dep_id = '.$_SESSION['myid'];
			  
			  }
		  
		     try {
			    $db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
			    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				
				if ($immatriculation) {
					$sqlString = "imat = '".$immatriculation."'";
				}else if($contractnr) {
					
					$dep_code = substr($contractnr, 0, 6);
					$contrat_id = substr($contractnr, 6);
					
					$sqlString = "dep_code = '".$dep_code."' AND contrat_id = ".$contrat_id;
				}
				
				
				$statement = $db->prepare("SELECT contrats.id, contrats.contrat_id, contrats.`imat`, contrats.`date_start`, contrats.`date_fin`, assist_cars.`car_marque`, assist_cars.`car_model` FROM contrats LEFT JOIN assist_cars ON contrats.id = assist_cars.`contrat` WHERE $sqlString AND contrat_id <> 0".$SQLWhere);
				$statement->execute();
				
				
				
			    foreach($statement as $row) {
				    
				    // cryptage de l'id dans l'URL afin d'éviter que le dépanneur n'essaye d'enlever le paramètre print=1 pour tenter de modifier le contrat
				    
				    $cryptedId = ($row['id'].substr($row['id'], 0, 1))*1225;
			 ?>
			 	
			 	<dt>
			 		<span><?=$row['imat']?></span> ECMV - <?=$row['car_marque']?> <?=$row['car_model']?></dt>
			 	<dd>
			 		<?=$dic_print_contrat_debut?>: <?=SQLDateTimetoDDMMYYHHMM($row['date_start'])?><br>
			 		<?=$dic_print_contrat_retour?>: <?=SQLDateTimetoDDMMYYHHMM($row['date_fin'])?><br>
			 		<a href="/contrat.php?contratid=<?=$cryptedId?>&amp;print=1" target="_blank"><?=$dic_print_contrat_consulter?></a><br />
			 	</dd>
			 	
			 	
			 <?php   
			    }
			    	$db = null;
				} catch (PDOException $e) {
				    print "Erreur !: " . $e->getMessage() . "<br/>";
				    die();
				}
		  
		  
		  }
		  
		  } 
	   
	  ?>
	</dl>

<p id="loadingMsg"></p>
</div>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>