<?php
session_name('SESSION1');
session_start();

//ini_set("log_errors", 1);
//ini_set("error_log", "log/contrat_post.txt");



include('inc/functions.php');
include('inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}


	
	

include('inc/dictionnary.php');
if (isset($_POST['submit']) or $_COOKIE['rememberme'] == 1) {
   
   if (isset($_POST['submit'])) {
      $_SESSION['zeLogin'] = $_POST['login_usr'];
      $_SESSION['zePwd'] = md5($_POST['login_pwd']);
   }else{
      $_SESSION['zeLogin'] = $_COOKIE['login_usr'];
      $_SESSION['zePwd'] = $_COOKIE['login_pwd'];
   }
   

      
   $statement = $db->prepare("SELECT * FROM assist_depaneurs WHERE dep_mail = :zeLogin AND dep_pwd = :zePwd;");
   $statement->execute(array('zeLogin' => $_SESSION['zeLogin'], 'zePwd' => $_SESSION['zePwd']));
   
   if ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
         
         $_SESSION['connected'] = 1;
         $_SESSION['myid'] = $row['id'];
         
         switch ($row['dep_assist']) {
         case 0:
            $_SESSION['zeType'] = 'depanneur';
            break;
         case 1:
            $_SESSION['zeType'] = 'assistant';
            break;
         case 2:
            $_SESSION['zeType'] = 'admin';
            break;
         }
         
         // Remember me
         if (isset($_POST['submit'])) {
            if ($_POST['rememberme'] == 1) {
               setcookie("rememberme", 1, time()+60*60*24*30);
               setcookie("login_usr", $_POST['login_usr'], time()+60*60*24*30);
               setcookie("login_pwd", md5($_POST['login_pwd']), time()+60*60*24*30);
            }else{
               setcookie("rememberme", "");
            }
         }
         
   }else{
      $_SESSION['connected'] = 0;
      session_destroy();
      setcookie("rememberme", "");
      $errorMsg = $dic_loginerror;
   }
   
}
if ($_SESSION['connected'] == 1)
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Contrat Europcar Assistance</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="noindex,nofollow" />

<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<link href="css/facturestyle.css" rel="stylesheet">
<link href="css/rangeslider.css" rel="stylesheet">
<link href="css/featherlight.min.css" type="text/css" rel="stylesheet" />

<link rel="stylesheet" href="css/print.css" type="text/css" media="print"/>

<!--[if IE]>
   <link href="css/facturestyle-ie.css" rel="stylesheet">
<![endif]-->

<!--[if IE 6]>
   <link href="css/facturestyle-ienope.css" rel="stylesheet">
<![endif]-->

<!--[if IE 7]>
   <link href="css/facturestyle-ienope.css" rel="stylesheet">
<![endif]-->

<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
<script src="js/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.creditCardValidator.js" type="text/javascript" charset="utf-8"></script>


<script>

$(document).ready(function(){
	
	
$("#date_start").mask("99/99/9999 99:99");
$("#date_fin").mask("99/99/9999 99:99");


$("#expdate").mask("99/99");



$("#client_birthdate").mask("99/99/9999");
$("#client_idexp").mask("99/99/9999");
$("#client_assexp").mask("99/99/9999");
$("#client_permdate").mask("99/99/9999");
$("#client_iddate").mask("99/99/9999");

$("#client_iddate2").mask("99/99/9999");
$("#client_idexp2").mask("99/99/9999");
$("#client_birthdate2").mask("99/99/9999");
$("#client_permdate2").mask("99/99/9999");
$("#client_assexp2").mask("99/99/9999");

$('#backdate_fin').mask("99/99/9999 99:99");


$("input[name='montant']").focusout(function() {
	 if ($(this).val()) {
		  $( "input[name='cca'], input[name='expdate'], input[name='autor']" ).prop('disabled', true).val('');
		  
	 }else{
	    $( "input[name='cca'], input[name='expdate'], input[name='autor']" ).prop('disabled', false).val('');
	    }
	  })



    $('.fuel').each(function(e) {
      $(this).rangeslider();
    });
	
   $('textarea').on('keydown',function(e){
     var t = $(this);
     switch(e.which){
     case 13:
       t.val(t.val()+'\n•');
       return false;
     }
       
   }); 
   
   
   var seconddrivervalue = $('input[name=2ndchauff]:checked').val();
   if (seconddrivervalue == 0) {
	   $('#seconddriver').parent().fadeTo('fase', 0.2); $('#seconddriver input').val('');
   }
   
   var resultCC = $('#cca').validateCreditCard();
   

   
   $('#cca').keyup(function () {
		var resultCC = $(this).validateCreditCard();
		if (resultCC.card_type != null) {
    		$('#cardType').html(resultCC.card_type.name);
    		
    		$( "input[name='montant']" ).prop('disabled', true).val('');
			
    		
    	}else{
	    	$('#cardType').html('&nbsp;');
	    	$( "input[name='montant']" ).prop('disabled', false).val('');
    	}
    });
  
   
   
   
});

</script>
</head>
<body>

<div id="container">
<div id="content">

<?php

$car_id = "";
$imat = "";
$dep_id = "";
$assist_id = "";
$nom_client = "";
$adresse_client = "";
$date_fin = "";
$date_start = "";
$checkexist = 1;
$franchise = 2;
$credit = 0;
$contratfromdb = 0;

	

   if (isset($_GET['contratid']))
   {
	   
	   	
	   	
	   	
	   	
	   
	  
      $contratid = $_GET['contratid'];
    $printOnly = 0;
    // Dans le cas où on vient de la recherche d'un contrat par immat, j'ai crypté l'id pour éviter qu'un dépanneur ne vienne tenter de modifier un contrat terminé.
	if ($_GET['print'] == 1) {
		$contratid = $_GET['contratid']/1225;
		$contratid =  substr($contratid, 0, strlen($contratid)-1);
		
		$printOnly = 1;
		//echo $contratid;
		
		}
		
		// On active le decryptage que pour les contrats créés après la mise en place du cryptage
	   $decrypt = 'none';
	   if ($contratid >= 15182) {
		   $decrypt = 'decrypt';
	   	}
     // echo $decrypt;
      
      

      
      
	  $statement = $db->prepare("SELECT `contrats`.contrat_id AS contrat_id, `contrats`.contrat_id_prov AS contrat_id_prov, `contrats`.car_id AS car_id, `contrats`.imat AS imat, `contrats`.dep_id AS dep_id, `contrats`.dep_nom AS dep_nom, `contrats`.dep_nom_back AS dep_nom_back, `contrats`.dep_code AS dep_code, `contrats`.assist_id AS assist_id, `contrats`.ba_id AS ba_id, `contrats`.nom_client AS nom_client, `contrats`.adresse_client AS adresse_client, `contrats`.date_fin AS date_fin, `contrats`.date_start AS date_start, `contrats`.model AS model, `contrats`.acriss AS acriss, `contrats`.km_start AS km_start, `contrats`.prolo AS prolo, `contrats`.start_remarques AS start_remarques, `contrats`.startfuel AS startfuel, `contrats`.damage_start AS damage_start, `contrats`.damage_back AS damage_back, `contrats`.assist_nom AS assist_nom, `contrats`.numref AS numref, `contrats`.cat AS cat, `contrats`.jours AS jours, `contrats`.credit AS credit, `contrats`.montant AS montant, `contrats`.cca AS cca, `contrats`.expdate AS expdate, `contrats`.garantie AS garantie, `contrats`.autor AS autor, `contrats`.franchise AS franchise, `contrats`.client_idnum AS client_idnum, `contrats`.client_prenom AS client_prenom, `contrats`.client_permnum AS client_permnum, `contrats`.client_iddate AS client_iddate, `contrats`.client_idplace AS client_idplace, `contrats`.client_permdate AS client_permdate, `contrats`.client_permplace AS client_permplace, `contrats`.client_idexp AS client_idexp, `contrats`.client_birthplace AS client_birthplace, `contrats`.client_assexp AS client_assexp,  `contrats`.client_CP AS client_CP, `contrats`.client_pays AS client_pays, `contrats`.client_birthdate AS client_birthdate, `contrats`.client_birthpays AS client_birthpays, `contrats`.client_tel AS client_tel, `contrats`.`2ndchauff` AS `2ndchauff`, `contrats`.backdate_fin AS backdate_fin, `contrats`.backdep_nom AS backdep_nom, `contrats`.km_retour AS km_retour, `contrats`.backcaution AS backcaution, `contrats`.backcautionmontant AS backcautionmontant, `contrats`.remarques AS remarques, `contrats`.backfuel AS backfuel, `contrats`.backdep_name AS backdep_name, `contrats`.client_name AS client_name, `contrats`.remarques2 AS remarques2, `contrats`.`nom_client2` AS `nom_client2`, `contrats`.`client_prenom2` AS `client_prenom2`, `contrats`.`adresse_client2` AS `adresse_client2`, `contrats`.`client_CP2` AS `client_CP2`, `contrats`.`client_pays2` AS `client_pays2`, `contrats`.`client_idnum2` AS `client_idnum2`, `contrats`.`client_idplace2` AS `client_idplace2`, `contrats`.`client_iddate2` AS `client_iddate2`, `contrats`.`client_idexp2` AS `client_idexp2`, `contrats`.`client_birthdate2` AS `client_birthdate2`, `contrats`.`client_tel2` AS `client_tel2`, `contrats`.`client_permnum2` AS `client_permnum2`, `contrats`.`client_permplace2` AS `client_permplace2`, `contrats`.`client_permdate2` AS `client_permdate2`, `contrats`.`client_assexp2` AS `client_assexp2`, `contrats`.`client_birthplace2` AS `client_birthplace2`, `contrats`.`client_birthpays2` AS `client_birthpays2`, `bondecommande`.`client_lastname` AS `bdc_client_lastname`, `bondecommande`.`client_firstname` AS `bdc_client_firstname`, `bondecommande`.`client_address` AS `bdc_client_address`, `bondecommande`.`client_cp` AS `bdc_client_cp`, `bondecommande`.`client_city` AS `bdc_client_city`, `bondecommande`.`client_tel` AS `bdc_client_tel`, `bondecommande`.`rental_start` AS `rental_start`, `bondecommande`.`rental_days` AS `rental_days`, assist_cars.`car_km` FROM contrats LEFT JOIN assist_cars ON assist_cars.`contrat` = `contrats`.`id` LEFT JOIN `bondecommande` ON `bondecommande`.`bdc_id` = assist_cars.`bondecommande` WHERE `contrats`.id = :contratid;");
	  $statement->execute(array('contratid' => $contratid));
   }

   if ($row = $statement->fetch(PDO::FETCH_ASSOC))
   {
      if($row['contrat_id'] == 0)
      {
	      $contrat_figer = 0;
	      
	      // Bugfix: Si on bloque un véhicule A puis un autre véhicule B sans avoir clôturé le contrat A, alors on a un soucis avec le num de contrat: A et B auront le même numéro de contrat définitif issus du numéro de contrat provisoire. Pour régler le problème, à chaque ouverture (édition) du contrat, on revérifie quel est le dernier numéro de contrat dispo chez le dépanneur
	      

			$statement2 = $db->prepare("SELECT * FROM assist_depaneurs WHERE id = :dep_id;");
			$statement2->execute(array('dep_id' => $row['dep_id']));
			
			
			$row2 = $statement2->fetch(PDO::FETCH_ASSOC);
			
			$statement2 = null;
			
			$dep_current = $row2['nocontrat_current'];
	      
	      
	      
         $contrat_id = $dep_current += 1;
     
        
         
         
      }
      else if ($row['contrat_id'] != 0) 
      {
         $contrat_id = $row['contrat_id'];
         $contrat_figer = 1;
         $contratfromdb = $row['contrat_id'];
      }
      if ($row['contrat_id_lang'] == 0)
      {
         if ($_SESSION['lg'] == 'fr')
         {
            $contratidlang =  269;
         } 
         if ($_SESSION['lg'] == 'nl')
         {
            $contratidlang =  265;
         }
      }
      else if ($row['contrat_id_lang'] != 0)
      {
         $contratidlang =  $row['contrat_id_lang'];
      }
      
      $car_id = $row['car_id'];
      $imat = $row['imat'];
      //echo "Info Vehicule :<br>ID : ".$row['car_id']."<br>imat : ".$row['imat'];
      //echo "<br>ID contrat : ".$contratid."<br>";
      $dep_id = $row['dep_id'];
      $dep_nom = $row['dep_nom'];
      $dep_nom_back = $row['dep_nom_back'];
      $dep_code = $row['dep_code'];
      $assist_id = $row['assist_id'];
      $ba_id = $row['ba_id'];
      $nom_client = $row['nom_client'];
      

       
      	if (strlen($nom_client) < 1) {
	      	
	      	$nom_client = $row['bdc_client_lastname'];
      	}
      $adresse_client = $row['adresse_client'];
      	if ($adresse_client == '') {
	      	$adresse_client = $row['bdc_client_address'];
      	}
         
     
      
      $date_fin = $row['date_fin'];
       if ($date_fin == '') {
	  	$date_fin = date('Y-m-d H:i', strtotime($row['rental_start']. ' + '.$row['rental_days'].' days'));
      }
      if (date('H:i:s', strtotime($date_fin)) == '00:00:00') {
	      $date_fin = date('Y-m-d', strtotime($date_fin)).' '.date('H:i:s');
      }
      
      //echo 'x: '.$date_fin;
      
      $date_start = $row['date_start'];
      if ($date_start == '') {
	  	$date_start = $row['rental_start'];
      }
      
      if (date('H:i:s', strtotime($date_start)) == '00:00:00') {
	      $date_start = date('Y-m-d', strtotime($date_start)).' '.date('H:i:s');
      }
      
      $model = $row['model'];
      $acriss = $row['acriss'];
      $km_start = $row['km_start'];
      if (!$km_start) {
	    
	    $km_start = $row['car_km'];
	      
      	}
      
      $prolo = $row['prolo'];
     
      $start_remarques = $row['start_remarques'];
      $startfuel = $row['startfuel'];
      if (!$startfuel) {
	      $startfuel = 8;
      }
      $damage_start = $row['damage_start'];
      $damage_back = $row['damage_back'];
      $assist_nom = $row['assist_nom'];
      $numref = $row['numref'];
      $cat = $row['cat'];
      $jours = $row['jours'];
      $credit = $row['credit'];
      $montant = $row['montant'];
      
      
      $cca = $row['cca'];
      
      $expdate = $row['expdate'];
      $garantie = $row['garantie'];
      $autor = $row['autor'];
      $franchise = $row['franchise'];
      $client_idnum = $row['client_idnum'];
      $client_prenom = $row['client_prenom'];
      if ($client_prenom == '') {
	      	$client_prenom = $row['bdc_client_firstname'];
      	}
      $client_permnum = $row['client_permnum'];
      $client_iddate = $row['client_iddate'];
      $client_idplace = $row['client_idplace'];
      $client_permdate = $row['client_permdate'];
      $client_permplace = $row['client_permplace'];
      $client_idexp = $row['client_idexp'];
      $client_birthplace = $row['client_birthplace'];
      $client_assexp = $row['client_assexp'];
      $client_CP = $row['client_CP'];
       if ($client_CP == 0) {
	      	$client_CP = $row['bdc_client_cp'];
      	}
       
    
      
      $client_pays = $row['client_pays'];
      $client_birthdate = $row['client_birthdate'];
      $client_birthpays = $row['client_birthpays'];
      $client_tel = $row['client_tel'];
       if ($client_tel == '') {
	      	$client_tel = $row['bdc_client_tel'];
      	}
       
    $nom_client2 = $row['nom_client2'];
	$client_prenom2 = $row['client_prenom2'];
	$adresse_client2 = $row['adresse_client2'];
	$client_CP2 = $row['client_CP2'];
	$client_pays2 = $row['client_pays2'];
	
	$client_idnum2 = $row['client_idnum2'];
	$client_idplace2 = $row['client_idplace2'];
	$client_iddate2 = $row['client_iddate2'];
	$client_idexp2 = $row['client_idexp2'];
	$client_birthdate2 = $row['client_birthdate2'];
	$client_tel2 = $row['client_tel2'];
	
	$client_permnum2 = $row['client_permnum2'];
	$client_permplace2 = $row['client_permplace2'];
	$client_permdate2 = $row['client_permdate2'];
	$client_assexp2 = $row['client_assexp2'];
	$client_birthplace2 = $row['client_birthplace2'];
	$client_birthpays2 = $row['client_birthpays2'];
      
      
      $ndchauff = $row['2ndchauff'];
      $backdate_fin = $row['backdate_fin'];
      $backdep_nom = $row['backdep_nom'];
      $km_retour = $row['km_retour'];
      $backcaution = $row['backcaution'];
      $backcautionmontant = $row['backcautionmontant'];
      $remarques = $row['remarques'];
      $backfuel = $row['backfuel'];
      $backdep_name = $row['backdep_name'];
      $client_name = $row['client_name'];
      $remarques2 = $row['remarques2'];
   }
?>

<form name='contrat' action='contrat_post.php' method='post' oninput="startfuelamount.value=startfuel.value;backfuelamount.value=backfuel.value">
   <input type='hidden' name ='car_id' value="<?php echo $car_id; ?>"/>
   <input type='hidden' name ='dep_id' value="<?php echo $dep_id; ?>"/>
   <input type='hidden' name ='assist_id' value="<?php echo $_GET["assistid"]; ?>"/>
   <input type='hidden' name ='contrat_id' value="<?php echo $contratid; ?>"/>
   <input type="hidden" name ='imat' value="<?php echo $imat; ?>"/>
   <input type="hidden" name ='contratid' value="<?php echo $contrat_id; ?>"/><!-- contrat_id from Database (XX ou bien 0 si contrat provisoire) -->
   <input type="hidden" name ='contratidlang' value="<?php echo $contratidlang; ?>"/>
   <!-- 1024 x 1400 -->
   <div id="container" class="print">
      <div class="row" id="row1">
         <div class="col" id="col1">
            <div class="row1" id="row1">
               <img src='img/logo-facture.png'/>
            </div>
            <div class="row1" id="row2">
               Europcar Registered Offices - Rue Saint Denis, 281 - 1190 Brussels<br>
               Europcar Business Offices - Weiveldlaan, 8 - 1930 Zaventem
            </div>
         </div>
         <div class="col" id="col2">
            <div class="row1" id="row1" style="text-align: center;">
               <?=$co_title?>
            </div>
            <div class="row1" id="row2">
	            
               &nbsp;<?=$co_nocontrat?>  <?php
	              if ($row['contrat_id'] == 0) {
		              echo ' <span style="color:red;" class="tohideprint">PROVISOIRE</span>';
	              }
	              ?> : <?php echo $dep_code." ".$contrat_id; ?>
              
            </div>
            <div class="row1" id="row3">
               &nbsp;<?=$co_nocontratg?>
            </div>
            <!--<div class="row1" id="row4">
               &nbsp;<?=$co_nores?>
            </div>-->
            <div class="row1" id="row5">
               <div class="col" id="col1">
                  Tel : 00 32 (0)2 709 71 00<br>
                  TVA N° BE 0413.087.168
               </div>
               <div class="col" id="col2">
                  Fax : 00 32(0)2 709 71 10<br>
                  RPM Bruxelles
               </div>
            </div>
         </div>
      </div>
      <div class="row" id="row2">
         <div class="row2 headerrow" id="row1"><?=$co_donneedepart?></div>
         <div class="col" id="col1">
            <div class="row2" id="row1"><?=$co_etatdepart?></div>
            <textarea tabindex="1" name="damage_start" id="damage_start"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?>><?php if ($damage_start == ''){echo '•';}echo $damage_start; ?></textarea>
            <?php if ($printOnly == 0) { ?>
            <input tabindex="2" type="button" onclick="document.getElementById('damage_start').value = '';document.getElementById('damage_back1').value = '';document.getElementById('damage_back2').value = '';" value="<?=$co_supdegats?>"/> <input tabindex="3" type="button" value="<?=$dic_ajouter_degat?>" id="openDegatsBuilder" data-featherlight="/degats_builder.php?s=start?>" data-featherlight-type="iframe">
            <?php } ?>
         </div>
         <div class="col" id="col2">
            <div class="col2" id="row1">
               <div class="row2" id="col1"><?=$co_dateheuredepart?></div>
               <div class="row2" id="col2"><?=$co_dep?></div>
               <div class="row2" id="col3"><?=$co_dateheureretour?></div>
               <div class="row2" id="col4"><?=$co_dep?></div>
            </div>
            <div class="col2" id="row2">
               <div class="row2" id="col1"><input type="text" tabindex="4" name='date_start' id="date_start" value="<?php echo SQLDateTimetoDDMMYYHHMM($date_start); ?>" <?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> /></div>
               <div class="row2" id="col2"><input type="text" tabindex="5" name='dep_nom' class="text1" value="<?php echo $dep_nom; ?>" readonly /></div>
               <div class="row2" id="col3"><input type="text" tabindex="6" name='date_fin' id="date_fin" value="<?php echo SQLDateTimetoDDMMYYHHMM($date_fin); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> /></div>
               <div class="row2" id="col4"><input type="text" tabindex="7" name="dep_nom_back" class="text1" value="<?php if ($dep_nom_back == '') { echo $dep_nom; }else{ echo $dep_nom_back; } ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> /></div>
            </div>
            <div class="col2" id="row3">
               <div class="row2" id="col1"><?=$co_noplaque?></div>
               <div class="row2" id="col2"><?=$co_type?></div>
               <div class="row2" id="col3"><?=$co_kmdepart?></div>
               <div class="row2" id="col4"></div>
               <div class="row2" id="col5"><?=$co_etatplein?></div>
               <!--<div class="row2" id="col6"></div>-->
            </div>
            <div class="col2" id="row4">
               <div class="row2" id="col1"><input type="text" tabindex="8" class="text1" value="<?php echo $imat; ?>" readonly></div>
               <div class="row2" id="col2"><input type="text" tabindex="9" name='model' class="text1" value="<?php echo $model; ?>" readonly></div>
               <div class="row2" id="col3"><input type="text" tabindex="10" name='km_start' id="km_start" class="text1" value="<?php echo $km_start; ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
               <div class="row2" id="col4"><!-- <input style="margin-top:15px;" type="checkbox" name='prolo' value='1' <?php if($prolo == 1)echo "checked"; ?> > --></div>
               <div class="row2" id="col5"><input type="range" tabindex="11" name="startfuel" class="fuel" id="startfuel" min="0" max="8" step="1" value="<?php echo $startfuel; ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?>><output name="startfuelamount" for="startfuel" class="fuelindicator"><?php echo $startfuel; ?></output></div>
               <!--<div class="row2" id="col6"></div>-->
            </div>
            <div class="col2" id="row5">
               <div class="row2" id="col1">
                  <div class="col2" id="row1"><?=$co_remarques?></div>
                  <div class="col2" id="row2"><textarea tabindex="12" cols="10" rows="4" name='start_remarques' class="text10" style="height:30px;"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ><?php echo $start_remarques; ?></textarea></div>
               </div>
               <div class="row2" id="col2">
                  <div class="col2" id="row1"><?=$co_signclient?></div>
                  <div class="col2" id="row2"></div>
               </div>
            </div>
         </div>
      </div>
      <div class="row" id="row3">
         <div class="row3 headerrow" id="row1"><?=$co_detailsassist?></div>
         <div class="col" id="col1">
            <div class="col1" id="row1"><?=$co_nomassist?></div>
            <div class="col1" id="row2"><?=$co_noref?></div>
            <div class="col1" id="row3"><?=$co_cat?></div>
            <div class="col1" id="row4"><?=$co_jours?></div>
         </div>
         <div class="col" id="col2">
            <div class="col2" id="row1"><input type="text" tabindex="13" name='assist_nom' class="text2" value="<?php echo $assist_nom; ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            <div class="col2" id="row2"><input type="text" tabindex="16" name='numref' class="text2" value="<?php echo $numref; ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            <div class="col2" id="row3"><input type="text" tabindex="18" class="text2" value="<?php echo $acriss; ?>" readonly ></div>
            <div class="col2" id="row4"><input tabindex="20" type="text" name='jours' class="text3" value="<?php echo $jours; ?>" <?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
         </div>
         <div class="col" id="col3">
            <div class="col3" id="row1">
               <div class="row1" id="col1"><?=$co_fullcredit?></div>
               <div class="row1" id="col2"><input tabindex="14" type="radio" onclick="document.getElementById('montant').disabled = true;document.getElementById('cca').disabled = true;document.getElementById('expdate').disabled = true;document.getElementById('garantie').disabled = true;document.getElementById('autor').disabled = true;document.getElementById('franchise').disabled = true;" name="credit" value="1" id="credityes" <?php if($credit == 1){echo "checked";} ?> ><b><?=$dic_oui?></b></div>
            </div>
            <div class="col3" id="row2"></div>
            <div class="col3" id="row3"><?=$co_signclient?></div>
            <div class="col3" id="row4"></div>
         </div>
         <div class="col" id="col4">
            <div class="col4" id="row1"><input type="radio" tabindex="15" onclick="document.getElementById('montant').disabled = false;document.getElementById('cca').disabled = false;document.getElementById('expdate').disabled = false;document.getElementById('garantie').disabled = false;document.getElementById('autor').disabled = false;document.getElementById('franchise').disabled = false;" name="credit" value="0" id="creditno" <?php if($credit == 0){echo "checked";} ?> ><b><?=$dic_non?></b></div>
            <div class="col4" id="row2"><?=$co_cash?></div>
            <div class="col4" id="row3"><?=$co_cca?></div>
            <div class="col4" id="row4"><?=$co_exp?></div>
            <div class="col4" id="row5"><?=$co_autor?></div>
         </div>
         <div class="col" id="col5">
            <div class="col5" id="row1"></div>
            <div class="col5" id="row2"><input tabindex="17" type="text" name='montant' class="text2" id="montant" value="<?php echo $montant; ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            <div class="col5" id="row3">
	            <div id="cardType" style="float:left; width:30%;">
					&nbsp;
	            </div>
	            <div style="float:left; width:70%;">
	            	<input tabindex="19" type="text" name='cca' id="cca" style="width:180px;" value="<?php echo encrypt_decrypt($decrypt, $cca); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> >
	            </div>
	        </div>
            <div class="col5" id="row4">
               <div class="row4" id="col1"><input type="text" tabindex="21" name='expdate' id="expdate" class="text4" value="<?php echo encrypt_decrypt($decrypt, $expdate); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
               <div class="row4" id="col2"><?=$co_garantie?></div>
               <div class="row4" id="col3"><input type="text" tabindex="22" name='garantie' id="garantie" class="text4" value="<?php echo $garantie; ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            </div>
            <div class="col5" id="row5">
               <div class="row5" id="col1"><input type="text" tabindex="23" name='autor' id="autor" class="text4" value="<?php echo encrypt_decrypt($decrypt, $autor); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
               <div class="row5" id="col2"><?=$co_franchise?></div>
               <div class="row5" id="col3"><select tabindex="24" name="franchise" id="franchise"><option value="0" <?php if($franchise == 0){echo "selected";} else if($franchise == 2 && $acriss == 'ECMR'){echo "selected";} ?> >650€</option><option value="1" <?php if($franchise == 1){echo "selected";} else if($franchise == 2 && $acriss == 'CCMR'){echo "selected";}?> >850€</option></select></div>
            </div>
         </div>
         <div class="row3" id="row2"><b><?=$co_clientresp?></b></div>
      </div>
      <div class="row" id="row4">
         <div class="row4 headerrow" id="row1"><?=$co_detailschauff?></div>
         <div class="col" id="col1">
            <div class="col1" id="row1"><?=$co_nom?></div>
            <div class="col1" id="row2"><input type="text" tabindex="25" name='nom_client' class="text5_smaller" value="<?php echo encrypt_decrypt($decrypt, $nom_client); ?>"<?php if($nom_client != '') { ?> readonly<?php } ?><?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            <div class="col1" id="row3"><?=$co_noid?></div>
            <div class="col1" id="row4"><input type="text" tabindex="30" name='client_idnum' class="text5_smaller" value="<?php echo encrypt_decrypt($decrypt, $client_idnum); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            <div class="col1" id="row5"><?=$co_noperm?></div>
            <div class="col1" id="row6"><input type="text" tabindex="38" name='client_permnum' class="text5_smaller" value="<?php echo encrypt_decrypt($decrypt, $client_permnum); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
         </div>
         <div class="col" id="col2">
            <div class="col2" id="row1"><?=$co_prenom?></div>
            <div class="col2" id="row2"><input type="text" tabindex="26" name='client_prenom' class="text5_smaller" value="<?php echo encrypt_decrypt($decrypt, $client_prenom); ?>"<?php if($client_prenom != '') { ?> readonly<?php } ?><?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            <div class="col2" id="row3">
               <div class="row3" id="col1"><?=$co_oba?></div>
               <div class="row3" id="col2"><?=$co_oble?></div>
            </div>
            
            
            <div class="col2" id="row4">
	            
	            
	            <input type="text" tabindex="31" name='client_idplace' id='client_idplace' class="text7_smaller" value="<?php echo encrypt_decrypt($decrypt, $client_idplace); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> >
	            
	            <input type="text" tabindex="32" name='client_iddate' id='client_iddate' class="text7_smaller" value="<?php echo SQLDatetoDDMMYY(encrypt_decrypt($decrypt, $client_iddate)); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> >
	            
	            
	            </div>
            
            
            
            <div class="col2" id="row5">
               <div class="row5" id="col1"><?=$co_oba?></div>
               <div class="row5" id="col2"><?=$co_oble?></div>
            </div>
            
            	<div class="col2" id="row6">
	            	
	            	
	            	<input type="text" tabindex="39" name='client_permplace' id='client_permplace' class="text7_smaller" value="<?php echo encrypt_decrypt($decrypt, $client_permplace); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> >
	            	
	            	<input type="text" tabindex="40" name='client_permdate' id='client_permdate' class="text7_smaller" value="<?php echo SQLDatetoDDMMYY(encrypt_decrypt($decrypt, $client_permdate)); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> >
	            
	            
	            
	            
	            </div>
         </div>
         <div class="col" id="col3">
            <div class="col3" id="row1"><?=$co_adresse?></div>
            <div class="col3" id="row2"><input type="text" tabindex="27" name='adresse_client' class="text6_smaller" value="<?php echo encrypt_decrypt($decrypt, $adresse_client); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            <div class="col3" id="row3">
               <div class="row3" id="col1"><?=$co_expiration?></div>
               <div class="row3" id="col2"><?=$co_lieunaissance?></div>
            </div>
            <div class="col3" id="row4">
               <div class="row4" id="col1"><input type="text" tabindex="33" name='client_idexp' id='client_idexp' class="text7_smaller" value="<?php echo SQLDatetoDDMMYY(encrypt_decrypt($decrypt, $client_idexp)); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
               <div class="row4" id="col2"><input type="text" tabindex="34" name='client_birthplace' class="text5_smaller" value="<?php echo encrypt_decrypt($decrypt, $client_birthplace); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            </div>
            <div class="col3" id="row5">
               <div class="row5" id="col1"><?=$co_expiration?></div>
               <div class="row5" id="col2"></div>
            </div>
            <div class="col3" id="row6">
               <div class="row6" id="col1"><input type="text" tabindex="41" name='client_assexp' id='client_assexp' class="text7_smaller" value="<?php echo SQLDatetoDDMMYY(encrypt_decrypt($decrypt, $client_assexp)); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
               <div class="row6" id="col2"></div>
            </div>
         </div>
         <div class="col" id="col4">
            <div class="col3" id="row1">
               <div class="row1" id="col1"><?=$co_cp?></div>
               <div class="row1" id="col2"><?=$co_pays?></div>
            </div>
            <div class="col3" id="row2">
               <div class="row2" id="col1"><input type="text" tabindex="28" name='client_CP' class="text8_smaller" value="<?php echo $client_CP; ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
               <div class="row2" id="col2"><input type="text" tabindex="29" name='client_pays' class="text7_smaller" value="<?php echo $client_pays; ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div>
            </div>
            <div class="col3" id="row3">
               <div class="row3" id="col1"><?=$co_datenaissance?></div>
               <div class="row3" id="col2"><?=$co_pays?></div>
               <div class="row3" id="col3"><?=$co_tel?></div>
            </div>
            <div class="col3" id="row4">
               <div class="row4" id="col1"><div class="row2" id="col2"><input type="text" tabindex="35" name='client_birthdate' id='client_birthdate' class="text7_smaller" value="<?php echo SQLDatetoDDMMYY(encrypt_decrypt($decrypt, $client_birthdate)); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div></div>
               <div class="row4" id="col2"><div class="row2" id="col2"><input type="text" tabindex="36" name='client_birthpays' class="text7_smaller" value="<?php echo encrypt_decrypt($decrypt, $client_birthpays); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div></div>
               <div class="row4" id="col3"><div class="row2" id="col2"><input type="text" tabindex="37" name='client_tel' id='client_tel' class="text7_smaller" value="<?php echo encrypt_decrypt($decrypt, $client_tel); ?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></div></div>
            </div>
            <div class="col3" id="row5"></div>
            <div class="col3" id="row6">
               <div class="row6" id="col1"><?=$eme?> chauffeur</div>

               
			   <?php

               
               // on vérifie que le conducteur additionnel est gratuit ou payant pour cette assistance	
				$statement = $db->prepare("SELECT `assist_depaneurs_conditions`.`dep_add_dr` FROM `assist_depaneurs_conditions` WHERE `assist_depaneurs_conditions`.`id` = ".$ba_id);
				$statement->execute();
				
				foreach($statement as $row) {
					$add_driver_included = $row['dep_add_dr'];
				}

			

               
               
               
               ?>
               
               <div class="row6" id="col2"><input type="radio" tabindex="42" name="2ndchauff" onclick="$('#seconddriver').parent().fadeTo('fase', 1);" value="1" <?php if($ndchauff == 1){echo "checked";} ?>><b><?=$dic_oui?></b><?php if ($add_driver_included == 0) { ?> (37€)<?php } ?></div>
               <div class="row6" id="col3"><input type="radio" tabindex="43" name="2ndchauff" onclick="$('#seconddriver').parent().fadeTo('fase', 0.2); $('#seconddriver input').val('');" value="0" <?php if($ndchauff == 0){echo "checked";} ?>><b><?=$dic_non?></b></div>
            </div>
         </div>
         <div class="col" id="col5">
            <?=$co_donnees2nd?><br>
            
            <style>
	        #seconddriver {
		        
	        } 
	        #seconddriver table {
		        border-top:1px solid #000;
		        }
	         #seconddriver td {
		         padding:1px;
		         border-right:1px solid #000;
		         border-bottom:1px solid #000;
		         }
		         #seconddriver td.noborderright {
			         border-right:0;
			         }
			        #seconddriver td.borderright {
			         border-right:1px solid #000;
			         }
		          #seconddriver table table {
			          border-top:0;
			          }
		        #seconddriver td td {
			        padding:0;
			        
			        border-right:0;
			        border-bottom:0;
			        }
			        #seconddriver td.colspancontainer {
				        padding:0;
			        }
			         #seconddriver td.colspancontainer td {
				         padding:1px;
				         }
	        #seconddriver input {
		        width:100%;
		        box-sizing: border-box;
		        }  
	        </style>
            
            <div id="seconddriver">
            	
            	
            	
            	
            	<table cellpadding="0" cellspacing="0" width="100%">
	            	<tr>
		            	<td style="width:25%;"><?=$co_nom?></td>
		            	<td style="width:25%;"><?=$co_prenom?></td>
		            	<td style="width:30%;"><?=$co_adresse?></td>
		            	<td style="width:10%;">CP</td>
		            	<td style="width:10%;" class="noborderright"><?=$co_pays?></td>
	            	</tr>
	            	<tr>
		            	<td><input type="text" tabindex="44" name="nom_client2" value="<?=encrypt_decrypt($decrypt, $nom_client2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
		            	<td><input type="text" tabindex="45" name="client_prenom2" value="<?=encrypt_decrypt($decrypt, $client_prenom2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
		            	<td><input type="text" tabindex="46" name="adresse_client2" value="<?=encrypt_decrypt($decrypt, $adresse_client2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
		            	<td><input type="text" tabindex="47" name="client_CP2" value="<?=$client_CP2?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
		            	<td class="noborderright"><input type="text" tabindex="48" name="client_pays2" value="<?=$client_pays2?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
	            	</tr>
	            	<tr>
		            	<td><?=$co_noid?></td>
		            	<td class="colspancontainer">
			            	<table cellpadding="0" cellspacing="0" width="100%">
				            	<tr>
					            	<td style="width:50%;<?PHP if ($lgstring == 'nl') { ?> font-size:9px;<?PHP } ?>" class="borderright"><?=$co_oba?></td>
					            	<td style="width:50%;<?PHP if ($lgstring == 'nl') { ?> font-size:9px;<?PHP } ?>"><?=$co_oble?></td>
				            	</tr>
			            	</table>
		            	</td>
		            	<td class="colspancontainer">
			            	<table cellpadding="0" cellspacing="0" width="100%">
				            	<tr>
					            	<td style="width:40%;" class="borderright"><?=$co_expiration?></td>
					            	<td style="width:60%;"><?=$co_datenaissance?></td>
				            	</tr>
			            	</table>
		            	</td>
		            	<td colspan="2" class="noborderright"><?=$co_tel?></td>
	            	</tr>
	            	<tr>
		            	<td><input type="text" tabindex="49" name="client_idnum2" value="<?=encrypt_decrypt($decrypt, $client_idnum2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
		            	<td class="colspancontainer">
			            	<table cellpadding="0" cellspacing="0" width="100%">
			            		<tr>
					            	<td style="width:50%;" class="borderright"><input type="text" tabindex="50" name="client_idplace2" value="<?=encrypt_decrypt($decrypt, $client_idplace2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
					            	<td style="width:50%;"><input type="text" tabindex="51" name="client_iddate2" id="client_iddate2" value="<?=encrypt_decrypt($decrypt, $client_iddate2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
			            		</tr>
			            	</table>
		            	</td>
		            	<td class="colspancontainer">
			            	<table cellpadding="0" cellspacing="0" width="100%">
				            	<tr>
					            	<td style="width:40%;" class="borderright"><input type="text" tabindex="52" name="client_idexp2" id="client_idexp2" value="<?=encrypt_decrypt($decrypt, $client_idexp2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
					            	<td style="width:60%;"><input type="text" tabindex="53" name="client_birthdate2" id="client_birthdate2" value="<?=encrypt_decrypt($decrypt, $client_birthdate2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
				            	</tr>
			            	</table>
		            	</td>
		            	<td colspan="2" class="noborderright"><input type="text" name="client_tel2" tabindex="54" value="<?=encrypt_decrypt($decrypt, $client_tel2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
	            	</tr>
	            	<tr>
		            	<td><?=$co_noperm?></td>
		            	<td class="colspancontainer">
			            	<table cellpadding="0" cellspacing="0" width="100%">
				            	<tr>
					            	<td style="width:50%;<?PHP if ($lgstring == 'nl') { ?> font-size:9px;<?PHP } ?>" class="borderright"><?=$co_oba?></td>
					            	<td style="width:50%;<?PHP if ($lgstring == 'nl') { ?> font-size:9px;<?PHP } ?>"><?=$co_oble?></td>
				            	</tr>
			            	</table>
		            	</td>
		            	<td class="colspancontainer">
			            	<table cellpadding="0" cellspacing="0" width="100%">
				            	<tr>
					            	<td style="width:40%;" class="borderright"><?=$co_expiration?></td>
					            	<td style="width:60%;"><?=$co_lieunaissance?></td>
				            	</tr>
			            	</table>
		            	</td>
		            	<td colspan="2" class="noborderright">PAYS DE NAISSANCE</td>
		    
	            	</tr>
	            	<tr>
		            	<td><input type="text" tabindex="55" name="client_permnum2" value="<?=encrypt_decrypt($decrypt, $client_permnum2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
		            	<td class="colspancontainer">
			            	<table cellpadding="0" cellspacing="0" width="100%">
			            		<tr>
					            	<td style="width:50%;" class="borderright"><input type="text" tabindex="56" name="client_permplace2" value="<?=encrypt_decrypt($decrypt, $client_permplace2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
					            	<td style="width:50%;"><input type="text" tabindex="57" name="client_permdate2" id="client_permdate2" value="<?=encrypt_decrypt($decrypt, $client_permdate2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
			            		</tr>
			            	</table>
		            	</td>
		            	<td class="colspancontainer">
			            	<table cellpadding="0" cellspacing="0" width="100%">
				            	<tr>
					            	<td style="width:40%;" class="borderright"><input type="text" tabindex="58" name="client_assexp2" id="client_assexp2" value="<?=$client_assexp2?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
					            	<td style="width:60%;"><input type="text" tabindex="59" name="client_birthplace2" value="<?=encrypt_decrypt($decrypt, $client_birthplace2)?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
				            	</tr>
			            	</table>
		            	</td>
		            	<td colspan="2" class="noborderright"><input type="text" tabindex="60" name="client_birthpays2" value="<?=$client_birthpays2?>"<?php if ($printOnly == 1 || $_GET['retour'] == 1) { ?> readonly<?php } ?> ></td>
	            	</tr>
            	</table>
            	
            	
            	
            	
            </div>
            
            
         </div>
         <div class="col" id="col6">
            <?=$co_conditions?>
         </div>
      </div>
     
      <div class="row" id="row5" style="border-top: 5px solid #000; <?php if($_GET['retour'] != 1 && $_GET['print'] != 1) { echo 'opacity:0.2'; }?>">
         <div class="row5 headerrow" id="row1"><?=$co_donneesretour?></div>
         <div class="col" id="col1">
            <div class="row2" id="row1"><?=$co_etatretour?></div>
            <span style="font-size:12px;" class="tohideprint"><?=$co_degatsexist?></span><textarea tabindex="61" style="border:none;height:70px;" id="damage_back1" name="damage_back1" readonly><?php echo $damage_back; ?></textarea>
            <textarea id="damage_back2" name="damage_back2" tabindex="62"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ><?php echo '•'; ?></textarea>
             <?php if ($printOnly == 0) { ?>
            <input type="button" value="<?=$dic_ajouter_degat?>" data-featherlight="/degats_builder.php?s=back" id="openDegatsBackBuilder" data-featherlight-type="iframe">
            <?php } ?>
         </div>
         <div class="col" id="col2">
            <div class="col2" id="row1">
               <div class="row2" id="col1"><?=$co_dateheureretour?></div>
               <div class="row2" id="col2"><?=$co_dep?></div>
               <div class="row2" id="col3"></div>
               <div class="row2" id="col4"></div>
            </div>
            <div class="col2" id="row2">
               <div class="row2" id="col1"><input type="text" tabindex="63" name='backdate_fin' id="backdate_fin" value="<?php echo SQLDateTimetoDDMMYYHHMM($backdate_fin); ?>"<?php if ($printOnly == 1) { ?> readonly<?php } ?> /></div>
               <div class="row2" id="col2"><input type="text" tabindex="64" name='backdep_nom' class="text1" value="<?php echo $backdep_nom; ?>"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ></div>
               <div class="row2" id="col3"></div>
               <div class="row2" id="col4"></div>
            </div>
            <div class="col2" id="row3">
               <div class="row2" id="col1"><?=$co_noplaque?></div>
               <div class="row2" id="col2"><?=$co_type?></div>
               <div class="row2" id="col3"><?=$co_kmretour?></div>
               <div class="row2" id="col4"></div>
               <div class="row2" id="col5" style="width:33.33%; border-right:0;"><?=$dic_cautionretour?></div>
               <!--<div class="row2" id="col6"><?=$co_recup?></div>-->
            </div>
            <div class="col2" id="row4">
               <div class="row2" id="col1"><input type="text" tabindex="65" class="text1" value="<?php echo $imat; ?>" readonly></div>
               <div class="row2" id="col2"><input type="text" tabindex="66" class="text1" value="<?php echo $model; ?>" readonly></div>
               <div class="row2" id="col3"><input type="text" tabindex="67" name='km_retour' id="km_retour" class="text1" value="<?php echo $km_retour; ?>"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ></div>
               <div class="row2" id="col4"><input type="text" tabindex="68" class="text1" value="<?php echo $prolo; ?>"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ></div>
               <div class="row2" id="col5" style="border-right:0;"><input type="radio" tabindex="69" name="backcaution" value="1" <?php if($backcaution == 1){echo "checked";} ?> ><?=$dic_oui?><br><input tabindex="70" type="radio" name="backcaution" value="0" <?php if($backcaution == 0){echo "checked";} ?>><?=$dic_non?></div>
               <div class="row2" id="col6" style="padding-top:5px;"><?=$dic_montant?><br><input tabindex="71" type="text" name="backcautionmontant" value="<?=$backcautionmontant?>" style="width:100%; box-sizing: border-box;"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ></div>
            </div>
            <div class="col2" id="row5">
               <div class="row2" id="col1">
                  <div class="col2" id="row1"><?=$co_remarques?></div>
                  <div class="col2" id="row2"><br><textarea cols="10" rows="4" name='remarques' class="text10" tabindex="72"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ><?php echo $remarques; ?></textarea></div>
               </div>
               <div class="row2" id="col2">
                  <div class="col2" id="row1"><?=$co_signclient?></div>
                  <div class="col2" id="row2"></div>
                  <div class="col2" id="row3"><?=$co_etatplein?></div>
                  <div class="col2" id="row4" style="margin-left:24px;"><input type="range" tabindex="73" name="backfuel" id="backfuel" class="fuel" min="0" max="8" value="<?php echo $backfuel; ?>"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ><output name="backfuelamount" for="backfuel" class="fuelindicator"><?php echo $backfuel; ?></output></div>
               </div>
            </div>
         </div>
      </div>
      <div class="row" id="row6" <?php if($_GET['retour'] != 1 && $_GET['print'] != 1) { echo 'style="opacity:0.2"'; }?>>
         <div class="row6 headerrow" id="row1"><?=$co_datlieu?></div>
         <div class="row6" id="row2">
            <div class="row2" id="col1"><?=$co_depnom?></div>
            <div class="row2" id="col2"><?=$co_depsign?></div>
            <div class="row2" id="col3"><?=$co_nomclient?></div>
            <div class="row2" id="col4"></div>
         </div>
         <div class="row6" id="row3">
            <div class="row3" id="col1"><input type="text" name='backdep_name' class="text6" value="<?php echo $backdep_name; ?>"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ></div>
            <div class="row3" id="col2"></div>
            <div class="row3" id="col3"><input type="text" name='client_name' class="text5" value="<?php echo encrypt_decrypt($decrypt, $client_name); ?>"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ></div>
            <div class="row3" id="col4"></div>
         </div>
         <div class="row6" id="row4">
            <div class="row4" id="col1"><?=$co_date?></div>
            <div class="row4" id="col2"></div>
         </div>
         <div class="row6" id="row5">
            <div class="row5" id="col1"><?=$co_remarques?><textarea cols="10" rows="4" name='remarques2' class="text11"<?php if ($printOnly == 1) { ?> readonly<?php } ?> ><?php echo $remarques2; ?></textarea></div>
            <div class="row5" id="col2"><?=$co_conditions3?></div>
         </div>
      </div>
      <input type="hidden" name="retour" id="retour" value="<?=$_GET['retour']?>" />
      <input type="hidden" name="definitivement" id="definitivement" value="" />
      
      <?php if ($printOnly == 0) { ?>
      
      	<?php if($contrat_figer == 0) { ?>
      	
      	<input type='button' name='contrat' value="<?=$co_imprimer?>" onclick="sendForm(1)" class="tohide"/>     
		<input type='button' name='contrat' value="<?=$dic_temp_save?>" onclick="sendForm(0)" class="tohide"/> 
      	<?php }else{ ?>
      	<?php

      	if (($_SESSION['zeType'] == 'depanneur' && $contratfromdb > 0 || $_SESSION['zeType'] == 'admin' && $contratfromdb > 0) && $_GET['retour'] == 1)
			{
				echo '<input type="button" name="contrat" value="'._('Clôturer définitivement ce contrat').'" onclick="sendForm(4)" class="tohide"/> ';
				}
      	?>
      	<?php if ($_GET['retour'] != 1) { ?><input type='button' name='contrat' value="<?=_('Valider le contrat au départ du véhicule')?>" onclick="sendForm(3)" class="tohide"/> <?php } ?>
      	<input type='button' name='contrat' value="<?=$co_imprimer?>" onclick="window.print();" class="tohide"/>
      	<input type='button' name='contrat' value="<?=_("Sauvegarder provisoirement")?>" onclick="sendForm(2)" class="tohide"/>
      	<?php } ?>
		
		<input type='button' name='contrat' value="<?=$dic_bdc_retour?>" onclick="location.href='depanneur_details.php?id=<?=$dep_id?>'" class="tohide"/>
 
      <?php }else{ ?>
      
      
      <input type='button' name='contrat' value="<?=$co_imprimer?>" onclick="window.print();" class="tohide"/>
      <?php } ?>
      

      
      
   </div>
</form>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="ie">Votre navigateur n'est pas compatible, veuillez le mettre à jour, ou utiliser un navigateur compatible (Google Chrome, Firefox, Internet Explorer version 10 et ultérieur)</div>
<script src="js/rangeslider.min.js"></script>
<script>
$('.fuel').rangeslider();


<?php if($_GET['print_after_validation'] == 1) { ?>
	$( document ).ready(function() {
	    window.print();
	});
<?php } ?>

function _calculateDiffYears(fieldName) {
	
	var getDate = $("#"+fieldName).val().split("/");
	var getDate = new Date(getDate[2], getDate[1] - 1, getDate[0]);
	
    var ageDifMs = Date.now() - getDate.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

// traitement formulaire:
function sendForm(definitivement) {
	
	var error = 0;
	
	// Vérification âge conducteur	
	if (_calculateDiffYears('client_birthdate') < 18) {
		error = 1;
		alert("Le conducteur doit avoir minimum 18 ans");
		return false;
	}

	// Vérification âge permis (min 1 an)
	if (_calculateDiffYears('client_permdate') < 1) {
		error = 1;
		alert("Le conducteur doit avoir son permis depuis au moins 1 an");
		return false;
	}

	if ($('#date_fin').val() == '' && definitivement == 1) {
		error = 1;
		alert("Veuillez renseigner la date de retour du véhicule");
		return false;
	}
	
	if ($('#client_tel').val() == '' && definitivement == 1) {
		error = 1;
		alert("Veuillez renseigner le numéro de téléphone du client");
		return false;
	}
	
	var resultCC = $('#cca').validateCreditCard();
	if (resultCC.valid == false && ($('#cca').val() != '' && $('#cca').val() != '0')) {
		error = 1;
		alert("Numéro de carte de crédit non valide");
		return false;
	}
	
	if ($('#cca').val() != '' && $('#cca').val() != '0') {
		if ($('#expdate').val() == '') {
			error = 1;
			alert("Veuillez renseigner la date d'expiration de la carte de crédit");
			return false;
		}
		if ($('#autor').val() == '' || $('#autor').val() == '0') {
			error = 1;
			alert("Veuillez renseigner le numéro d'autorisation de la carte de crédit");
			return false;
		}
	}
	
	
	
	 // Obligatoire d'indiquer les km au départ
		if ($('#km_start').val() == '' || $('#km_start').val() == '0') {
			error = 1;
			alert("Veuillez renseigner le kilométrage du véhicule au départ");
			return false;
		}
	
	
	if (definitivement == 1) {
		$('#definitivement').val(1);
	}else if (definitivement == 2) {
		$('#definitivement').val(2);
	}else if (definitivement == 3) {
		$('#definitivement').val(3);
	}else if (definitivement == 4) {
		
		// Obligatoire d'indiquer les km au retour
		if ($('#km_retour').val() == '' || $('#km_retour').val() == '0') {
			error = 1;
			alert("Veuillez renseigner le kilométrage du véhicule au retour");
			return false;
		}
		
		//console.log(Number($('#km_retour').val()) + " < " + Number($('#km_start').val()));
		if (Number($('#km_retour').val()) < Number($('#km_start').val())) {
			error = 1;
			alert("Le kilométrage encodé au retour du véhicule doit être supérieur au kilométrage enregistré au départ du véhicule");
			return false;
			}
		
		
		
		
		$('#definitivement').val(4);
	}else{
		$('#definitivement').val(0);
	}
	
	
	
	if (error == 0) {
		document.contrat.submit();
	}
}



</script>
</body>
</html>
<?php
}

else 
{  
   session_name('SESSIONcontrat');
   session_start();
   $_SESSION['contrat_id'] = $contratid;
   print "<script language='Javascript'>document.location.href='index.php';</script>";
}
?>
<?php $statement = null; $db = null; ?>