#!/usr/local/bin/php.ORIG.5_6
<?php
	// Pour se conformer à GDPR, on exécute ce CRON pour effacer les vieux contrats et BDC (plus de 3 mois) afin de limiter la fuite de données personnelles en cas de piratage. Les anciens contrats sont stockés chez Europcar, dans Greenway.
	
	include('/home/chaussur/assistv2.previewstage.net/inc/connexion-pdo.php');
	include('/home/chaussur/assistv2.previewstage.net/inc/functions.php');
	
	try {
		$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		
		$statement = $db->prepare("DELETE FROM `contrats` WHERE `date_fin` <= NOW() - INTERVAL 3 MONTH");
		$statement->execute();
		
		$statement = $db->prepare("DELETE FROM bondecommande WHERE DATE_ADD(rental_start, INTERVAL rental_days DAY) <= NOW() - INTERVAL 3 MONTH");
		$statement->execute();
					
					
		$db = null;
	
	} catch (PDOException $e) {
	    print "Erreur !: " . $e->getMessage() . "<br/>";
	    die();
	}	
	
	?>