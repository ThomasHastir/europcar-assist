#!/usr/local/bin/php.ORIG.5_6
<?php
	/* ------------------------------------------------------
		
	All daily reports sent by email are here:
	
	1/ Rapport journalier par e-mail: Contrats prolongés la veille - Lister plaque + nom dépanneur + durée
	2/ Rapport journalier par e-mail: Véhicules shop / vente au total (et non uniquement ceux de la veille) - Lister plaque + nom dépanneur
	3/ Rapport journalier par e-mail: Lister les voitures qui sont rentrées et ressorties le jour même
	4/ Rapport mensuel par e-mail: Contrats clôturés du mois précédent classé par dépanneur (Avec numéro de plaque + numéro de contrat)
	
	------------------------------------------------------ */
	
	
	include('/home/chaussur/assistv2.previewstage.net/inc/connexion-pdo.php');
	include('/home/chaussur/assistv2.previewstage.net/inc/functions.php');
	require '/home/chaussur/assistv2.previewstage.net/inc/phpmailer/PHPMailerAutoload.php';
	
	
	$array = array(1, 2, 3, 4);
	$frequency = array('none', 'daily', 'daily', 'daily', 'monthly');
	


	
	try {
		$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
		
		foreach ($array as $r){
		
	
			// On vérifie que le mail n'a pas déjà été envoyé aujourd'hui ou ce mois-ci (si monthly report) en regardant dans les logs
			$sql = "SELECT * FROM `log_crons` WHERE DATE(`log_date`) = DATE(NOW()) AND `log_mail_id` = :r";
			if ($frequency[$r] == 'monthly') {
				$sql = "SELECT * FROM `log_crons` WHERE MONTH(`log_date`) = MONTH(NOW()) AND YEAR(`log_date`) = YEAR(NOW()) AND `log_mail_id` = :r";
				}
				
			$statement = $db->prepare($sql);
			$statement->execute(array('r' => $r));
			
			$isExisting = $statement->rowCount();
			
			if ($isExisting > 0) {
				echo $frequency[$r].' report # '.$r.' already sent !';
				continue;
				}
			
	
			switch ($r) {
				
				
				case 1:
					
					/* ------------------------------------------------------
						
					1/ Rapport journalier par e-mail: Contrats prolongés la veille - Lister plaque + nom dépanneur + durée
					
					------------------------------------------------------ */
		
					$statement = $db->prepare("SELECT `p`.`log_date`, `p`.`log_days` AS `days`, `p`.`log_new_date` AS `new_date`, `p`.`log_contrat_id`,  `c`.`imat` AS `imat`, `c`.`dep_nom` AS `dep_nom`, `c`.`assist_nom` AS `assist_nom` FROM `log_prolongations` `p` LEFT JOIN `contrats` `c` ON `p`.`log_contrat_id` = `c`.`id` WHERE DATE(`p`.`log_date`) = DATE(NOW() - INTERVAL 1 DAY) ORDER BY `c`.`dep_nom`;");
					$statement->execute();
					
					$isExisting = $statement->rowCount();
					
					
					
					if ($isExisting > 0) {
						
						$subject = "[ASSIST] Daily report : All contracts extended yesterday";
						
						$message = '<p>Dear BE-h24,</p>';
						$message .= '<p>Please find below a list of all contracts that have been extended yesterday.</p>';
					
						$message .= '<table width="100%" cellpadding="4" cellspacing="0" border="0">';
							$message .= '<tr>';
							$message .= '<th align="left">Licence plate</th>';
							$message .= '<th align="left">Added days</th>';
							$message .= '<th align="left">New car return date</th>';
							$message .= '<th align="left">Troubleshooter</th>';
							$message .= '<th align="left">Assistance</th>';
							$message .= '</tr>';
							
						$i = 0;
					
						foreach($statement as $row) {
							
							$i++;
							if ($i % 2 == 0) {$odd = 1;}else{$odd = 0;}
							
							$message .= '<tr>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['imat'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['days'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.SQLDatetoDDMMYY($row['new_date']).'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['dep_nom'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['assist_nom'].'</td>';
							$message .= '</tr>';
							}
						
						$message .= '</table>';
						}
					break;
					
				case 2:
					
					/* ------------------------------------------------------
						
					2/ Rapport journalier par e-mail: Véhicules shop / vente au total (et non uniquement ceux de la veille) - Lister plaque + nom dépanneur
					
					------------------------------------------------------ */
		
					$statement = $db->prepare("SELECT `car_immatriculation`, `car_marque`, `car_model`, `assist_depaneurs`.`dep_nom`, CASE `car_status` when 5  then 'SHOP' when 6  then 'VENTE' END as `car_status` FROM `assist_cars` LEFT JOIN assist_depaneurs ON `assist_cars`.car_depaneur = assist_depaneurs.`id` WHERE (car_status_BB = 1 AND car_status = 5) OR car_status = 6;");
					$statement->execute();
					
					$isExisting = $statement->rowCount();
					
					
					
					if ($isExisting > 0) {
						
						$subject = "[ASSIST] Daily report : All cars with status SHOP or VENTE";
						
						$message = '<p>Dear BE-h24,</p>';
						$message .= '<p>Please find below a list of all cars with the status equal to SHOP or VENTE.</p>';
					
						$message .= '<table width="100%" cellpadding="4" cellspacing="0" border="0">';
							$message .= '<tr>';
							$message .= '<th align="left">Licence plate</th>';
							$message .= '<th align="left">Car brand</th>';
							$message .= '<th align="left">Model</th>';
							$message .= '<th align="left">Troubleshooter</th>';
							$message .= '<th align="left">Status</th>';
							$message .= '</tr>';
							
						$i = 0;
					
						foreach($statement as $row) {
							
							$i++;
							if ($i % 2 == 0) {$odd = 1;}else{$odd = 0;}
							
							$message .= '<tr>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['car_immatriculation'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['car_marque'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['car_model'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['dep_nom'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['car_status'].'</td>';
							$message .= '</tr>';
							}
						
						$message .= '</table>';
						}
					break;
					
					
					
					case 3:
					
					/* ------------------------------------------------------
						
					3/ Rapport journalier par e-mail: Lister les voitures qui sont rentrées et ressorties le jour même
					
					------------------------------------------------------ */
		
					$statement = $db->prepare("SELECT `assist_queries`.`car_immatriculation`, `assist_cars`.`car_marque`, `assist_cars`.`car_model` FROM `assist_queries` LEFT JOIN `assist_cars` ON `assist_queries`.`car_immatriculation` = `assist_cars`.`car_immatriculation` WHERE DATE(`assist_queries`.`car_date`) = DATE(NOW() - INTERVAL 1 DAY) AND `assist_queries`.`car_status` = 2 AND DATE(`assist_queries`.`car_date`) = DATE(`assist_cars`.`car_date_in`)");
					$statement->execute();
					
					$isExisting = $statement->rowCount();
					
					
					
					if ($isExisting > 0) {
						
						$subject = "[ASSIST] Daily report : All cars that check-in/out on the same day yesterday";
						
						$message = '<p>Dear BE-h24,</p>';
						$message .= '<p>Please find below a list of all cars that check-in/out on the same day yesterday.</p>';
					
						$message .= '<table width="100%" cellpadding="4" cellspacing="0" border="0">';
							$message .= '<tr>';
							$message .= '<th align="left">Licence plate</th>';
							$message .= '<th align="left">Car brand</th>';
							$message .= '<th align="left">Model</th>';
							$message .= '</tr>';
							
						$i = 0;
					
						foreach($statement as $row) {
							
							$i++;
							if ($i % 2 == 0) {$odd = 1;}else{$odd = 0;}
							
							$message .= '<tr>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['car_immatriculation'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['car_marque'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['car_model'].'</td>';
							$message .= '</tr>';
							}
						
						$message .= '</table>';
						}
					break;
					
					
					
				case 4:
					
					/* ------------------------------------------------------
						
					4/ Rapport mensuel par e-mail: Contrats clôturés du mois précédent classé par dépanneur (Avec numéro de plaque + numéro de contrat)
					
					------------------------------------------------------ */
		
					$statement = $db->prepare("SELECT CONCAT(`dep_code`, `contrat_id`) AS `contract`, `imat`, `dep_nom`, `dep_nom_back` FROM `contrats` WHERE YEAR(backdate_fin) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(backdate_fin) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ORDER BY `dep_nom` ASC;");
					$statement->execute();
					
					$isExisting = $statement->rowCount();
					
					
					
					if ($isExisting > 0) {
						
						$subject = "[ASSIST] Monthly report : All closed contrats for past month";
						
						$message = '<p>Dear BE-h24,</p>';
						$message .= '<p>Please find below a list of all contracts that have been closed last month.</p>';
					
						$message .= '<table width="100%" cellpadding="4" cellspacing="0" border="0">';
							$message .= '<tr>';
							$message .= '<th align="left">Contract #</th>';
							$message .= '<th align="left">Licence plate</th>';
							$message .= '<th align="left">Troubleshooter (pick up)</th>';
							$message .= '<th align="left">Troubleshooter (return)</th>';
							$message .= '</tr>';
							
						$i = 0;
					
						foreach($statement as $row) {
							
							$i++;
							if ($i % 2 == 0) {$odd = 1;}else{$odd = 0;}
							
							$message .= '<tr>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['contract'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['imat'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['dep_nom'].'</td>';
							$message .= '<td'.(($odd == 0)?' bgcolor="#f9f9f9"':'').'>'.$row['dep_nom_back'].'</td>';
							$message .= '</tr>';
							}
						
						$message .= '</table>';
						}
					break;
				
				
				
			
			}
			
			
			if (!$message) {
				echo 'no report to be sent';
				continue;
				}
				
				
			echo $message;
			
				
			
			$mail = new PHPMailer;
		
			$mail->Encoding = 'base64';
			$mail->CharSet = 'UTF-8';
			
			$mail->From = 'europcar@mediaa.be';
			$mail->FromName = 'Europcar Assist';
			
			$mail->Sender = 'europcar@mediaa.be';
			
			$mail->addReplyTo('BE-h24@europcar.com');
			$mail->addAddress('shirley.noel@europcar.com');
			$mail->addCC('kurt.vantrimpont@europcar.com');
			$mail->addBCC('thomas@mediaa.be');
			
	
			$mail->isHTML(true);
			
			
			
			$mail->Subject = $subject;
			$mail->Body    = $message;
			$mail->AltBody = strip_tags($message);
			
			
			
			if(!$mail->send()) {
			    echo '<p>Message could not be sent.</p>';
			    echo '<p>Mailer Error: ' . $mail->ErrorInfo.'</p>';
			}else{
				echo '<p>report #'.$r.' sent</p>';
				
				
			
				//Log sent mail in DB to avoid multiple sents
				$statement = $db->prepare("INSERT INTO `log_crons` (`log_date`, `log_mail_id`) VALUES (DATE(NOW()), :r)");
				$statement->execute(array('r' => $r));
				
				
				
		
				
			}
			
			$mail->clearAllRecipients();
			$mail->clearReplyTos();
		
		
		}
		

		$db = null;
	
	} catch (PDOException $e) {
	    print "Erreur !: " . $e->getMessage() . "<br/>";
	    die();
	}	
	
	?>