<?PHP
session_name('SESSION1');
session_start();

include('../inc/dictionnary.php');
include('../inc/functions.php');

include('../inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

if (!$_POST['agency_id']) {
	
	/* ------------------------------
		
	Bon de commande pour Dépanneur	
	
	------------------------------ */
	
	$statement = $db->prepare("UPDATE bondecommande SET client_lastname=:client_lastname, client_firstname=:client_firstname, client_address=:client_address, client_cp=:client_cp, client_city=:client_city, client_tel=:client_tel, client_plate=:client_plate, rental_days=:rental_days, rental_start=:rental_start, rental_remarks=:rental_remarks".($_POST['action']=='send'?', bdc_sent=1':'')." WHERE bdc_id=:id;");
	$statement->execute(array('client_lastname' => encrypt_decrypt('encrypt', $_POST['client_lastname']), 'client_firstname' => encrypt_decrypt('encrypt', $_POST['client_firstname']), 'client_address' => encrypt_decrypt('encrypt', $_POST['client_address']), 'client_cp' => $_POST['client_cp'], 'client_city' => encrypt_decrypt('encrypt', $_POST['client_city']), 'client_tel' => encrypt_decrypt('encrypt', $_POST['client_tel']), 'client_plate' => encrypt_decrypt('encrypt', $_POST['client_plate']), 'rental_days' => $_POST['rental_days'], 'rental_start' => convertToSQLDateTime($_POST['rental_start']), 'rental_remarks' => $_POST['rental_remarks'], 'id' => $_POST['id']));
	
	
	$statement = $db->prepare("SELECT `assist_cars`.`contrat`, `assist_cars`.`car_immatriculation`, `assist_depaneurs`.`dep_mail` FROM `assist_cars` LEFT JOIN `assist_depaneurs` ON `assist_depaneurs`.`id` = `assist_cars`.`car_depaneur` WHERE bondecommande=:id;");
	$statement->execute(array('id' => $_POST['id']));
	
	$row = $statement->fetch(PDO::FETCH_ASSOC);
	$contrat = $row['contrat'];
	$car_immatriculation = $row['car_immatriculation'];
	$dep_mail = $row['dep_mail'];
	
	$statement = $db->prepare("UPDATE contrats SET jours=:rental_days WHERE id=:contrat;");
	$statement->execute(array('rental_days' => $_POST['rental_days'], 'contrat' => $contrat));
	
	switch ($_POST['action']) {
	case 'back':
		// Si retour, alors on retourne sur la page du dépanneur après avoir sauvegardé les données temporairement
		echo 'window.location.href = \''.$_POST['url'].'\'';
		break;
	case 'send':
		// envoi d'un mail au dépanneur
		require '../inc/phpmailer/PHPMailerAutoload.php';
		
		$message = str_replace('$immat$', $car_immatriculation, $dic_bdc_mail_body);
		
		$mail = new PHPMailer;
		
		$mail->Encoding = 'base64';
		$mail->CharSet = 'UTF-8';
	
		$mail->From = 'europcar@mediaa.be';
		$mail->FromName = 'Europcar Assist';
		
		$mail->Sender = 'europcar@mediaa.be';
		
		$mail->addReplyTo('BE-h24@europcar.com');
		$mail->addAddress($dep_mail);
	
		$mail->isHTML(true);
		
		$mail->Subject = $dic_bdc_mail_subject;
		$mail->Body    = $message;
		$mail->AltBody = strip_tags($message);
	
		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
		
		$mail->clearAllRecipients();
		$mail->clearReplyTos();
		
		// Si envoi, alors on disable les inputs et on modifie les options dans la toolbar avec alert client
		echo '$(\'input, textarea\').prop(\'disabled\', true);';
		echo '$(\'.toolbar\').html(\'<a href="javascript:window.print();" class="tohide">'.$co_imprimer.'</a><br><a href="javascript:history.back();" class="tohide">'.$dic_bdc_retour.'</a>\');';
		echo 'alert(\''.$dic_bdc_dispo.'\')';
		break;
	
	}

}else{
	/* ------------------------------
		
	Bon de commande pour Agence de location	
	
	------------------------------ */
	
	$statement = $db->prepare("SELECT `dep_nom`, `dep_address`, `dep_cp`, `dep_city`, `dep_tel`, `dep_mail` FROM `assist_depaneurs` WHERE `id` =  :id;");
	$statement->execute(array('id' => $_POST['assist_id']));

	$row_assistance = $statement->fetch(PDO::FETCH_ASSOC);
	
	
	$bdc_subject = _('Bon de commande');
	
	$bdc_html = '';
	
	
	
	$bdc_html .= '<p>'.sprintf(_('Un nouveau bon de commande vient d\'être envoyé par %s'), $row_assistance['dep_nom']).'</p>'."\r\n"."\r\n";
	
	$bdc_html .= '<p>'._('Vous trouverez les détails ci-dessous :').'</p>'."\r\n"."\r\n";
	
	$bdc_html .= '<p>'._('Coordonnées de l\'assistance :').'</p>'."\r\n"."\r\n";
	$bdc_html .= '<p>'.$row_assistance['dep_nom'].'</p>'."\r\n";
	$bdc_html .= '<p>'.$row_assistance['dep_address'].'</p>'."\r\n";
	$bdc_html .= '<p>'.$row_assistance['dep_cp'].' '.$row_assistance['dep_city'].'</p>'."\r\n";
	$bdc_html .= '<p>'.$row_assistance['dep_tel'].'</p>'."\r\n";
	
	
	
	$bdc_html .= '<p>'._('Coordonnées client :').'</p>'."\r\n"."\r\n";
	
	$bdc_html .= '<p>'.$_POST['client_lastname'].' '.$_POST['client_firstname'].'<br>'."\r\n";
	$bdc_html .= $_POST['client_address'].'<br>'."\r\n";
	$bdc_html .= $_POST['client_cp'].' '.$_POST['client_city'].'<br>'."\r\n";
	$bdc_html .= $_POST['client_tel'].'<br>'."\r\n";
	$bdc_html .= _('Plaque d\'immatriculation :').$_POST['client_plate'].'</p>'."\r\n"."\r\n";
	
	$bdc_html .= '<p>'._('Numéro de dossier :').' '.$_POST['num_dossier'].'</p>'."\r\n"."\r\n";
	
	$bdc_html .= '<p>'._('Catégorie du véhicule :').' '.$_POST['accriss'].'</p>'."\r\n"."\r\n";
	
	$bdc_html .= '<p>'._('Nombre de jours :').' '.$_POST['rental_days'].'</p>'."\r\n"."\r\n";
	
	$bdc_html .= '<p>'._('Date de début :').' '.$_POST['rental_start'].'</p>'."\r\n"."\r\n";
	
	$bdc_html .= '<p>'._('Remarques :').' '.$_POST['rental_remarks'].'</p>'."\r\n"."\r\n";

	
	
	
	
	$statement = $db->prepare("SELECT `dep_nom`, `dep_mail` FROM `assist_depaneurs` WHERE `id` =  :id;");
	$statement->execute(array('id' => $_POST['agency_id']));

	$row_agence = $statement->fetch(PDO::FETCH_ASSOC);
	
	
	require '../inc/phpmailer/PHPMailerAutoload.php';
		
	
	$mail = new PHPMailer;
	
	$mail->Encoding = 'base64';
	$mail->CharSet = 'UTF-8';

	$mail->From = 'europcar@mediaa.be';
	$mail->FromName = 'Europcar Assist';
	
	$mail->Sender = 'europcar@mediaa.be';
	
	$mail->addReplyTo('BE-h24@europcar.com');
	
	$mail->addAddress($row_agence['dep_mail']);
	$mail->addCC($row_assistance['dep_mail']);
	//$mail->addAddress('thomas@mediaa.be');

	$mail->isHTML(true);
	
	$mail->Subject = $bdc_subject;
	$mail->Body    = $bdc_html;
	$mail->AltBody = strip_tags($bdc_html);

	if(!$mail->send()) {
	    echo 'Message could not be sent.';
	    echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
	
	$mail->clearAllRecipients();
	$mail->clearReplyTos();
	
	// Si envoi, alors on disable les inputs et on modifie les options dans la toolbar avec alert client
		echo '$(\'input, textarea\').prop(\'disabled\', true);';
		echo '$(\'.toolbar\').html(\'<a href="javascript:window.print();" class="tohide">'.$co_imprimer.'</a><br><a href="javascript:history.back();" class="tohide">'.$dic_bdc_retour.'</a>\');';
		echo 'alert(\''._("Le bon de commande a bien été envoyé. Vous recevrez également une copie par e-mail dans quelques instants.").'\')';
		//break;
	
	
}


$statement = null; $db = null;

?>