<?PHP

include('inc/dictionnary.php');
include('inc/connexion.php');

function create_csv_string($data) {

  // Open temp file pointer
  if (!$fp = fopen('php://temp', 'w+')) return FALSE;

  // Loop data and write to file pointer
  foreach ($data as $line) fputcsv($fp, $line);

  // Place stream pointer at beginning
  rewind($fp);

  // Return the data
  return stream_get_contents($fp);

}

function send_csv_mail ($csvData, $body, $to = 'guillaume@mediaa.be', $subject = 'Prolongations de contrats des dernières 24H', $from = 'ECBE') {

  // This will provide plenty adequate entropy
  $multipartSep = '-----'.md5(time()).'-----';

  // Arrays are much more readable
  $headers = array(
    "From: $from",
    "Reply-To: $from",
    "Content-Type: multipart/mixed; boundary=\"$multipartSep\""
  );

  // Make the attachment
  $attachment = chunk_split(base64_encode(create_csv_string($csvData))); 

  // Make the body of the message
  $body = "--$multipartSep\r\n"
        . "Content-Type: text/plain; charset=ISO-8859-1; format=flowed\r\n"
        . "Content-Transfer-Encoding: 7bit\r\n"
        . "\r\n"
        . "$body\r\n"
        . "--$multipartSep\r\n"
        . "Content-Type: text/csv\r\n"
        . "Content-Transfer-Encoding: base64\r\n"
        . "Content-Disposition: attachment; filename=\"file.csv\"\r\n"
        . "\r\n"
        . "$attachment\r\n"
        . "--$multipartSep--";

   // Send the email, return the result
   return @mail($to, $subject, $body, implode("\r\n", $headers));

}

//echo $to."<br>".$subject."<br>".$headers."<br><br>";
$count = 0;
try 
{
	$data = $conn->query ("SELECT * FROM contrats WHERE prolo = 1");
}
catch(PDOException $e)
{
	// echo $sql . "<br>" . $e->getMessage();
}
$array[0] = array('imat', 'date retour originale', 'nouvelle date retour', 'ref. contrat', 'lien contrat');
foreach ($data as $contrat)
{
  try 
  {
    $data = $conn->query ("UPDATE contrats SET prolo = 0 WHERE id = '{$contrat['id']}'");
  }
  catch(PDOException $e)
  {
    // echo $sql . "<br>" . $e->getMessage();
  }
	//$message .= "<tr><td><b>N° Immatriculation : </b>".$contrat['imat']."</td><td><b> Date retour originale : </b>".$contrat['date_fin_old']."</td><td><b> Nouvelle date retour : </b>".$contrat['date_fin']."</td></tr>";
	$count++;
	$array[$count] = array($contrat['imat'], $contrat['date_fin_old'], $contrat['date_fin'], $contrat['numref'], "https://assist.europcar.be/contrat.php?contratid=".$contrat['id']);
}
//$message .= "</table></body></html>";
//echo $message;
if ($count > 0)
{
	var_dump($array);
	//mail($to, $subject, $message, $headers);
	send_csv_mail($array, "Hello, You'll find the contrats extentions from the last 24H in attachement");
}
?>
<?php

?>