<?php
function SQLDatetoToHuman($date) {
	// from : 2015-08-31 00:00:00
	// to	: 31/08/2015 00:00
	$explode = explode(' ', $date);
	
	$date = explode('-', $explode[0]);
	
	
	$time = explode(':', $explode[1]); 
	
	return $date[2].'/'.$date[1].'/'.$date[0].' '.$time[0].':'.$time[1];
	
	}

function SQLDatetoDDMMYY($date) {
	// from : 2015-08-31
	// to	: 31/08/2015
	if ($date) {
		$date = explode('-', $date);
		return $date[2].'/'.$date[1].'/'.$date[0];
	}else{
		return '';
	}
	}
	

function SQLtime_to_hhmm($time) {
	$time = explode(':', $time);
	//return $time;
	return $time[0].':'.$time[1];
	}
	
	
	
	
function DDMMYYtoSQLDate($date) {
	// from : 31/08/2015
	// to	: 2015-08-31
	$date = explode('/', $date);
	return $date[2].'-'.$date[1].'-'.$date[0];
	}
function SQLDateTimetoDDMMYYHHMM($date) {
	// from : 31/08/2015 12:00
	// to	: 2015-08-31 12:00:00
	if ($date) {
		$explode = explode(' ', $date);
		$date = explode('-', $explode[0]);
		$time = explode(':', $explode[1]); 
		return $date[2].'/'.$date[1].'/'.$date[0].' '.$time[0].':'.$time[1];
	}else{
		return '';
	}
	}
	
function convertToSQLDateTime($date) {
	// from : 31/08/2015 12:00
	// to	: 2015-08-31 12:00:00
	if ($date) {
		$explode = explode(' ', $date);
		$date = explode('/', $explode[0]);
		$time = explode(':', $explode[1]); 
		return $date[2].'-'.$date[1].'-'.$date[0].' '.$time[0].':'.$time[1].':00';
	}else{
		return '';
	}
	}
	
	
/**
 * simple method to encrypt or decrypt a plain text string
 * initialization vector(IV) has to be the same when encrypting and decrypting
 * 
 * @param string $action: can be 'encrypt' or 'decrypt'
 * @param string $string: string to encrypt or decrypt
 *
 * @return string
 */
function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'iNb0H4aK01^j7#Ja'; // A stocker dans un fichier php pas accessible dans le WEBROOT
    $secret_iv = 'wQ39fsvrec%*iowL'; // A stocker dans un fichier php pas accessible dans le WEBROOT
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    } else {
	    $output = $string;
    }
    return $output;
}
?>