<?PHP
if (isset($_GET['lg'])) {
	$_SESSION['lg'] = $_GET['lg'];
	setcookie("assist_lg", $_SESSION['lg'], time()+60*60*24*30);
}else if (isset($_SESSION['lg'])) {
	setcookie("assist_lg", $_SESSION['lg'], time()+60*60*24*30);
}else if (isset($_COOKIE['assist_lg'])) {
	$_SESSION['lg'] = $_COOKIE['assist_lg'];
}else{
	$tmpLg=substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	switch($tmpLg) {
		case 'nl':
			$tmpLg = 'nl';
			break;
		default:
			$tmpLg = 'fr';
	}
	$_SESSION['lg'] = $tmpLg;
	setcookie("assist_lg", $tmpLg, time()+60*60*24*30);
}
$lgstring = $_SESSION['lg'];





switch($lgstring) {
	case 'fr':
		// Dictionnaire FR
		$dic_switchlg = '<a href="/?lg=nl" id="switchlg">in het Nederlands aub</a>';
		
		
		
		$dic_accueil = 'Accueil';
		$dic_chercherdepanneur = 'Chercher un dépanneur';
		$dic_modifierpwd = 'Modifier mot de passe';
		$dic_imatlist = 'Liste des plaques';
		$dic_contacteznous = 'Contactez-nous';
		$dic_sedeconnecter = 'Se déconnecter';
		
		$dic_welcome = 'Bienvenue.';
		$dic_veuillezcompleter = 'Veuillez compléter les informations suivantes pour vous connecter.';
		$dic_veuillezcompleterkill = 'Annulation de contrat en cours, veuillez compléter les informations suivantes pour continuer.';
		$dic_veuillezcompletercontrat = 'Vous devez être connecté pour pouvoir consulter un contrat, veuillez compléter les informations suivantes pour continuer.';
		$dic_mesidentifiants = 'Mes identifiants';
		$dic_email = 'E-mail:';
		$dic_pwd = 'Password:';
		$dic_mesoptions = 'Mes options';
		$dic_gardermasession = 'Garder ma session active sur cet ordinateur';
		$dic_seconnecter = 'Se connecter';
		$dic_loginerror = 'Votre login/password semble incorrect. Merci de vérifier';
		
		$dic_bienvenueassistance = 'Bienvenue';
		$dic_selectionparadresse = 'Sélectionner un dépanneur par adresse';
		$dic_chercher = 'Chercher';
		$dic_exemple = 'Ex: <i>Weiveldlaan 8, 1930 Zaventem</i> ou <i>1930 Zaventem</i>';
		$dic_selectionparnom = 'Sélectionner un dépanneur par nom';
		$dic_plusdevehicule = 'Le dépanneur sélectionné n\'a plus de véhicule disponible. Veuillez faire un choix parmis les dépanneurs suivants:';
		$dic_plusdevehiculealert = 'Le dépanneur sélectionné n\'a plus de véhicule disponible. Veuillez choisir un autre dépanneur.';
		$dic_selectionnerdepanneur = 'Sélectionner ce dépanneur';
		$dic_depferme = 'Ce dépanneur est fermé en ce moment.';
		$dic_vehiculesdispos = 'véhicule(s) disponible(s)';
		
		$dic_dep_closed_1 = 'Ce dépanneur est actuellement fermé.';
		$dic_dep_closed_2 = 'Veuillez contacter le dépanneur au numéro <strong>$tel$</strong> pour la disponibilité des véhicules.';
		
		$dic_agency_selection = 'Sélectionnez une agence Europcar';
		$dic_agency_closed = 'Cette agence est actuellement fermée.';
		$dic_agency_book = 'Réserver un véhicule dans cette agence';
		
		$dic_depanneur = 'Dépanneur:';
		$dic_decouvrezliste = 'Vous trouverez ci-dessous la liste des véhicules disponibles chez $repl_depanneur$ à $repl_city$:';
		$dic_bloquervehicule = 'Bloquer ce véhicule';
		$dic_prolocontrat = 'Prolonger ce contrat';
		$dic_blocbb = 'Blocage « VENTE »';
		$dic_blocbbedit = 'Modifier date « VENTE »';
		$dic_blocbbd = 'Confirmer récupération';
		$dic_blocshop = 'Blocage « Véhicule Shop »';
		$dic_supprimervehicule = 'Supprimer ce véhicule';
		$dic_etesvoussur = 'Etes-vous sûr ?';
		$dic_veuillezprecisernumero = 'Veuillez préciser un numéro de commande :';
		$dic_selectionnezunba = 'un numéro de BA :';
		$dic_veuillezpreciserjours = 'et les jours accordés :';
		$dic_veuillezpreciserprolo = 'Veuillez préciser une nouvelle date retour :';
		$dic_veuillezpreciserdate = 'Veuillez préciser une date limite :';
		$dic_valider = 'Valider';
		$dic_valider_bevestigen = 'Valider';
		$dic_temp_save = 'Sauvegarder provisoirement';
		$voir_contrat = 'Voir contrat';
		$dic_completer_bdc = 'Compléter le bon de commande';
		$dic_consulter_bdc = 'Consulter le bon de commande';
		
		$dic_bloquejusquau1 = "Véhicule bloqué jusqu\'au";
		$dic_bloquejusquau2 = 'par';
		$dic_bloquejusquau3 = 'le dépanneur';
		$dic_bloqued = 'Véhicule bloqué pour récupération - Disponible jusqu au ';
		
		$dic_debloquer = 'Débloquer ce véhicule';
		$dic_precisernumerocommande = 'Veuillez préciser un numéro de commande';
		$dic_bloqueentretemps = 'Ce véhicule a été bloqué entre temps. Merci de rafraîchir la page pour obtenir la liste des véhicules disponibles.';
		$dic_livre = 'Le véhicule a été livré au client';
		$dic_retourmemeendroit = 'Ce véhicule doit impérativement être retourné au même endroit.';
		$dic_cooordonnees = 'Coordonnées du dépanneur';
		$dic_horaires = 'Heures d\'ouverture';
		$dic_horaires_ferme = 'Fermé';
		$dic_horaire_info = 'Veuillez appeler en dehors des heures d\'ouverture';
		$dic_horaires_lun = 'Lundi';
		$dic_horaires_mar = 'Mardi';
		$dic_horaires_mer = 'Mercredi';
		$dic_horaires_jeu = 'Jeudi';
		$dic_horaires_ven = 'Vendredi';
		$dic_horaires_sam = 'Samedi';
		$dic_horaires_dim = 'Dimanche';
		$dic_ajoutervehicule = 'Ajoutez un véhicule en insérant son numéro de plaque d\'immatriculation';
		$dic_samarque = 'sa marque';
		$dic_ajoutervehiculemodel = 'son modèle';
		$dic_ajoutervehicule_suite = 'et en précisant sa catégorie';
		$dic_cat_ecmr = 'ECMV (Renault Clio ou equivalent)';
		$dic_cat_ccmr = 'CCMV (Opel Astra ou equivalent)';
		$dic_ouequivalent = ' ou équivalent';
		$dic_ajouter = 'Ajouter';
		$dic_vehiculedejapresent = 'Ce véhicule a déjà été encodé dans la base de données!';
		$prolonger = 'Prolonger un contrat';
		$interval = 'Assigner contrats';
		$dic_finaliser = 'Finaliser contrat';
		$dic_cloturer = 'Cloturer contrat';
		$finaliser_contrat = 'Contrat à finaliser';
		
		$dic_modifiermdp = 'Modifier mon mot de passe';
		$dic_choosenewpwd = 'Choisissez un nouveau mot de passe:';
		$dic_votrenouveaumdp = 'Votre nouveau mot de passe:';
		$dic_retapezmdp = 'Retapez votre mot de passe:';
		$dic_modifier = 'Modifier';
		$dic_modifiersucces = 'Votre mot de passe a été modifié avec succès.';
		$dic_erreurpwd = 'Erreur, veuillez ressaisir votre mot de passe';

		$chercher_contrat = 'Rechercher un contrat';
		$no_ref = 'N° de référence';
		$par_imat = 'Immatriculation';
		$par_nom = 'Nom client';
		$chercher = 'Chercher';
		$contratno = 'Contrat';
		$date_retour = 'Date retour';
		$client = 'Client';
		$creation_date = 'Date de création';
		
		$contrats_att = 'Contrats attribués';
		$dernier = 'Dernier utilisé';
		$achtung = 'ATTENTION, les contrats assignés sont bientôt épuisés.';
		$achtung2 = 'ATTENTION, les contrats assignés sont épuisés.';

		$kill1 = 'Etes-vous sûr de vouloir cloturer le contrat pour le véhicule suivant ?<br>Immatriculation : ';
		$dic_annuler = 'Annuler';
		$kill2 = "<br>Modèle : ";
		$kill3 = "Le contrat pour le véhicule <b>";
		$kill4 = "</b> a bien été clôturé.<br>";
		$kill5 = "Retourner à l'accueil";

		$co_title = 'CONTRAT DE LOCATION';
		$co_nocontrat = 'N° de contrat';
		$co_nocontratg = 'N° de contrat Greenway';
		$co_nores = 'N° réservation';
		$co_donneedepart = 'DONNEES VEHICULE DEPART';
		$co_etatdepart = 'ETAT VEHICULE DEPART';
		$dic_ajouter_degat = 'Ajouter dégat';
		$co_supdegats = 'Supprimer dégats';
		$dic_selectionnez = 'Sélectionnez';
		$co_dateheuredepart = 'DATE/HEURE DEPART';
		$co_dep = 'DEPANNEUR';
		$co_dateheureretour = 'DATE/HEURE RETOUR';
		$co_noplaque = 'N° PLAQUE';
		$co_type = 'TYPE';
		$co_kmdepart = 'KM DEPART';
		$co_livraison = 'LIVRAISON';
		$co_recup = 'RECUPERATION';
		$co_remarques = 'REMARQUES';
		$co_refuel = 'Refueling charge';
		$co_signclient = 'SIGNATURE CLIENT';
		$co_etatplein = 'ETAT DU PLEIN';
		$co_detailsassist = 'DETAILS COMPAGNIE D\'ASSISTANCE / MOYEN DE PAYEMENT';
		$co_nomassist = 'NOM ASSISTEUR';
		$co_noref = 'N° REFERENCE';
		$co_cat = 'CAT. DEMANDEE';
		$co_jours = 'JOURS ACCORDES';
		$co_fullcredit = 'FULL CREDIT';
		$co_cash = 'MONTANT CASH';
		$co_cca = 'N° CC';
		$co_exp = 'DATE EXPIRATION';
		$co_autor = 'N° AUTORISATION';
		$co_garantie = 'GARANTIE';
		$co_franchise = 'FRANCHISE';
		$co_clientresp = 'LE CLIENT RESTE RESPONSABLE POUR LA FRANCHISE EN CAS D\'ACCIDENT OU DE DEGATS EN TORT';
		$co_detailschauff = 'DETAILS DU CHAUFFEUR, LOUEUR, OU DEUXIEME CHAUFFEUR';
		$co_nom = 'NOM';
		$co_noid = 'N° CARTE D\'IDENTITE';
		$co_noperm = 'N° PERMIS DE CONDUIRE';
		$co_prenom = 'PRENOM';
		$co_oba = 'OBTENUE A';
		$co_oble = 'OBTENUE LE';
		$co_adresse = 'ADRESSE';
		$co_expiration = 'EXPIRATION';
		$co_lieunaissance = 'LIEU DE NAISSANCE';
		$co_expiration = 'EXPIRATION';
		$co_assurances = 'ASSURANCES';
		$co_cp = 'CODE POSTAL/VILLE';
		$co_pays = 'PAYS';
		$co_datenaissance = 'DATE NAISSANCE';
		$co_pays = 'PAYS';
		$co_tel = 'TEL';
		$eme = '2eme';
		$co_donnees2nd = 'DONNEES DEUXIEME CHAUFFEUR';
		$co_donneesretour = 'DONNEES VEHICULE RETOUR';
		$co_etatretour = 'ETAT VEHICULE AU RETOUR';
		$co_kmretour = 'KM RETOUR';
		$co_datlieu = 'DATE/LIEU + SIGNATURE DEPANNEUR';
		$co_depnom = 'NOM DU DEPANNEUR';
		$co_depsign = 'SIGNATURE DU DEPANNEUR';
		$co_nomclient = 'NOM DU CLIENT';
		$co_date = 'DATE';
		$co_signclientmin = 'Signature client';
		$co_imprimer = 'Imprimer';
		$co_degatsexist = 'Dégats existants';
		$co_conditions = '• "J\'ai pris connaissance de l\'état du véhicule décrit ci-dessus. J\'accepte de vérifier qu\'il correspond à la réalité et de faire immédiatement constater tout autre dégât par un agent Europcar. Je m\'engage expressément à signaler tout dommage survenu au véhicule pendant la location et dans ce cas, à remettre à mon retour un constat amiable d\'accident dûment complété."<br>• Le client reste responsable du véhicule, s\'il restitue le véhicule en dehors des heures d\'ouverture convenues.<br>• J\'ai lu et j\'accepte les conditions générales de location en annexe.<p>Signature du Client :</p>';
		$co_conditions2 = 'Le conducteur reconnait accepter les conditions de location spécifiques des sociétés d\'assistance tout comme la facturation d\'éléments additionnels';
		$co_conditions3 = '• "Je reconnais que les dégâts occasionnés au véhicule, constatés au retour de la location, sont survenus durant la période où le véhicule était sous ma responsabilité. Je m\'engage à intervenir pour tout ou partie des dégâts, et ce selon les modalités fixées dans le contrat de location que j\'ai signé au départ de la location."<p style="text-align:right;margin-right:20px;">Signature du Client :</p>';
		
		$dic_preciser_jours_prolongation = 'Veuillez préciser le nombre de jours à ajouter';
		$dic_prolo_historique = 'Historique des prolongations pour ce contrat';
		$dic_prolo_historique_log_1 = 'Le';
		$dic_prolo_historique_log_2 = 'pour';
		$dic_prolo_historique_log_3 = 'jour(s) jusqu\'au';
		$dic_prolo_historique_log_4 = 'par';
		
		$dic_oui = 'Oui';
		$dic_non = 'Non';
		
		$dic_cautionretour = 'CAUTION RETOUR';
		$dic_montant = 'MONTANT';
		
		// Bon de commande
		$dic_bdc_bondecommande = 'Bon de commande';
		$dic_bdc_numerodedossier = 'Numéro de dossier';
		$dic_bdc_depanneur = 'Dépanneur';
		$dic_bdc_assistance = 'Assistance';
		$dic_bdc_client = 'Client';
		$dic_bdc_nom = 'Nom';
		$dic_bdc_prenom = 'Prénom';
		$dic_bdc_adresse = 'Adresse';
		$dic_bdc_codepostal = 'Code postal';
		$dic_bdc_ville = 'Ville';
		$dic_bdc_telephone = 'Téléphone';
		$dic_bdc_plaqueimmatriculation = 'Plaque d\'immatriculation';
		$dic_bdc_categorievehicule = 'Catégorie du véhicule';
		$dic_bdc_nombredejours = 'Nombre de jours';
		$dic_bdc_datedebut = 'Date de début';
		$dic_bdc_datefin = 'Date de fin';
		$dic_bdc_remarques = 'Remarques';
		$dic_bdc_caution = 'Caution';
		$dic_bdc_sommecaution = 'Somme caution';
		$dic_bdc_sauvegarder = 'Sauvegarder';
		$dic_bdc_cloturer = 'Clôturer et envoyer au dépanneur';
		$dic_bdc_retour = 'Retour';
		$dic_bdc_save_ok = 'Les données ont bien été sauvegardées';
		$dic_bdc_dispo = 'Le bon de commande est maintenant disponible chez le dépanneur.';
		$dic_bdc_mail_subject = '[ASSIST] Nouveau bon de commande disponible';
		$dic_bdc_mail_body = '<p>Cher Dépanneur,</p>';
		$dic_bdc_mail_body .= '<p>Un nouveau bon de commande est disponible sur la plateforme Europcar Assist.</p>';
		$dic_bdc_mail_body .= '<p>Véhicule concerné: $immat$</p>';
		$dic_bdc_mail_body .= '<p>Rendez-vous sur https://assist.europcar.be afin de compléter le contrat.</p>';

		
		// situation journalière
		$dic_daily_title = 'Situation journalière';
		$dic_daily_q1 = 'Combien de véhicules sont-il disponibles à la location?';
		$dic_daily_q2 = 'Combien de véhicules sont-il indisponibles pour cause de problème technique ou dégât?';
		$dic_daily_q3 = 'Combien de véhicules sont-ils "VENTE" ?';
		$dic_daily_q4 = 'Combien de véhicules sont-ils sur la route ?';
		
		$dic_daily_veuillez = 'Veuillez préciser les immatriculations de ces véhicules';
		
		$dic_daily_vehicule = 'Véhicule';
		
		$dic_daily_err1 = 'Veuillez renseigner le nombre de véhicules disponibles';
		$dic_daily_err2 = 'Veuillez renseigner le nombre de véhicules SHOP';
		$dic_daily_err3 = 'Veuillez renseigner le nombre de véhicules VENTE';
		$dic_daily_err4 = 'Veuillez renseigner le nombre de véhicules sur la route';
		
		$dic_daily_send = 'Envoyer';
		$dic_daily_confirm = 'Votre situation journalière a bien été envoyée';
		
		// Imprimer un contrat:
		$dic_print_contrat_title = 'Imprimer un contrat';
		$dic_print_contrat_plaque = 'Plaque d\'immatriculation';
		$dic_print_contrat_debut = 'Date de début';
		$dic_print_contrat_retour = 'Date de retour';
		$dic_print_contrat_consulter = 'Consulter et imprimer ce contrat';
		

		break;
	case 'nl':
		// Dictionnaire NL
		$dic_switchlg = '<a href="/?lg=fr" id="switchlg">en Français svp</a>';
		
		
		$dic_accueil = 'Onthaal';
		$dic_chercherdepanneur = 'Een takeldienst zoeken';
		$dic_modifierpwd = 'Paswoord wijzigen';
		$dic_imatlist = 'nummerplaten lijst';
		$dic_contacteznous = 'Contacteer ons';
		$dic_sedeconnecter = 'Afmelden';
		
		$dic_welcome = 'Welkom.';
		$dic_veuillezcompleter = 'Gelieve volgende informatie in te vullen om je aan te melden';
		$dic_veuillezcompleterkill = 'Lopend contract annulering, neem dan de volgende informatie door te gaan.';
		$dic_veuillezcompletercontrat = 'U moet ingelogd zijn om een contract te bekijken, kunt u de volgende gegevens door te gaan.';
		$dic_mesidentifiants = 'Mijn gegevens';
		$dic_email = 'E-mail:';
		$dic_pwd = 'Paswoord:';
		$dic_mesoptions = 'Mijn opties';
		$dic_gardermasession = 'Mij aangemeld houden op deze computer';
		$dic_seconnecter = 'Aanmelden';
		$dic_loginerror = 'Uw login/paswoord werd incorrect ingevoerd. Gelieve na te kijken.';
		
		$dic_bienvenueassistance = 'Welkom';
		$dic_selectionparadresse = 'Selecteer een takeldienst op adres';
		$dic_chercher = 'Zoeken';
		$dic_exemple = 'Bijv: <i>Weiveldlaan 8, 1930 Zaventem</i> of <i>1930 Zaventem</i>';
		$dic_selectionparnom = 'Selecteer een takeldienst op naam';
		$dic_plusdevehicule = 'De geselecteerde takeldienst heeft geen voertuigen ter beschikking. Gelieve een keuze te maken uit volgende takeldiensten:';
		$dic_plusdevehiculealert = 'De geselecteerde takeldienst heeft geen voertuigen ter beschikking. Gelieve een andere keuze te maken.';
		$dic_selectionnerdepanneur = 'Selecteer deze takeldienst';
		$dic_depferme = 'Deze takeldienst is gesloten op dit ogenblik.';
		$dic_vehiculesdispos = 'wagen(s) beschikbaar';
		
		$dic_dep_closed_1 = 'Deze takeldienst is momenteel gesloten.';
		$dic_dep_closed_2 = 'Gelieve de takeldienst te contacteren op het nummer <strong>$tel$</strong> voor de beschikbaarheid van de wagens';
		
		$dic_agency_selection = 'Selecteer een Europcar agentschap';
		$dic_agency_closed = 'Dit agentschap is momenteel gesloten.';
		$dic_agency_book = 'Een voertuig in dit agentschap reserveren';
		
		$dic_depanneur = 'Takeldienst:';
		$dic_decouvrezliste = 'U vindt hieronder de lijst van beschikbare voertuigen bij $repl_depanneur$ te $repl_city$:';
		$dic_bloquervehicule = 'Blokkeer dit voertuig';
		$dic_prolocontrat = 'Verlengen contract';
		$dic_blocbb = '"VENTE" blokkering';
		$dic_blocbbedit = 'Edit datum "VENTE"';
		$dic_blocbbd = 'Confirmer récupération';
		$dic_blocshop = '"Vehicle Shop" blokkering';
		$dic_supprimervehicule = 'Voertuig verwijderen';
		$dic_etesvoussur = 'Bent u zeker?';
		$dic_veuillezprecisernumero = 'Gelieve een bestelnummer in te voeren:';
		$dic_selectionnezunba = 'Een BA nummer selecteren:';
		$dic_veuillezpreciserjours = 'Aantal voorziene dagen :';
		$dic_veuillezpreciserprolo = 'Geef een nieuwe datum terugkeer :';	
		$dic_veuillezpreciserdate = 'Geef een deadline :';
		$dic_valider = 'Valideer';
		$dic_valider_bevestigen = 'Bevestigen';
		$dic_temp_save = 'Voorlopig bewaren';
		$voir_contrat = 'Zie contract';
		$dic_completer_bdc = 'Bestelbon aanvullen';
		$dic_consulter_bdc = 'Bestelbon raadplegen';
		$dic_bloquejusquau = '  (ref: XXXXXXX)';
		
		$dic_bloquejusquau1 = 'Voertuig geblokkeerd tot';
		$dic_bloquejusquau2 = 'door';
		$dic_bloquejusquau3 = 'de takeldienst';
		$dic_bloqued = 'Voertuig geblokkeerd voor herstel - Beschikbaar tot ';
		
		$dic_debloquer = 'Deblokkeer dit voertuig';
		$dic_precisernumerocommande = 'Gelieve een bestelnummer op te geven:';
		$dic_bloqueentretemps = 'Dit voertuig is inmiddels geblokkeerd. Gelieve op refresh te drukken om de lijst met beschikbare voertuigen te bekomen.';
		$dic_livre = 'Het voertuig werd aan de klant geleverd.';
		$dic_retourmemeendroit = 'Dit voertuig moet verplicht op dezelfde plaats teruggebracht worden';
		$dic_cooordonnees = 'Gegevens van de takeldienst';
		$dic_horaires = 'Openingsuren';
		$dic_horaires_ferme = 'Gesloten';
		$dic_horaire_info = 'Gelieve buiten de openingsuren te bellen';
		$dic_horaires_lun = 'Maandag';
		$dic_horaires_mar = 'Dinsdag';
		$dic_horaires_mer = 'Woensdag';
		$dic_horaires_jeu = 'Donderdag';
		$dic_horaires_ven = 'Vrijdag';
		$dic_horaires_sam = 'Zaterdag';
		$dic_horaires_dim = 'Zondag';
		$dic_ajoutervehicule = 'Voeg een voertuig toe door ingave van de nummerplaat';
		$dic_samarque = 'merk';
		$dic_ajoutervehiculemodel = 'modeel';
		$dic_ajoutervehicule_suite = 'en specifieer de categorie';
		$dic_cat_ecmr = 'ECMV (Renault Clio of gelijkwaardig)';
		$dic_cat_ccmr = 'CCMV (Opel Astra of gelijkwaardig)';
		$dic_ouequivalent = ' of gelijkaardig';
		$dic_ajouter = 'Toevoegen';
		$dic_vehiculedejapresent = 'Dit voertuig werd reeds ingegeven in de database!';
		$prolonger = 'Contract verlengen';
		$interval = 'Contracten toewijst';
		$dic_finaliser = 'Contract vervolledigen';
		$dic_cloturer = 'Contract deblokkeren';
		$finaliser_contrat = 'Contract afronden';
		
		$dic_modifiermdp = 'Wijzig mijn paswoord';
		$dic_choosenewpwd = 'Kies een nieuw paswoord:';
		$dic_votrenouveaumdp = 'Uw nieuw paswoord:';
		$dic_retapezmdp = 'Geef uw paswoord nogmaals in:';
		$dic_modifier = 'Wijzigen';
		$dic_modifiersucces = 'Uw paswoord werd met succes gewijzigd.';
		$dic_erreurpwd = 'Foutmelding, gelieve uw paswoord opnieuw in te geven';
		
		$kill1 = 'Weet zeker dat u het contract voor het volgende voertuig cloturer ?<br>Registratie : ';
		$dic_annuler = 'Annuleren';
		$kill2 = "<br>Model : ";
		$kill3 = "Het contract voor het voertuig <b>";
		$kill4 = "</b> is voltooid.<br>";
		$kill5 = "Terug naar home.";

		$chercher_contrat = 'Zoek een contract';
		$no_ref = 'Ref.nummer';
		$par_imat = 'Registratie';
		$par_nom = 'Klant naam';
		$chercher = 'Zoek';
		$contratno = 'Contract';
		$date_retour = 'Terugkeer datum';
		$client = 'Klant';
		$creation_date = 'Aanmaakdatum';

		$contrats_att = 'Contracten bekroond';
		$dernier = 'Laatst gebruikt';
		$achtung = 'LET OP, toegewezen contracten worden snel uitgeput.';
		$achtung2 = 'LET OP, wordt toegewezen contracten uitgeput.';
		
		$co_title = 'HUUROVEREENKOMST';
		$co_nocontrat = 'Contract nummer';
		$co_nocontratg = 'Greenway Contract nummer';
		$co_nores = 'Reservatie nummer';
		$co_donneedepart = 'GEGEVENS WAGEN BIJ VERTREK';
		$co_etatdepart = 'STAAT VAN HET VOERTUID BIJ VERTREK';
		$dic_ajouter_degat = 'Toevoegen schade';
		$co_supdegats = 'Verwijder schade';
		$dic_selectionnez = 'Selecteer';
		$co_dateheuredepart = 'VERTREK DATUM';
		$co_dep = 'TAKELDIENST';
		$co_dateheureretour = 'TERUGKOMST DATUM';
		$co_noplaque = 'N° PLAAT';
		$co_type = 'MODEL AUTO';
		$co_kmdepart = 'KM UIT';
		$co_livraison = 'LEVERING';
		$co_recup = 'OPHALING';
		$co_remarques = 'OPMERKINGEN';
		$co_refuel = 'Refueling charge';
		$co_signclient = 'KLANT HANDTEKENING';
		$co_etatplein = 'BRANDSTOFSTAND';
		$co_detailsassist = 'DETAILS VAN DE ASSISTENTIE / BETALINGSWIJZE';
		$co_nomassist = 'ASSISTENTIE';
		$co_noref = 'REF. NUMMER';
		$co_cat = 'GEVRAAGDE CAT.';
		$co_jours = 'AANTAL TOEGEKENDE DAGEN';
		$co_fullcredit = 'FULL CREDIT';
		$co_cash = 'CASH';
		$co_cca = 'CC N°';
		$co_exp = 'VERVALDATUM';
		$co_autor = 'AUTH. N°';
		$co_garantie = 'WAARBORG';
		$co_franchise = 'VRIJSTELLING';
		$co_clientresp = 'DE KLANT BLIJFT VERANTWOORDELIJK VOOR DE VRIJSTELLING IN GEVAL VAN ONGEVAL OF SCHADE IN FOUT';
		$co_detailschauff = 'DETAILS VAN DE CHAUFFEUR, HUURDER OF BIJKOMSTIGE CHAUFFEUR(S)';
		$co_nom = 'NAAM';
		$co_noid = 'IDENTITEITSKAART N°';
		$co_noperm = 'RIJBEWIJS N°';
		$co_prenom = 'VOORNAAM';
		$co_oba = 'VERKREGEN TE';
		$co_oble = 'VERKREGEN OP';
		$co_adresse = 'ADRES';
		$co_expiration = 'VERVALDATUM';
		$co_lieunaissance = 'GEBOORTEPLAATS';
		$co_assurances = 'VERZEKERING';
		$co_cp = 'POSTCODE/WOONPLAATS';
		$co_pays = 'LAND';
		$co_datenaissance = 'GEBOORTEDATUM';
		$co_tel = 'TEL';
		$eme = '2de';
		$co_donnees2nd = 'GEGEVENS TWEEDE CHAUFFEUR';
		$co_donneesretour = 'GEGEVENS WAGENS BIJ TERUGKOMST';
		$co_etatretour = 'STAAT VAN HET VOERTUIG BIJ TERUGKOMST';
		$co_kmretour = 'KM IN';
		$co_datlieu = 'DATUM/PLAATS + HANDTEKENING VAN TAKELDIENST';
		$co_depnom = 'NAAM VAN TAKELDIENST';
		$co_depsign = 'HANDTEKENING TAKELDIENST';
		$co_nomclient = 'NAAM KLANT';
		$co_date = 'DATUM';
		$co_signclientmin = 'Handtekening Klant';
		$co_imprimer = 'Afprinten';
		$co_degatsexist = 'Bestaande schade :';
		$co_conditions = '•Ik heb kennisgenomen van de hogervermelde staat van het voertuig. Ik aanvaard te controleren of dit overeenstemt met de werkelijkheid en elke nieuwe schade voor mijn vertrek te laten vastellen door een aangestelde van Europcar. Il verbind mij er uitdrukkelijk toe elke schade tijdens de huur toegebracht aan het voertuig mee te delen en desgevallend een volledig aanrijdingsformulier te overhandingen bij het interveren van het voertuig."<br>• De klant blijft alsook verantwoordelijk voor het voertuig, wanneer hij deze buiten de afgesproken openingsoren terugbrengt, dit totdat het agentschap terug opengaat.<br>• Ik heb kennisgenomen van de algemene voorwaarden en ga er mee akkoord.<p>Handtekening Client :</p>';
		$co_conditions2 = 'De bestuurder erkent de specifieke bijstandsfirma voorwaarden evenals de facturatie van alle bijkomende elementen.';
		$co_conditions3 = '• "Ik erken dat de schade aan het voertuig, vastgesteld bij de terugkeer, veroorzaakt werd gedurende de periode waarin het voertuig onder mijn verantwoordelijkheid was. Ik verbind mij ertoe om de volledige of gedeellelijke schade te betalen en dat volgens de vastgestelde modaliteiten van de huurovereenkomst die il bij vertrek van de huur getekend heb."<p style="text-align:right;margin-right:20px;">Handtekening Client :</p>';
		$dic_preciser_jours_prolongation = 'Aantal dagen toe te voegen';
		$dic_prolo_historique = 'Historiek van de verlengingen voor dit contract';
		$dic_prolo_historique_log_1 = 'Op';
		$dic_prolo_historique_log_2 = 'voor';
		$dic_prolo_historique_log_3 = 'dag(en) tot';
		$dic_prolo_historique_log_4 = 'door takeldienst';
		
		$dic_oui = 'Ja';
		$dic_non = 'Nee';
		
		$dic_cautionretour = 'WAARBORG TERUG';
		$dic_montant = 'BEDRAG';
		
		// Bon de commande
		$dic_bdc_bondecommande = 'Bestelbon';
		$dic_bdc_numerodedossier = 'Dossiernummer';
		$dic_bdc_depanneur = 'Takeldienst';
		$dic_bdc_assistance = 'Bijstandsfirma';
		$dic_bdc_client = 'Klant';
		$dic_bdc_nom = 'Naam';
		$dic_bdc_prenom = 'Voornaam';
		$dic_bdc_adresse = 'Adres';
		$dic_bdc_codepostal = 'Postcode';
		$dic_bdc_ville = 'Stad';
		$dic_bdc_telephone = 'Telefoon';
		$dic_bdc_plaqueimmatriculation = 'Nummerplaat';
		$dic_bdc_categorievehicule = 'Categorie voertuig';
		$dic_bdc_nombredejours = 'Aantal dagen';
		$dic_bdc_datedebut = 'Begindatum';
		$dic_bdc_datefin = 'Einddatum';
		$dic_bdc_remarques = 'Opmerkingen';
		$dic_bdc_caution = 'Waarborg';
		$dic_bdc_sommecaution = 'Bedrag waarborg';
		$dic_bdc_sauvegarder = 'Bewaren';
		$dic_bdc_cloturer = 'Afsluiten en naar takeldienst sturen';
		$dic_bdc_retour = 'Terug';
		$dic_bdc_save_ok = 'De gegevens werden bewaard';
		$dic_bdc_dispo = 'De bestelbon is nu beschikbaar bij de takeldienst';
		
		$dic_bdc_mail_subject = '[ASSIST] Nieuwe bestelbon beschikbaar';
		$dic_bdc_mail_body = '<p>Beste Takeldienst,</p>';
		$dic_bdc_mail_body .= '<p>Een nieuwe bestelbon is beschikbaar op het Europcar Assist platform.</p>';
		$dic_bdc_mail_body .= '<p>Betreft voertuig: $immat$</p>';
		$dic_bdc_mail_body .= '<p>Raadpleeg https://assist.europcar.be om het contract te vervolledigen.</p>';

		// situation journalière
		$dic_daily_title = 'Dagelijkse situatie';
		$dic_daily_q1 = 'Hoeveel wagens zijn er beschikbaar voor verhuur?';
		$dic_daily_q2 = 'Hoeveel wagens zijn niet inzetbaar vanwege technisch probleem of schade?';
		$dic_daily_q3 = 'Hoeveel voertuigen zijn er "VERKOOP"?';
		$dic_daily_q4 = 'Hoeveel voertuigen zijn er rijdend?';
		
		$dic_daily_veuillez = 'Gelieve de nummerplaten van deze voertuigen door te geven:';
		
		$dic_daily_vehicule = 'Voertuig';
		
		$dic_daily_err1 = 'Gelieve het aantal beschikbare voertuigen door te geven';
		$dic_daily_err2 = 'Gelieve het aantal voertuigen SHOP door te geven';
		$dic_daily_err3 = 'Gelieve het aantal voertuigen VERKOOP door te geven';
		$dic_daily_err4 = 'Gelieve het aantal rijdende voertuigen door te geven';
		
		$dic_daily_send = 'Versturen';
		$dic_daily_confirm = 'Uw dagelijkse situatie werd doorgestuurd';
		
		// Imprimer un contrat:
		$dic_print_contrat_title = 'Contract afprinten';
		$dic_print_contrat_plaque = 'Nummerplaat';
		$dic_print_contrat_debut = 'Vertrekdatum';
		$dic_print_contrat_retour = 'Einddatum';
		$dic_print_contrat_consulter = 'Contract raadplegen en afprinten';
		
		
		
		break;
}


switch ($lgstring) {	
	case 'fr':
		define('LOCALE','fr_BE');
		define('LOCALE_EXTENDED','fr_BE.UTF-8');

	break;
	case 'nl':
		define('LOCALE','nl_BE');
		define('LOCALE_EXTENDED','de_DE.UTF-8');

	break;
	
	}

putenv("LANG=".LOCALE);
setlocale( LC_ALL, LOCALE, LOCALE_EXTENDED);
bindtextdomain(LOCALE, dirname(dirname(__FILE__)).'/locale/');
bind_textdomain_codeset(LOCALE, 'UTF-8');
textdomain(LOCALE);
?>