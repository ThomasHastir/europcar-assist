<?PHP

include('inc/dictionnary.php');
include('inc/connexion.php');

function create_csv_string($data) {

  // Open temp file pointer
  if (!$fp = fopen('php://temp', 'w+')) return FALSE;

  // Loop data and write to file pointer
  foreach ($data as $line) fputcsv($fp, $line);

  // Place stream pointer at beginning
  rewind($fp);

  // Return the data
  return stream_get_contents($fp);

}

function send_csv_mail ($csvData, $body, $to = 'guillaume@mediaa.be', $subject = 'Due vente cars', $from = 'ECBE') {

  // This will provide plenty adequate entropy
  $multipartSep = '-----'.md5(time()).'-----';

  // Arrays are much more readable
  $headers = array(
    "From: $from",
    "Reply-To: $from",
    "Content-Type: multipart/mixed; boundary=\"$multipartSep\""
  );

  // Make the attachment
  $attachment = chunk_split(base64_encode(create_csv_string($csvData))); 

  // Make the body of the message
  $body = "--$multipartSep\r\n"
        . "Content-Type: text/plain; charset=ISO-8859-1; format=flowed\r\n"
        . "Content-Transfer-Encoding: 7bit\r\n"
        . "\r\n"
        . "$body\r\n"
        . "--$multipartSep\r\n"
        . "Content-Type: text/csv\r\n"
        . "Content-Transfer-Encoding: base64\r\n"
        . "Content-Disposition: attachment; filename=\"file.csv\"\r\n"
        . "\r\n"
        . "$attachment\r\n"
        . "--$multipartSep--";

   // Send the email, return the result
   return @mail($to, $subject, $body, implode("\r\n", $headers));

}
$now = date("d");
$count = 0;
try 
{
	$data = $conn->query ("SELECT * FROM assist_cars WHERE car_status_BB = 1");
}
catch(PDOException $e)
{
	// echo $sql . "<br>" . $e->getMessage();
}
$array[0] = array('imat', 'date récup prévue');
foreach ($data as $car)
{
  $car_date_BB = substr($car['car_date_BB'], 0, 2);
  echo $now."<br>".$car_date_BB."<br>";
  if (($car_date_BB - $now) <= 1)
    {
      try 
      {
        $data = $conn->query ("UPDATE assist_cars SET car_status_BB = 0, car_status = 5 WHERE id = '{$car['id']}'");
      }
      catch(PDOException $e)
      {
        // echo $sql . "<br>" . $e->getMessage();
      }
      $count++;
      $array[$count] = array($car['car_immatriculation'], $car['car_date_BB']);
    }
}

if ($count > 0)
{
	var_dump($array);
	send_csv_mail($array, "Hello, You'll find the VENTE cars due in less than 24H");
}
?>
<?php

?>