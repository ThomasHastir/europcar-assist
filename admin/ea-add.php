<?PHP
session_name('SESSION2');
session_start();

if ($_SESSION['admin_connected'] == 1) {

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript">
jQuery(function($){
   $("#ea_immatriculation").mask("9-aaa-999");
});
</script>
<script type="text/javascript">
function form_validate() {
	var x=document.forms["form1"]["ea_immatriculation"].value;
	if (x==null || x=="") {
		alert("Veuillez indiquer un numéro d'immatriculation !");
		return false;
	}else{
		document.forms['form1'].submit();
	}
}
</script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
	<div id="content">
		<h1>Ajouter un véhicule Europ Assistance</h1>
		<form name="form1" id="form1" action="ea-action.php" method="post" onsubmit="form_validate(this);return false;">
			<fieldset>
				<legend>Plaque d'immatriculation</legend>
				<p><span><label for="ea_immatriculation" class="cellLike"><strong>Immatriculation: </strong></label><input type="text" name="ea_immatriculation" id="ea_immatriculation" value="" /></span></p>
			</fieldset>
			<p><input type="submit" value="Valider" /></p>
		</form>
		<p>&nbsp;</p>
	</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>
<?PHP
}
?>