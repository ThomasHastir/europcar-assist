<?PHP
session_name('SESSION2');
session_start();

if ($_SESSION['admin_connected'] == 1) {
	
	$id = $_GET['id'];
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
	<?PHP
	include('../inc/connexion.php');
	
	$sql = "SELECT dep_nom FROM assist_depaneurs WHERE id = $id;";
	
	$result = mysql_query($sql) 
	or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	mysql_close($link);
	
	$altRow = "";
	if ($row = mysql_fetch_array($result)) {
		$nom_assistance = $row['dep_nom'];
		}
	?>
	<h1>Conditions <?=$nom_assistance?></h1>
	<p>Vous pouvez <a href="assist-condition-edit.php?dep_id=<?=$id?>">ajouter</a>, éditer ou supprimer des conditions pour <strong><?=$nom_assistance?></strong> via la liste ci-dessous:</p>
	
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<th>GREENWAY NAME</th>
			<th>CONTRACT ID</th>
			<th>BUSINESS ACCOUNT ID</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	<?php
	include('../inc/connexion.php');
		
		$sql = "SELECT `assist_depaneurs_conditions`.`id`, `assist_depaneurs_conditions`.`dep_gw_name`, `assist_depaneurs_conditions`.`dep_contract_id`, `assist_depaneurs_conditions`.`dep_ba_id` FROM `assist_depaneurs_conditions` WHERE `dep_id` = $id ORDER BY dep_gw_name ASC;";
		
		$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		mysql_close($link);
		
		$altRow = "";
		while ($row = mysql_fetch_array($result)) {	
			?>
			<tr>
				<td<?=$altRow?>><?=$row['dep_gw_name']?></td>
				<td<?=$altRow?>><?=$row['dep_contract_id']?></td>
				<td<?=$altRow?>><?=$row['dep_ba_id']?></td>
				
				<td<?=$altRow?>><a href="assist-condition-edit.php?id=<?=$row['id']?>"><img src="/img/edit.png" alt="" /></a></td>
				<td<?=$altRow?>><a href="assist-condition-delete.php?id=<?=$row['id']?>&dep_id=<?=$id?>" onclick="return confirm('Etes vous sûr?')"><img src="/img/delete.png" alt="" /></a></td>
			</tr>
			<?php
				if ($altRow == "") {
				$altRow = " class=\"altrow\"";
				}else{
					$altRow = "";
				}
			}
	?>
	</table>

	
	<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>
<?PHP
}
?>