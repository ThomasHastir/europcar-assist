<?PHP
session_name('SESSION2');
session_start();

function SQLDateTimetoDDMMYYHHMM($date) {
	// from : 2015-08-31 12:00
	// to	: 31/08/2015 12:00
	$explode = explode(' ', $date);
	$date = explode('-', $explode[0]);
	$time = explode(':', $explode[1]); 
	return $date[2].'/'.$date[1].'/'.$date[0].' '.$time[0].':'.$time[1];
	}

if ($_SESSION['admin_connected'] == 1) {
	
if ($_GET['error'] == 1) {
	$errorMsg = '<p style="color:#ff0000; font-weight:bold;">Le véhicule que vous venez d\'encoder est déjà présent dans la liste ci-dessous:</p>';
}else{
	$errorMsg = '';
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
	<h1>Les véhicules Europ Assistance</h1>
	<p>Vous pouvez <a href="ea-add.php">ajouter</a> ou supprimer <?=$p_text?> un véhicule Europ Assistance via la liste ci-dessous:</p>
	<?=$errorMsg?>
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<th>Immatriculation</th>
			<th>Modèle</th>
			<th>Dépanneur</th>
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
		<?PHP
		include('../inc/connexion.php');
		
		$sql = "SELECT `assist_ea`.`id`, `assist_ea`.`ea_immatriculation`, `assist_depaneurs`.`dep_nom`, `assist_cars`.`car_status`, `assist_cars`.`car_date_out`, `assist_cars`.`car_marque`, `assist_cars`.`car_model` FROM `assist_ea` LEFT JOIN `assist_cars` ON `assist_ea`.`ea_immatriculation` = `assist_cars`.`car_immatriculation` LEFT JOIN `assist_depaneurs` ON `assist_cars`.`car_depaneur` = `assist_depaneurs`.`id` ORDER BY `assist_ea`.`id` DESC;";
		
		$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		mysql_close($link);
		
		$altRow = "";
		while ($row = mysql_fetch_array($result)) {
		?>
		<tr>
			<td<?=$altRow?>><?PHP if ($_GET['error_detail'] != '') { ?><span style="color:#ff0000; font-weight:bold;"><?PHP } ?><?=strtoupper($row['ea_immatriculation'])?><?PHP if ($_GET['error_detail'] != '') { ?></span><?PHP } ?></td>
			<td<?=$altRow?>><?=$row['car_marque']?> <?=$row['car_model']?></td>
			<td<?=$altRow?>><?=$row['dep_nom']?></td>
			<td<?=$altRow?>>
				<?php
					if ($row['car_status'] == 2 && isset($row['car_date_out'])) {
						echo 'En cours d\'utilisation depuis le '.SQLDateTimetoDDMMYYHHMM($row['car_date_out']);
					}else{
						echo 'Libre';
					}
					
				?>
				
			</td>
			<td<?=$altRow?>><a href="ea-delete.php?id=<?=$row['id']?>" onclick="return confirm('Etes vous sûr?')"><img src="/img/delete.png" alt="" /></a></td>
		</tr>
		<?PHP
			if ($altRow == "") {
				$altRow = " class=\"altrow\"";
			}else{
				$altRow = "";
			}
		}
		?>
	</table>
	
	<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>
<?PHP
}
?>