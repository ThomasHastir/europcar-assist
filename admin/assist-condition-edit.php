<?PHP
session_name('SESSION2');
session_start();

if ($_SESSION['admin_connected'] == 1) {

	$id = $_GET['id'];
	$dep_id = $_GET['dep_id'];
	
	if(!$dep_id) {
		$dep_id = $_POST['dep_id'];
		}
	
	if ($_POST) {

		foreach ( $_POST as $key=>$val) 
			$programmes[$key] = "'".mysql_escape_string($val)."'";
			

			if("" == trim($_POST['id'])){
				unset($programmes['id']);
			}
		
		$keys 		= array_keys($programmes);
		$values 	= array_values($programmes);
		
		$keys		= implode(',',$keys);
		$values		= implode(",",$values);
		$insert		= "REPLACE INTO assist_depaneurs_conditions ($keys) VALUES ($values)\n";

		include('../inc/connexion.php');
		$sql = $insert;
		$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		mysql_close($link);
		
		header("Location: assist-condition.php?id=".$dep_id."&valid=1");
	
	}

	if (isset($id)) {
		include('../inc/connexion.php');
		$sql = "SELECT * FROM assist_depaneurs_conditions WHERE id = ".$id.";";
		$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		mysql_close($link);
		$row = mysql_fetch_array($result);
		$dep_id = $row['dep_id'];
		$dep_gw_name = $row['dep_gw_name'];
		$dep_adress = $row['dep_adress'];
		$dep_vat = $row['dep_vat'];
		$dep_contract_id = $row['dep_contract_id'];
		$dep_ba_id = $row['dep_ba_id'];
		$dep_remarks = $row['dep_remarks'];
		//$dep_mop = $row['dep_mop'];
		$dep_guarant = $row['dep_guarant'];
		$dep_min_age = $row['dep_min_age'];
		$dep_young = $row['dep_young'];
		$dep_super_young = $row['dep_super_young'];
		$dep_add_dr = $row['dep_add_dr'];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/send.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
	<h1>Editer condition</h1>
	<form name="form1" id="form1" action="assist-condition-edit.php?id=<?=$id?>" method="post" onsubmit="form_validate(this);return false;">
		<fieldset>
			<legend>Editer</legend>
			<p><span><label for="dep_gw_name" class="cellLike"><strong>Greenway name: </strong></label><input type="text" name="dep_gw_name" id="dep_gw_name" value="<?=$dep_gw_name?>" size="50" /></span></p>
				<p><span><label for="dep_adress" class="cellLike"><strong>Adresse: </strong></label><input type="text" name="dep_adress" id="dep_adress" value="<?=$dep_adress?>" size="50" /></span></p>
				<p><span><label for="dep_vat" class="cellLike"><strong>VAT: </strong></label><input type="text" name="dep_vat" id="dep_vat" value="<?=$dep_vat?>" size="15" maxlength="12" /></span></p>
				<p><span><label for="dep_contract_id" class="cellLike"><strong>Contract ID: </strong></label><input type="text" name="dep_contract_id" id="dep_contract_id" value="<?=$dep_contract_id?>" size="15" maxlength="8" /></span></p>
				<p><span><label for="dep_ba_id" class="cellLike"><strong>Business Account ID: </strong></label><input type="text" name="dep_ba_id" id="dep_ba_id" value="<?=$dep_ba_id?>" size="15" maxlength="8" /></span></p>
				<p><span><label for="dep_remarks" class="cellLike"><strong>Remarques: </strong></label><input type="text" name="dep_remarks" id="dep_remarks" value="<?=$dep_remarks?>" size="50" /></span></p>
				<p><span><label for="dep_guarant" class="cellLike"><strong>Guarantee: </strong></label><input type="text" name="dep_guarant" id="dep_guarant" value="<?=$dep_guarant?>" size="15" /></span></p>
				
				
				
				<p><span><label for="dep_min_age" class="cellLike"><strong>Minimum age: </strong></label><select name="dep_min_age" id="dep_min_age"><option></option><option value="19"<?php if($dep_min_age == 19) { ?> selected="selected"<?php } ?>>19</option><option value="21"<?php if($dep_min_age == 21) { ?> selected="selected"<?php } ?>>21</option></select></span></p>
				
				
				<p><span><label for="dep_young" class="cellLike"><strong>Young Driver: </strong></label><select name="dep_young" id="dep_young"><option></option><option value="0"<?php if($dep_young == 0) { ?> selected="selected"<?php } ?>>Not incl.</option><option value="1"<?php if($dep_young == 1) { ?> selected="selected"<?php } ?>>Incl.</option></select></span></p>
			
				<p><span><label for="dep_super_young" class="cellLike"><strong>Super Young Driver: </strong></label><select name="dep_super_young" id="dep_super_young"><option></option><option value="0"<?php if($dep_super_young == 0) { ?> selected="selected"<?php } ?>>Not incl.</option><option value="1"<?php if($dep_super_young == 1) { ?> selected="selected"<?php } ?>>Incl.</option></select></span></p>
				
				
				<p><span><label for="dep_add_dr" class="cellLike"><strong>Add. Driver: </strong></label><select name="dep_add_dr" id="dep_add_dr"><option></option><option value="0"<?php if($dep_add_dr == 0) { ?> selected="selected"<?php } ?>>Not incl.</option><option value="1"<?php if($dep_add_dr == 1) { ?> selected="selected"<?php } ?>>Incl.</option></select></span></p>
		</fieldset>
		<p>
			<input type="hidden" name="dep_id" id="dep_id" value="<?=$dep_id?>" />
			<input type="hidden" name="id" id="id" value="<?=$id?>" />
			<input type="submit" value="Editer" />
		</p>
	</form>
	<p>&nbsp;</p>
</div>
</div>
<div id="footer"></div>
</body>
</html>
<?PHP
}
?>