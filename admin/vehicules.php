<?PHP
session_name('SESSION2');
session_start();

if ($_SESSION['admin_connected'] == 1) {
	
$dep_assist = $_GET['dep_assist'];

switch ($dep_assist) {
	case 0:
		$h1_title = 'Tous les dépanneurs';
		$p_text = 'un dépanneur';
		break;
	case 1:
		$h1_title = 'Toutes les sociétés d\'assistance';
		$p_text = 'une société d\'assistance';
		break;
	}
	
	
function SQLDatetoDDMMYY($date) {
	// from : 2015-08-31
	// to	: 31/08/2015
	$date = explode('-', $date);
	return $date[2].'/'.$date[1].'/'.$date[0];
	}
function DDMMYYtoSQLDate($date) {
	// from : 31/08/2015
	// to	: 2015-08-31
	$date = explode('/', $date);
	return $date[2].'-'.$date[1].'-'.$date[0];
	}
function SQLDatetoToHuman($date) {
	// from : 2015-08-31 00:00:00
	// to	: 31/08/2015 00:00
	$explode = explode(' ', $date);
	
	$date = explode('-', $explode[0]);
	
	
	$time = explode(':', $explode[1]); 
	
	return $date[2].'/'.$date[1].'/'.$date[0].' à '.$time[0].':'.$time[1];
	
	}
	
$success_msg = '';

if ($_FILES["csv_file"]["size"] > 0) { 

    $file = $_FILES["csv_file"]["tmp_name"]; 
    $handle = fopen($file,"r"); 
     
    include('../inc/connexion.php');
   
	if ( mysql_query("DELETE FROM vehicules_ec")) {
		
		
		mysql_query("ALTER TABLE vehicules_ec AUTO_INCREMENT = 1");
		
		//mysql_query("DELETE FROM vehicules_marques");
		
		//mysql_query("ALTER TABLE vehicules_marques AUTO_INCREMENT = 1");
	    
		$row = 0;
		$car_name = '';
		
		do { 
			if ($data[0]) {
				// On ne prend pas la 1ere ligne
				if ($row > 0) {
					
					// Si c'est un truck on ne l'importe pas
					$firstLetterSipp = substr($data[2], 0, 1);
					if ($firstLetterSipp != 'V') {
						
						
						// Table de correspondances Acriss:
						//E, M, N, H = ECMV
						//Le reste = CCMV
						
						switch ($firstLetterSipp) {
							case 'E':
							case 'M':
							case 'N':
							case 'H':
								$data[2] = 'ECMV';
								break;
							default:
								$data[2] = 'CCMV';
						}
					
						mysql_query("INSERT INTO vehicules_ec (activity, make_description, sipp_code, short_ctry_description, fuel_type_p_d, registration_number, registration_date, unit_number, chassis, driven_mileage, in_service_date, projected_sale_date, vehicule_colour, all_financer_codes) VALUES 
						( 
						'".addslashes($data[0])."', 
						'".addslashes($data[1])."', 
						'".addslashes($data[2])."', 
						'".addslashes($data[3])."', 
						'".addslashes($data[4])."', 
						'".addslashes($data[5])."', 
						'".addslashes(DDMMYYtoSQLDate($data[6]))."', 
						'".addslashes($data[7])."', 
						'".addslashes($data[8])."', 
						'".addslashes($data[9])."', 
						'".addslashes(DDMMYYtoSQLDate($data[10]))."', 
						'".addslashes(DDMMYYtoSQLDate($data[11]))."', 
						'".addslashes($data[12])."', 
						'".addslashes($data[13])."' 
						) 
						"); 

					}
					
					
				}
				$row++;
			} 
		} while ($data = fgetcsv($handle,1000,",","'")); 
		

		mysql_query("UPDATE `assist_config` SET `vehicules_ec_lastupdate` = NOW() WHERE `id` = 1;");

		
		$success_msg = '<strong>Données mises à jours avec succès</strong>';
	}
	
	mysql_close($link);
	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
	<h1><?=$h1_title?></h1>
	<p><?php if (!$success_msg) { ?>
	<?php
	include('../inc/connexion.php');
	
	$sql = "SELECT `vehicules_ec_lastupdate` FROM `assist_config` WHERE `id` = 1;";
	
	$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	mysql_close($link);
	
	if ($row = mysql_fetch_array($result)) {
		$vehicules_ec_lastupdate = SQLDatetoToHuman($row['vehicules_ec_lastupdate']);
		}
	?>
	La dernière importation date du <?=$vehicules_ec_lastupdate?>. <a href="javascript:void(0);" id="updatenow">Mettre à jour maintenant</a>.
	<?php }else{ ?><?=$success_msg?><?php } ?></p>
	
	<div style="display:none; background:#f8f8f8; padding:10px;">
		<form action="vehicules.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
			<p><input type="file" name="csv_file" id="csv_file"> <input type="submit" value="Envoyer"></p>
		</form>
		<p style="font-size:9px; margin:0; padding:0;">Notes:</p>
		<ul style="font-size:9px; margin:0; padding:0; list-style:none;">
			<li>Le fichier doit être enregistré au format CSV (via Excel par exemple);</li>
			<li>Le séparateur de colonne doit être une virgule. <a href="csv/fichier-complet.csv">Téléchargez un fichier d'exemple;</a></li>
			<li>En fonction du poids du fichier, cela peut prendre un certain temps à charger (plusieurs secondes), veuillez rester sur cette page jusqu'au chargement des nouvelles données.</li>
		</ul>
	</div>
	
	<table cellspacing="0" cellpadding="0" border="0" class="smaller">
		<tr>
			<th>Activity</th>
			<th>Make description</th>
			<th>Sipp Code</th>
			<th>Short Ctry description</th>
			<th>Fuel type P/D</th>
			<th>Registration number</th>
			<th>Registration date</th>
			<th>Unit number</th>
			<th>Chassis</th>
			<th>Driven Mileage</th>
			<th>In service date</th>
			<th>Projected sale date</th>
			<th>Vehicule Colour</th>
		</tr>
		<?PHP
		include('../inc/connexion.php');
		
		$sql = "SELECT activity, make_description, sipp_code, short_ctry_description, fuel_type_p_d, registration_number, registration_date, unit_number, chassis, driven_mileage, in_service_date, projected_sale_date, vehicule_colour FROM vehicules_ec WHERE registration_number <> '' ORDER BY registration_number ASC;";
		
		$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		mysql_close($link);
		
		$altRow = "";
		while ($row = mysql_fetch_array($result)) {
		?>
		<tr>
			<td<?=$altRow?>><?=$row['activity']?></td>
			<td<?=$altRow?>><?=$row['make_description']?></td>
			<td<?=$altRow?>><?=$row['sipp_code']?></td>
			<td<?=$altRow?>><?=$row['short_ctry_description']?></td>
			<td<?=$altRow?>><?=$row['fuel_type_p_d']?></td>
			<td<?=$altRow?>><?=$row['registration_number']?></td>
			<td<?=$altRow?>><?=SQLDatetoDDMMYY($row['registration_date'])?></td>
			<td<?=$altRow?>><?=$row['unit_number']?></td>
			<td<?=$altRow?>><?=$row['chassis']?></td>
			<td<?=$altRow?>><?=$row['driven_mileage']?></td>
			<td<?=$altRow?>><?=SQLDatetoDDMMYY($row['in_service_date'])?></td>
			<td<?=$altRow?>><?=SQLDatetoDDMMYY($row['projected_sale_date'])?></td>
			<td<?=$altRow?>><?=$row['vehicule_colour']?></td>
		</tr>
		<?PHP
			if ($altRow == "") {
				$altRow = " class=\"altrow\"";
			}else{
				$altRow = "";
			}
		}
		?>
	</table>
	
	<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
<script type="text/javascript">
$( document ).on( "click", "a#updatenow", function() {
	$(this).parent().next('div').show();
});
</script>
</body>
</html>
<?PHP
}
?>