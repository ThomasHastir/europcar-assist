<?php
	
session_name('SESSION2');
session_start();

include('../../inc/connexion-pdo.php');
	
	try {
				$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
				$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$statement = $db->prepare("SELECT `dep_mail` FROM `assist_depaneurs` WHERE `dep_mail` = :dep_mail AND `id` != :id;");
				$statement->execute(array('dep_mail' => $_GET['v'], 'id' => $_GET['i']));
				
				$isExisting = $statement->rowCount();
				
				
				
				if ($isExisting > 0) {
					header("HTTP/1.0 404 Not Found");
					die();
					
				}
				
	
	// Close connection
		$db = null;
		
	} catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
	
	?>