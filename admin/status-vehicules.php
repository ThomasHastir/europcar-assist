<?PHP
session_name('SESSION2');
session_start();

function SQLDatetoToHuman($date) {
	// from : 2015-08-31 00:00:00
	// to	: 31/08/2015 00:00
	$explode = explode(' ', $date);
	
	$date = explode('-', $explode[0]);
	
	
	$time = explode(':', $explode[1]); 
	
	return $date[2].'/'.$date[1].'/'.$date[0].'&nbsp;'.$time[0].':'.$time[1];
	
	}

if ($_SESSION['admin_connected'] == 1) {
	

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
	<h1>Statut en temps réel des véhicules</h1>
	<p>Ci-dessous, vous trouverez la liste de tous les véhicules en stock chez les dépanneurs ou en en cours de location.<br>Pour débloquer un véhicule, cliquez sur la croix.</p>
	<?=$errorMsg?>
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<th>Plaque d'immatriculation</th>
			<th>Marque</th>
			<th>Modèle</th>
			<th>Dépanneur</th>
			<th>Statut</th>
			<th>&nbsp;</th>
		</tr>
		<?PHP
		include('../inc/connexion.php');
		
		$sql = "SELECT `assist_cars`.id, `contrats`.date_start, `contrats`.date_fin, assist_cars.car_immatriculation, assist_cars.car_marque, assist_cars.car_model, assist_cars.car_status, `assist_depaneurs`.dep_nom FROM assist_cars LEFT JOIN `assist_depaneurs` ON `assist_depaneurs`.`id` = `assist_cars`.car_depaneur LEFT JOIN `contrats` ON `contrats`.`id` = `assist_cars`.contrat WHERE assist_cars.car_status != 5 AND assist_cars.car_status != 6 AND `assist_depaneurs`.dep_nom IS NOT NULL AND REPLACE(assist_cars.car_immatriculation, '-', '') IN (SELECT registration_number FROM vehicules_ec) ORDER BY `assist_depaneurs`.dep_nom ASC;";
		
		$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		
		mysql_close($link);
		$altRow = "";
		while ($row = mysql_fetch_array($result)) {
			
			switch ($row['car_status']) {
				case 0:
					$car_status = '<span style="color:#078f00;">Libre</span>';
					break;
				case 1:
					$car_status = '<span style="color:#6a6a6a;">Bloqué</span>';
					break;
				case 2:
					if ($row['date_fin']) {
						if (strtotime($row['date_fin']) > time()) {
						
							$car_status = '<span style="color:#ff7200;">En cours de location jusqu\'au '.SQLDatetoToHuman($row['date_fin']).'</span>';
						}else{
							$car_status = '<span style="color:#ff0000; font-weight:bold;">En cours de location jusqu\'au '.SQLDatetoToHuman($row['date_fin']).'</span>';
						}
					}else{
						$car_status = '<span style="color:#ff7200;">En cours de location jusqu\'au '.SQLDatetoToHuman($row['date_fin']).'</span>';
					}
					break;
				case 3:
					$car_status = '<span style="color:#078f00;">Libre</span>';
					break;
				case 4:
					$car_status = '<span style="color:#078f00;">Libre</span>';
					break;
				}
		?>
		<tr>
			<td<?=$altRow?>><?=strtoupper($row['car_immatriculation'])?></td>
			<td<?=$altRow?>><?=strtoupper($row['car_marque'])?></td>
			<td<?=$altRow?>><?=strtoupper($row['car_model'])?></td>
			<td<?=$altRow?>><?=strtoupper($row['dep_nom'])?></td>
			<td<?=$altRow?>><?=$car_status?></td>
			<td<?=$altRow?>><?php if($row['car_status'] == 1) { ?><a href="unblock-vehicule.php?id=<?=$row['id']?>" onclick="return confirm('Etes vous sûr?')"><img src="/img/delete.png" alt="" /></a><?php } ?></td>
		</tr>
		<?PHP
			if ($altRow == "") {
				$altRow = " class=\"altrow\"";
			}else{
				$altRow = "";
			}
		}
		
		?>
	</table>
	
	<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>
<?PHP
}
?>