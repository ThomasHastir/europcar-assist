<?PHP
session_name('SESSION2');
session_start();

$errorMsg = "";

if (isset($_POST['submit']) or $_COOKIE['admin_rememberme'] == 1) {
	
	if (isset($_POST['submit'])) {
		$_SESSION['adminLogin'] = $_POST['admin_usr'];
		$_SESSION['adminPwd'] = $_POST['admin_pwd'];
	}else{
		$_SESSION['adminLogin'] = $_COOKIE['admin_usr'];
		$_SESSION['adminPwd'] = $_COOKIE['admin_pwd'];
	}
	

	
	
	if ($_SESSION['adminLogin'] == 'Kurt.Vantrimpont@europcar.com' && $_SESSION['adminPwd'] == 'europcarkurt') {
			
			$_SESSION['admin_connected'] = 1;
			
			
			// Remember me
			if (isset($_POST['submit'])) {
				if ($_POST['admin_rememberme'] == 1) {
					setcookie("admin_rememberme", 1, time()+60*60*24*30);
					setcookie("admin_usr", $_POST['admin_usr'], time()+60*60*24*30);
					setcookie("admin_pwd", $_POST['admin_pwd'], time()+60*60*24*30);
				}else{
					setcookie("admin_rememberme", "");
				}
			}
			
	}else{
		$_SESSION['admin_connected'] = 0;
		session_destroy();
		setcookie("admin_rememberme", "");
		$errorMsg = 'Votre login/password semble incorrect. Merci de vérifier';
	}
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="../js/send.js"></script>
<script type="text/javascript">
function form_validate() {
	$error = 0;
	
	if ($('fieldset input:text').val() == '') {
		$('fieldset input:text').parent().css({ backgroundColor: "#fef0ef", border:"2px solid #ca3d38" });
		$error = 1;
	}else{
		$('fieldset input:text').parent().css({ backgroundColor: "#fff", border:"none" });
		$error = 0;
	}
	if ($('fieldset input:password').val() == '') {
		$('fieldset input:password').parent().css({ backgroundColor: "#fef0ef", border:"2px solid #ca3d38" });
		$error = 1;
	}else{
		$('fieldset input:password').parent().css({ backgroundColor: "#fff", border:"none" });
		$error = 0;
	}
	
	
	if ($error == 0) {
		document.forms['admin_form'].submit();
	}
}

<?PHP
if (strlen($errorMsg) > 0 ) {
	echo 'alert("', $errorMsg, '");';
	?>
	$(document).ready(function() {
 $('fieldset input:text').parent().css({ backgroundColor: "#fef0ef", border:"2px solid #ca3d38" });
	$('fieldset input:password').parent().css({ backgroundColor: "#fef0ef", border:"2px solid #ca3d38" });
});
	
	<?PHP
}
?>
</script>
</head>
<body>
<div id="header">
<?PHP
if ($_SESSION['admin_connected'] == 1) {
?>
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
<?PHP	
}
?>
</div>
<div id="container">
<div id="content">
<?PHP
if ($_SESSION['admin_connected'] == 1) {
	

/*
----------------
A chaque connexion sur le site, on update tous les véhicules bloqué depuis plus de 48h
----------------
*/


?>

<form name="form1" action="export/export-queries.php" method="post">
	<fieldset>
		<legend>Exporter l'historique des requêtes</legend>
		<p><label for="">Date de début:</label><br />
			<select name="startDay">
				<option value="00">JJ</option>
				<?PHP
				for ($i=1; $i<=31; $i++) {
					$ischecked = date('j') == $i ? ' selected' : '';
					if ($i < 10) {
						echo '<option value="0', $i,'"', $ischecked, '>0', $i,'</option>';
					}else{
						echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
					}
				}
				?>
			</select>&nbsp;
			<select name="startMonth">
				<option value="00">MM</option>
				<?PHP
				for ($i=1; $i<=12; $i++) {
					$ischecked = date('n') == $i ? ' selected' : '';
					if ($i < 10) {
						echo '<option value="0', $i,'"', $ischecked, '>0', $i,'</option>';
					}else{
						echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
					}
				}
				?>
			</select>&nbsp;
			<select name="startYear">
				<option value="0000">YYYY</option>
				<?PHP
				for ($i=2009; $i<=date('Y'); $i++) {
					$ischecked = date('Y') == $i ? ' selected' : '';
					echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
				}
				?>
			</select>
		</p>
		<p><label for="">Date de fin:</label><br />
			<select name="endDay">
				<option value="00">JJ</option>
				<?PHP
				for ($i=1; $i<=31; $i++) {
					$ischecked = date('j') == $i ? ' selected' : '';
					if ($i < 10) {
						echo '<option value="0', $i,'"', $ischecked, '>0', $i,'</option>';
					}else{
						echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
					}
				}
				?>
			</select>&nbsp;
			<select name="endMonth">
				<option value="00">MM</option>
				<?PHP
				for ($i=1; $i<=12; $i++) {
					$ischecked = date('n') == $i ? ' selected' : '';
					if ($i < 10) {
						echo '<option value="0', $i,'"', $ischecked, '>0', $i,'</option>';
					}else{
						echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
					}
				}
				?>
			</select>&nbsp;
			<select name="endYear">
				<option value="0000">YYYY</option>
				<?PHP
				for ($i=2009; $i<=date('Y'); $i++) {
					$ischecked = date('Y') == $i ? ' selected' : '';
					echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
				}
				?>
			</select>
		</p>
		<p><input type="submit" value="Exporter" /></p>
	</fieldset>
</form>

<form name="form2" action="export/export-refus.php" method="post">
	<fieldset>
		<legend>Exporter l'historique des refus</legend>
		<p><label for="">Date de début:</label><br />
			<select name="startDay2">
				<option value="00">JJ</option>
				<?PHP
				for ($i=1; $i<=31; $i++) {
					$ischecked = date('j') == $i ? ' selected' : '';
					if ($i < 10) {
						echo '<option value="0', $i,'"', $ischecked, '>0', $i,'</option>';
					}else{
						echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
					}
				}
				?>
			</select>&nbsp;
			<select name="startMonth2">
				<option value="00">MM</option>
				<?PHP
				for ($i=1; $i<=12; $i++) {
					$ischecked = date('n') == $i ? ' selected' : '';
					if ($i < 10) {
						echo '<option value="0', $i,'"', $ischecked, '>0', $i,'</option>';
					}else{
						echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
					}
				}
				?>
			</select>&nbsp;
			<select name="startYear2">
				<option value="0000">YYYY</option>
				<?PHP
				for ($i=2009; $i<=date('Y'); $i++) {
					$ischecked = date('Y') == $i ? ' selected' : '';
					echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
				}
				?>
			</select>
		</p>
		<p><label for="">Date de fin:</label><br />
			<select name="endDay2">
				<option value="00">JJ</option>
				<?PHP
				for ($i=1; $i<=31; $i++) {
					$ischecked = date('j') == $i ? ' selected' : '';
					if ($i < 10) {
						echo '<option value="0', $i,'"', $ischecked, '>0', $i,'</option>';
					}else{
						echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
					}
				}
				?>
			</select>&nbsp;
			<select name="endMonth2">
				<option value="00">MM</option>
				<?PHP
				for ($i=1; $i<=12; $i++) {
					$ischecked = date('n') == $i ? ' selected' : '';
					if ($i < 10) {
						echo '<option value="0', $i,'"', $ischecked, '>0', $i,'</option>';
					}else{
						echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
					}
				}
				?>
			</select>&nbsp;
			<select name="endYear2">
				<option value="0000">YYYY</option>
				<?PHP
				for ($i=2009; $i<=date('Y'); $i++) {
					$ischecked = date('Y') == $i ? ' selected' : '';
					echo '<option value="', $i,'"', $ischecked, '>', $i,'</option>';
				}
				?>
			</select>
		</p>
		<p><input type="submit" value="Exporter" /></p>
	</fieldset>
</form>

<form>
	<fieldset>
		<legend>Voir l'état des stocks par dépanneur en temps réel</legend>
		<p>
			<select name="depanneurs" id="depanneurs" onchange="getStats('window.location.href=this.options [this.selectedIndex].value')">
				<option>Sélectionnez un dépanneur</option>
			<?PHP
			include('../inc/connexion.php');
			
			$sql = "SELECT id, dep_nom FROM assist_depaneurs WHERE dep_assist = 0 ORDER BY dep_nom ASC;";
			
			//echo $sql;
			
			$result = mysql_query($sql) 
			or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
			mysql_close($link);
		
			
			while ($row = mysql_fetch_array($result)) {
				echo '<option value="', $row['id'], '">', $row['dep_nom'], '</option>';
			}
			?>
			</select>
		</p>
		<div id="showStats"></div>
	</fieldset>
</form>

<form>
	<fieldset>
		<legend>Voir l'état des stocks de tous les dépanneurs</legend>
		<p>
			<a href="export/export-all.php" target="_blank">Exporter au format XLS</a>
		</p>
	</fieldset>
</form>

<?PHP
// Requete pour sortir l'historique
#SELECT TB1.car_immatriculation, TB1.car_date, assist_assistance.assist_name, TB1.car_dossier, TB1.car_status, assist_depaneurs.dep_nom FROM ((assist_queries AS TB1 LEFT JOIN assist_depaneurs ON TB1.car_depaneur = assist_depaneurs.id) LEFT JOIN assist_assistance ON assist_assistance.id = TB1.car_assistance)  WHERE car_date > '2012-03-20 00:00:00' AND car_date < '2012-03-23 00:00:00'

// Sortir l'historique des refus

// Nombre de véhicules en stock/bloqués par dépanneur
?>

<p id="loadingMsg"></p>


<?PHP
} else {
?>
<h1>Bienvenue.</h1>
<p>Veuillez compléter les informations suivantes pour vous connecter.</p>
<form name="admin_form" id="admin_form" action="index.php" method="post" onsubmit="form_validate(this);return false;">
<fieldset>
	<legend>Mes identifiants</legend>
	<p><label for="admin_usr" class="cellLike">Login:</label> <input type="text" name="admin_usr" id="admin_usr" value="<?PHP if(isset($_POST['admin_usr'])) { ?><?=$_POST['admin_usr']?><?PHP } ?>" /></p>
	<p><label for="admin_pwd" class="cellLike">Password:</label> <input type="password" name="admin_pwd" id="admin_pwd" value="<?PHP if(isset($_POST['admin_pwd'])) { ?><?=$_POST['admin_pwd']?><?PHP } ?>" /></p>
</fieldset>
<fieldset>
	<legend>Mes options</legend>
	<p><input type="checkbox" name="admin_rememberme" id="admin_rememberme" value="1" /> <label for="admin_rememberme">Se souvenir de moi</label></p>
</fieldset>
<p>
	<input type="submit" name="submit" value="Se connecter" class="login_btn" />
</p>
</form>
<?PHP
};
?>
<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>