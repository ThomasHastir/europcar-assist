<?PHP
session_name('SESSION2');
session_start();

include('../inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

if ($_SESSION['admin_connected'] == 1) {
	
$dep_assist = $_GET['dep_assist'];

switch ($dep_assist) {
	case 0:
		$h1_title = 'Tous les dépanneurs';
		$p_text = 'un dépanneur';
		break;
	case 1:
		$h1_title = 'Toutes les sociétés d\'assistance';
		$p_text = 'une société d\'assistance';
		break;
	case 3:
		$h1_title = 'Toutes les agences Europcar';
		$p_text = 'une agence Europcar';
		break;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
	<h1><?=$h1_title?></h1>
	<p>Vous pouvez <a href="edit.php?dep_assist=<?=$dep_assist?>">ajouter</a> ou éditer <?=$p_text?> via la liste ci-dessous:</p>
	
	
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td<?php if ($dep_assist == 3) { echo ' width="50%"'; }?>>
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<th>Nom</th>
						<?PHP
						if ($dep_assist == 0) {
						?>
						<th>Code postal</th>
						<th>Ville</th>
						<th>Code</th>
						<th>E-mail</th>
						<?PHP
						}
						?>
						<th>&nbsp;</th>
						<?php if ($dep_assist == 1) { ?><th>&nbsp;</th><?php } ?>
						<th>&nbsp;</th>
						<?php if ($dep_assist == 0 || $dep_assist == 1) { ?><th>&nbsp;</th><?php } ?>
					</tr>
					<?PHP
			
					
					$statement = $db->prepare("SELECT id, dep_nom, dep_cp, dep_city, dep_code, dep_mail FROM assist_depaneurs WHERE dep_assist = :dep_assist ORDER BY dep_nom ASC;");
					$statement->execute(array('dep_assist' => $dep_assist));
					
					$altRow = "";
					foreach($statement as $row) {
					?>
					<tr>
						<td<?=$altRow?>><?=$row['dep_nom']?></td>
						<?PHP
						if ($dep_assist == 0) {
						?>
						<td<?=$altRow?>><?=$row['dep_cp']?></td>
						<td<?=$altRow?>><?=$row['dep_city']?></td>
						<td<?=$altRow?>><?=$row['dep_code']?></td>
						<td<?=$altRow?>><?=$row['dep_mail']?></td>
						<?PHP
						}
						?>
						<td<?=$altRow?>><a href="edit.php?dep_assist=<?=$dep_assist?>&amp;id=<?=$row['id']?>"><img src="/img/edit.png" alt="" /></a></td>
						<?php if ($dep_assist == 1) { ?><td<?=$altRow?>><a href="assist-condition.php?id=<?=$row['id']?>"><img src="/img/assistance_add.png" alt="" /></a></td><?php } ?>
						<td<?=$altRow?>><a href="delete.php?dep_assist=<?=$dep_assist?>&amp;id=<?=$row['id']?>" onclick="return confirm('Etes vous sûr?')"><img src="/img/delete.png" alt="" /></a></td>
						<?php if ($dep_assist == 0 || $dep_assist == 1) { ?>
						<?php
							// Generate hash for auto connect
							$secretKey = 'h0olhe4D!#sal';
							$hash = md5($dep_assist.$row['id'].$secretKey);
						?>
						<td<?=$altRow?>><a href="/?autoconnect=1&amp;dep_assist=<?=$dep_assist?>&amp;id=<?=$row['id']?>&amp;hash=<?=$hash?>" target="_blank"><img src="/img/key.png" alt="" /></a></td>
						<?php } ?>
					</tr>
					<?PHP
						if ($altRow == "") {
							$altRow = " class=\"altrow\"";
						}else{
							$altRow = "";
						}
					}
					?>
				</table>
			</td>
			
	
	<?PHP
	if ($dep_assist == 3) {
		
		?>
			<td valign="top" width="50%">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<th>Options</th>
					</tr>
					<tr>
						<td>
		<?php
		
		
		
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			
			echo '<p style="color:#690;">Modification enregistrée</p>';
			
			if($_POST['display_agencies'] == 1) {
				$display_agencies = 1;
				
			}else{
				$display_agencies = 0;
			}
			$statement = $db->prepare("UPDATE `assist_depaneurs` SET `active` = :active WHERE `dep_assist` = 3;");
			$statement->execute(array('active' => $display_agencies));
		}
		
		$statement = $db->prepare("SELECT `active` FROM `assist_depaneurs` WHERE `dep_assist` = 3 LIMIT 0,1");
		$statement->execute();
		
		$row = $statement->fetch(PDO::FETCH_ASSOC);
		$active = $row['active'];
		
	?>
	<form name="form1" id="form1" action="list.php?dep_assist=3" method="post" style="width:auto;">
		<p><label for="display_agencies"><input type="checkbox" name="display_agencies" id="display_agencies" value="1"<?php if($active == 1) { ?> checked="checked"<?php } ?>> Activer l'affichage des agences sur la plateforme</label></p>
		<p><input type="submit" value="Enregistrer"></p>
	</form>
						</td>
					</tr>
				</table>
			</td>
	<?php
		}
	?>
		</tr>
	</table>
	
	<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
<?php $statement = null; $db = null; ?>
</body>
</html>
<?PHP
}
?>