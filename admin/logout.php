<?PHP
session_name('SESSION2');
session_start();

session_destroy();
		
setcookie("admin_rememberme", 0, time()-3600);
setcookie("admin_usr", "", time()-3600);
setcookie("admin_pwd", "", time()-3600);

header('Location: /admin/index.php');
?>