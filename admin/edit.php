<?PHP
session_name('SESSION2');
session_start();

include('../inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

if ($_SESSION['admin_connected'] == 1) {
	
$dep_assist = $_GET['dep_assist'];
$id = $_GET['id'];

switch ($dep_assist) {
	case 0:
		$h1_title = 'Editer un dépanneur';
		break;
	case 1:
		$h1_title = 'Editer une société d\'assistance';
		break;
	case 3:
		$h1_title = 'Editer une agence Europcar';
		break;
	}
?>
<?PHP

if (isset($id)) {
	
	$statement = $db->prepare("SELECT * FROM assist_depaneurs WHERE id = :id;");
	$statement->execute(array('id' => $id));
	$row = $statement->fetch(PDO::FETCH_ASSOC);
	
	$dep_nom = $row['dep_nom'];
	$dep_address = $row['dep_address'];
	$dep_cp = $row['dep_cp'];
	$dep_city = $row['dep_city'];
	$dep_code = $row['dep_code'];
	$dep_tel = $row['dep_tel'];
	$dep_fax = $row['dep_fax'];
	$dep_mail = $row['dep_mail'];
	$dep_pwd = $row['dep_pwd'];
	$dep_lat = $row['dep_lat'];
	$dep_long = $row['dep_long'];
	
	$dep_relassistance = $row['dep_relassistance'];
	$dep_relassistance= explode(',',$dep_relassistance);
	
	$dep_mon_am_start = $row['dep_mon_am_start'];
	$dep_mon_am_end = $row['dep_mon_am_end'];
	$dep_mon_pm_start = $row['dep_mon_pm_start'];
	$dep_mon_pm_end = $row['dep_mon_pm_end'];
	$dep_tue_am_start = $row['dep_tue_am_start'];
	$dep_tue_am_end = $row['dep_tue_am_end'];
	$dep_tue_pm_start = $row['dep_tue_pm_start'];
	$dep_tue_pm_end = $row['dep_tue_pm_end'];
	$dep_wed_am_start = $row['dep_wed_am_start'];
	$dep_wed_am_end = $row['dep_wed_am_end'];
	$dep_wed_pm_start = $row['dep_wed_pm_start'];
	$dep_wed_pm_end = $row['dep_wed_pm_end'];
	$dep_thu_am_start = $row['dep_thu_am_start'];
	$dep_thu_am_end = $row['dep_thu_am_end'];
	$dep_thu_pm_start = $row['dep_thu_pm_start'];
	$dep_thu_pm_end = $row['dep_thu_pm_end'];
	$dep_fri_am_start = $row['dep_fri_am_start'];
	$dep_fri_am_end = $row['dep_fri_am_end'];
	$dep_fri_pm_start = $row['dep_fri_pm_start'];
	$dep_fri_pm_end = $row['dep_fri_pm_end'];
	$dep_sat_am_start = $row['dep_sat_am_start'];
	$dep_sat_am_end = $row['dep_sat_am_end'];
	$dep_sat_pm_start = $row['dep_sat_pm_start'];
	$dep_sat_pm_end = $row['dep_sat_pm_end'];
	$dep_sun_am_start = $row['dep_sun_am_start'];
	$dep_sun_am_end = $row['dep_sun_am_end'];
	$dep_sun_pm_start = $row['dep_sun_pm_start'];
	$dep_sun_pm_end = $row['dep_sun_pm_end'];
}else{
	$dep_nom = '';
	$dep_address = '';
	$dep_cp = '';
	$dep_city = '';
	$dep_code = '';
	$dep_tel = '';
	$dep_fax = '';
	$dep_mail = '';
	$dep_pwd = '';
	$dep_lat = '50.8503396';
	$dep_long = '4.3517103';
	
	$dep_relassistance = array();
	
	$dep_mon_am_start = '09:00:00';
	$dep_mon_am_end = '11:59:59';
	$dep_mon_pm_start = '13:00:00';
	$dep_mon_pm_end = '23:59:59';
	$dep_tue_am_start = '09:00:00';
	$dep_tue_am_end = '11:59:59';
	$dep_tue_pm_start = '13:00:00';
	$dep_tue_pm_end = '23:59:59';
	$dep_wed_am_start = '09:00:00';
	$dep_wed_am_end = '11:59:59';
	$dep_wed_pm_start = '13:00:00';
	$dep_wed_pm_end = '23:59:59';
	$dep_thu_am_start = '09:00:00';
	$dep_thu_am_end = '11:59:59';
	$dep_thu_pm_start = '13:00:00';
	$dep_thu_pm_end = '23:59:59';
	$dep_fri_am_start = '09:00:00';
	$dep_fri_am_end = '11:59:59';
	$dep_fri_pm_start = '13:00:00';
	$dep_fri_pm_end = '23:59:59';
	$dep_sat_am_start = '09:00:00';
	$dep_sat_am_end = '11:59:59';
	$dep_sat_pm_start = '13:00:00';
	$dep_sat_pm_end = '23:59:59';
	$dep_sun_am_start = '09:00:00';
	$dep_sun_am_end = '11:59:59';
	$dep_sun_pm_start = '13:00:00';
	$dep_sun_pm_end = '23:59:59';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
<script src="../js/parsley.min.js"></script>
<?PHP
if ($dep_assist == 0 || $dep_assist == 3) {
?>
<script type="text/javascript" src="../js/send.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.21.custom.min.js"></script>

<link href='../css/ui-darkness/jquery-ui-1.8.21.custom.css' rel='stylesheet' type='text/css'>
<script type="text/javascript">
$(function() {
	$("#closingday").datepicker({
		dateFormat: "dd/mm/yy",
		firstDay: 1,
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dayNamesMin: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam']
		});
		
		/*
		$('checkbox').click (function (){
			var thisCheck = $(this);
			if (thischeck.is (':checked'))
			{
			alert("coucou");
			}
		});
		*/
		
		$('.dep_allday').click (function (){
			if($(this).is (':checked')) {
				//alert("checked");	
				$(this).parent().parent().children('span').children('select').attr('disabled', 'disabled');
			}else{
				//alert("unchecked");
				$(this).parent().parent().children('span').children('select').removeAttr('disabled');
			}
		});
		
});

</script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var geocoder = new google.maps.Geocoder();
var map;
var marker

function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}
function updateMarkerPosition(latLng) {
	document.getElementById('dep_lat').value = latLng.lat();
	document.getElementById('dep_long').value = latLng.lng();
}
function updateMarkerAddress(str) {
  //document.getElementById('address').innerHTML = str;
}
function initialize() {
  var latLng = new google.maps.LatLng(<?=$dep_lat?>, <?=$dep_long?>);
  map = new google.maps.Map(document.getElementById('mapCanvas'), {
    zoom: 13,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  marker = new google.maps.Marker({
    position: latLng,
    title: 'Point A',
    map: map,
    draggable: true
  });
  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);
  
  // Add dragging event listeners.
  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...');
  });
  
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerPosition(marker.getPosition());
  });
  
  google.maps.event.addListener(marker, 'dragend', function() {
    geocodePosition(marker.getPosition());
  });
}

function codeAddress() {
	var address = document.getElementById("dep_address").value+", "+document.getElementById("dep_cp").value+" "+document.getElementById("dep_city").value;
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			document.getElementById('dep_lat').value = results[0].geometry.location.lat();
			document.getElementById('dep_long').value = results[0].geometry.location.lng();
			map.setCenter(results[0].geometry.location);
		    var newLatLng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()); 
 			marker.setPosition(newLatLng)
		}
	})
}
// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?PHP
}
?>
<!-- Parsley -->
	    <script>
	
	      $(document).ready(function() {
		      
		      //has uppercase
window.Parsley.addValidator('uppercase', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var uppercases = value.match(/[A-Z]/g) || [];
    return uppercases.length >= requirement;
  },
  messages: {
    en: 'Votre mot de passe doit contenir au minimum %s lettre majuscule.'
  }
});

//has lowercase
window.Parsley.addValidator('lowercase', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var lowecases = value.match(/[a-z]/g) || [];
    return lowecases.length >= requirement;
  },
  messages: {
    en: 'Votre mot de passe doit contenir au minimum %s lettre minuscule.'
  }
});

//has number
window.Parsley.addValidator('number', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var numbers = value.match(/[0-9]/g) || [];
    return numbers.length >= requirement;
  },
  messages: {
    en: 'Votre mot de passe doit contenir au minimum %s chiffre.'
  }
});

//has special char
window.Parsley.addValidator('special', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var specials = value.match(/[^a-zA-Z0-9]/g) || [];
    return specials.length >= requirement;
  },
  messages: {
    en: 'Votre mot de passe doit contenir au minimum %s caractère spécial ($ ! %, etc..).'
  }
});

window.Parsley.addValidator('userexists', {
			  validateString: function(value) {
			    return $.ajax('ajax_calls/checkuserexists.php?v=' + value + '&i=<?=$_GET['id']?>')
			  },
			  messages: {en: 'Chosen username already exists. Please choose another one.'}
			});
		      

		      
	        window.Parsley.on('parsley:field:validate', function() {
	          validateFront();
	        });
	        $('#form1 input[type="submit"]').on('click', function() {
	          $('#form1').parsley().validate();
	          validateFront();
	        });
	        var validateFront = function() {
	          if (true === $('#form1').parsley().isValid()) {
	            $('.bs-callout-info').removeClass('hidden');
	            $('.bs-callout-warning').addClass('hidden');
	          } else {
	            $('.bs-callout-info').addClass('hidden');
	            $('.bs-callout-warning').removeClass('hidden');
	          }
	        };
	        
	        
	        
	        
	  
	        
	        
	        
	      });
	      try {
	        hljs.initHighlightingOnLoad();
	      } catch (err) {}
	    </script>
	    <!-- /Parsley -->
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
	<h1><?=$h1_title?></h1>
	<form name="form1" id="form1" action="edit_action.php" data-parsley-validate enctype="multipart/form-data" method="post">
		<?PHP
		if ($dep_assist == 0 || $dep_assist == 3) {
		?>
		<div id="mapCanvas" style="width:500px; height:200px;"></div>
		<?PHP
		}
		?>
		<fieldset>
			<legend>Identifiants</legend>
			<p><span><label for="dep_mail" class="cellLike"><strong>E-mail: </strong></label><input type="email" name="dep_mail" id="dep_mail" required="required" data-parsley-userexists="1" value="<?=$dep_mail?>" /></span><br />
			<span><label for="dep_pwd" class="cellLike"><strong>Password: </strong></label><input type="text" name="dep_pwd" id="dep_pwd"<?php if(!$_GET['id']) { ?> required="required"<?php } ?> data-parsley-minlength="8" data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1" data-parsley-special="1" value="" /></span> <span style="font-size:9px;">Laissez vide si vous ne souhaitez pas le changer</span></p>
		</fieldset>
		<fieldset>
			<legend>Coordonnées</legend>
			<p><span><label for="dep_img" class="cellLike">Logo: </label><input type="file" name="dep_img" id="dep_img" /></span><br />
			<span><label for="dep_nom" class="cellLike">Nom: </label><input type="text" name="dep_nom" id="dep_nom" value="<?=$dep_nom?>" /></span>
			<?PHP
			if ($dep_assist == 0 || $dep_assist == 3) {
			?>
			<br /><span><label for="dep_address" class="cellLike">Adresse: </label><input type="text" name="dep_address" id="dep_address" value="<?=$dep_address?>" /></span><br />
			<span><label for="dep_cp" class="cellLike">Code postal: </label><input type="text" name="dep_cp" id="dep_cp" value="<?=$dep_cp?>" /></span><br />
			<span><label for="dep_city" class="cellLike">Ville: </label><input type="text" name="dep_city" id="dep_city" value="<?=$dep_city?>" /></span> <input type="button" name="" id="" value="Positionner sur la carte" onclick="codeAddress();" /><br />
			<span><label for="dep_code" class="cellLike">Code: </label><input type="text" name="dep_code" id="dep_code" value="<?=$dep_code?>" /></span><br />
			<span><label for="dep_tel" class="cellLike">Téléphone: </label><input type="text" name="dep_tel" id="dep_tel" value="<?=$dep_tel?>" /></span><br />
			<span><label for="dep_fax" class="cellLike">Fax: </label><input type="text" name="dep_fax" id="dep_fax" value="<?=$dep_fax?>" /></span>
			<?PHP
			}
			?>
			</p>
		</fieldset>
		

		
		
		<?PHP
		if ($dep_assist == 0 || $dep_assist == 3) {
		?>
		<fieldset>
			<legend>Heures d'ouverture</legend>
			<?PHP
			function ecrireHoraires($jour, $ampm, $startend, $defaut) {
				
				if (($defaut == '23:50:00' && $ampm == 'am' && $startend == 'start') || ($defaut == '23:50:01' && $ampm == 'am' && $startend == 'end') || ($defaut == '08:00:01' && $ampm == 'pm' && $startend == 'start') || ($defaut == '08:00:02' && $ampm == 'pm' && $startend == 'end')) {
					$closeAllDay = ' disabled="disabled"';
					$closeAllDayCB = ' checked="checked"';
				}else{
					$closeAllDay = '';
					$closeAllDayCB = '';
				}
				
				if ($ampm == 'am') {
					$heureMin = 0;
					$heureMax = 12;
					
				}else{
					$heureMin = 12;
					$heureMax = 24;
				}
				
				echo '<select name="dep_', $jour, '_', $ampm, '_', $startend, '" id="dep_', $jour, '_', $ampm, '_', $startend, '" ', $closeAllDay, '>';
					
					for ( $heures = $heureMin ; $heures < $heureMax ; $heures++ ) {
						for ( $minutes = 0 ; $minutes <= 45 ; $minutes += 15 ) {
							if ($heures < 10 && strlen($heures) < 2) {
								$heures = "0".$heures;
							}
							if ($minutes < 10) {
								$minutes = "0".$minutes;
							}
							
							
							$heureString = $heures.":".$minutes.":00";
							if ($defaut == $heureString) {
								$isSelected = " selected = 'selected'";
							}else{
								$isSelected = "";	
							}
					
						echo '<option value="', $heures, ':', $minutes, ':00', '"', $isSelected, '>', $heures, ':', $minutes, '</option>';
					
						}
					}
					$tempHeure = $heureMax-1;
					$heureString = $tempHeure.":59:59";
					if ($defaut == $heureString) {
						$isSelected = " selected = 'selected'";
					}else{
						$isSelected = "";	
					}
					echo '<option value="', $heureMax-1, ':59:59"', $isSelected, '>', $heureMax, ':00</option>';
				echo '</select>';
				
				if ($ampm == 'pm' && $startend == 'end') {
					echo '<input type="checkbox" name="dep_', $jour, '" id="dep_', $jour, '" value="1" class="dep_allday" ', $closeAllDayCB, ' /><label for="dep_', $jour, '"> Fermé toute la journée</label>';
				}
				
			
			}
			?>
			<p><strong>Lundi</strong><br />
				<span>
					<label for="dep_mon_am_start" class="cellLike">Matin: </label>
					<?PHP
						ecrireHoraires('mon', 'am', 'start', $dep_mon_am_start);
					?> à 
					<?PHP
						ecrireHoraires('mon', 'am', 'end', $dep_mon_am_end);
					?>
				</span><br />
				<span>
					<label for="dep_mon_pm_start" class="cellLike">Après-midi: </label>
					<?PHP
						ecrireHoraires('mon', 'pm', 'start', $dep_mon_pm_start);
					?> à 
					<?PHP
						ecrireHoraires('mon', 'pm', 'end', $dep_mon_pm_end);
					?>
				</span><br />
			</p>
			<p><strong>Mardi</strong><br />
				<span>
					<label for="dep_tue_am_start" class="cellLike">Matin: </label>
					<?PHP
						ecrireHoraires('tue', 'am', 'start', $dep_tue_am_start);
					?> à 
					<?PHP
						ecrireHoraires('tue', 'am', 'end', $dep_tue_am_end);
					?>
				</span><br />
				<span>
					<label for="dep_tue_pm_start" class="cellLike">Après-midi: </label>
					<?PHP
						ecrireHoraires('tue', 'pm', 'start', $dep_tue_pm_start);
					?> à 
					<?PHP
						ecrireHoraires('tue', 'pm', 'end', $dep_tue_pm_end);
					?>
				</span><br />
			</p>
			<p><strong>Mercredi</strong><br />
				<span>
					<label for="dep_wed_am_start" class="cellLike">Matin: </label>
					<?PHP
						ecrireHoraires('wed', 'am', 'start', $dep_wed_am_start);
					?> à 
					<?PHP
						ecrireHoraires('wed', 'am', 'end', $dep_wed_am_end);
					?>
				</span><br />
				<span>
					<label for="dep_wed_pm_start" class="cellLike">Après-midi: </label>
					<?PHP
						ecrireHoraires('wed', 'pm', 'start', $dep_wed_pm_start);
					?> à 
					<?PHP
						ecrireHoraires('wed', 'pm', 'end', $dep_wed_pm_end);
					?>
				</span><br />
			</p>
			<p><strong>Jeudi</strong><br />
				<span>
					<label for="dep_thu_am_start" class="cellLike">Matin: </label>
					<?PHP
						ecrireHoraires('thu', 'am', 'start', $dep_thu_am_start);
					?> à 
					<?PHP
						ecrireHoraires('thu', 'am', 'end', $dep_thu_am_end);
					?>
				</span><br />
				<span>
					<label for="dep_thu_pm_start" class="cellLike">Après-midi: </label>
					<?PHP
						ecrireHoraires('thu', 'pm', 'start', $dep_thu_pm_start);
					?> à 
					<?PHP
						ecrireHoraires('thu', 'pm', 'end', $dep_thu_pm_end);
					?>
				</span><br />
			</p>
			<p><strong>Vendredi</strong><br />
				<span>
					<label for="dep_fri_am_start" class="cellLike">Matin: </label>
					<?PHP
						ecrireHoraires('fri', 'am', 'start', $dep_fri_am_start);
					?> à 
					<?PHP
						ecrireHoraires('fri', 'am', 'end', $dep_fri_am_end);
					?>
				</span><br />
				<span>
					<label for="dep_fri_pm_start" class="cellLike">Après-midi: </label>
					<?PHP
						ecrireHoraires('fri', 'pm', 'start', $dep_fri_pm_start);
					?> à 
					<?PHP
						ecrireHoraires('fri', 'pm', 'end', $dep_fri_pm_end);
					?>
				</span><br />
			</p>
			<p><strong>Samedi</strong><br />
				<span>
					<label for="dep_sat_am_start" class="cellLike">Matin: </label>
					<?PHP
						ecrireHoraires('sat', 'am', 'start', $dep_sat_am_start);
					?> à 
					<?PHP
						ecrireHoraires('sat', 'am', 'end', $dep_sat_am_end);
					?>
				</span><br />
				<span>
					<label for="dep_sat_pm_start" class="cellLike">Après-midi: </label>
					<?PHP
						ecrireHoraires('sat', 'pm', 'start', $dep_sat_pm_start);
					?> à 
					<?PHP
						ecrireHoraires('sat', 'pm', 'end', $dep_sat_pm_end);
					?>
				</span><br />
			</p>
			<p><strong>Dimanche</strong><br />
				<span>
					<label for="dep_sun_am_start" class="cellLike">Matin: </label>
					<?PHP
						ecrireHoraires('sun', 'am', 'start', $dep_sun_am_start);
					?> à 
					<?PHP
						ecrireHoraires('sun', 'am', 'end', $dep_sun_am_end);
					?>
				</span><br />
				<span>
					<label for="dep_sun_pm_start" class="cellLike">Après-midi: </label>
					<?PHP
						ecrireHoraires('sun', 'pm', 'start', $dep_sun_pm_start);
					?> à 
					<?PHP
						ecrireHoraires('sun', 'pm', 'end', $dep_sun_pm_end);
					?>
				</span><br />
			</p>
		</fieldset>
		<fieldset>
			<legend>Jours de fermeture</legend>
			<div id="closingdayContainer">
			<?PHP
			$statement = $db->prepare("SELECT * FROM assist_horaires WHERE horaire_dep = :id ORDER BY horaire_jour ASC;");
			$statement->execute(array('id' => $id));
			
			$ajaxString = "<p>";
			foreach($statement as $row) {
				$date = $row['horaire_jour'];
				$timestamp = strtotime(date("Y-m-d H:i:s", strtotime($date)));
				$expired = date('d/m/Y', $timestamp);
				$ajaxString .= $expired." <a href=\"javascript:addClosingDay(\'\', \'".$zeid."\', ".$row['id'].");\"><img src=\"/img/delete.png\" align=\"top\" style=\"border:0;\" alt=\"\" /></a><br />";
			}
			$ajaxString .= "</p>";
			?>
			<?=$ajaxString?>
			</div>
			<p id="loadingMsg"></p>
			<p><input type="text" name="closingday" id="closingday" value="" /> <input type="button" value="Ajouter" onclick="addClosingDay(document.getElementById('closingday').value, <?=$id?>, 0);" /></p>
		</fieldset>
		<fieldset>
			<legend>Assistances</legend>
			<p>Ce dépanneur travaille avec les sociétés d'assistance suivantes:</p>
			<p>
			<?PHP
			
			$statement = $db->prepare("SELECT id, dep_nom FROM assist_depaneurs WHERE dep_assist = 1 ORDER BY dep_nom ASC;");
			$statement->execute();
			
			foreach($statement as $row) {
				$dep_id = $row['id'];
				$dep_nom = $row['dep_nom'];
				
				
				if(in_array($dep_id,$dep_relassistance)) {
					$isChecked = " checked=\"checked\"";
				}else{
					$isChecked = "";	
				}
				
				echo '<input type="checkbox" name="rel_assistance[]" id="rel_assistance_', $dep_id, '" value="', $dep_id, '"', $isChecked, ' /> <label for="rel_assistance_', $dep_id, '">', $dep_nom, '</label><br />';
			}
			?>
			</p>
		</fieldset>
		<?PHP
		}
		?>
		<p>
		<?PHP
		if ($dep_assist == 0 || $dep_assist == 3) {
		?>
			<input type="hidden" name="dep_lat" id="dep_lat" value="<?=$dep_lat?>" />
			<input type="hidden" name="dep_long" id="dep_long" value="<?=$dep_long?>" />
		<?PHP
		}
		?>
			<input type="hidden" name="id" id="id" value="<?=$id?>" />
			<input type="hidden" name="dep_assist" id="dep_assist" value="<?=$dep_assist?>" />
			<input type="submit" value="Valider" /></p>
	</form>
	<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
<?php $statement = null; $db = null; ?>
</body>
</html>
<?PHP
}
?>