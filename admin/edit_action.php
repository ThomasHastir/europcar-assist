<?PHP
session_name('SESSION2');
session_start();

include('../inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

function redimage($img_src,$img_dest,$dst_w,$dst_h) {
   // Lit les dimensions de l'image
   $size = GetImageSize($img_src);  
   $src_w = $size[0]; $src_h = $size[1];  
   // Teste les dimensions tenant dans la zone
   $test_h = round(($dst_w / $src_w) * $src_h);
   $test_w = round(($dst_h / $src_h) * $src_w);
   // Si Height final non précisé (0)
   if(!$dst_h) $dst_h = $test_h;
   // Sinon si Width final non précisé (0)
   elseif(!$dst_w) $dst_w = $test_w;
   // Sinon teste quel redimensionnement tient dans la zone
   elseif($test_h>$dst_h) $dst_w = $test_w;
   else $dst_h = $test_h;

   // La vignette existe ?
   $test = (file_exists($img_dest));
   // L'original a été modifié ?
   if($test)
      $test = (filemtime($img_dest)>filemtime($img_src));
   // Les dimensions de la vignette sont correctes ?
   if($test) {
      $size2 = GetImageSize($img_dest);
      $test = ($size2[0]==$dst_w);
      $test = ($size2[1]==$dst_h);
   }

   // Créer la vignette ?
   if(!$test) {
      // Crée une image vierge aux bonnes dimensions
      // $dst_im = ImageCreate($dst_w,$dst_h);
      $dst_im = ImageCreateTrueColor($dst_w,$dst_h); 
      // Copie dedans l'image initiale redimensionnée
      $src_im = ImageCreateFromJpeg($img_src);
      // ImageCopyResized($dst_im,$src_im,0,0,0,0,$dst_w,$dst_h,$src_w,$src_h);
      ImageCopyResampled($dst_im,$src_im,0,0,0,0,$dst_w,$dst_h,$src_w,$src_h);
      // Sauve la nouvelle image
      ImageJpeg($dst_im,$img_dest, 80);
      // Détruis les tampons
      ImageDestroy($dst_im);  
      ImageDestroy($src_im);
      
    // Changement des modes des images
    chmod($img_dest, octdec('777'));
   }

   
}

$id = $_POST['id'];

$dep_mail = $_POST['dep_mail'];
$dep_pwd = $_POST['dep_pwd'];
$dep_nom = $_POST['dep_nom'];

$dep_address = $_POST['dep_address'];
$dep_cp = $_POST['dep_cp'];
$dep_city = $_POST['dep_city'];
$dep_code = $_POST['dep_code'];
$dep_tel = $_POST['dep_tel'];
$dep_fax = $_POST['dep_fax'];
$dep_lat = $_POST['dep_lat'];
$dep_long = $_POST['dep_long'];

$dep_assist = $_POST['dep_assist'];


$dep_rel = implode(',', $_POST['rel_assistance']);

$dep_mon = $_POST['dep_mon'];
if($dep_mon == 1) {
	$dep_mon_am_start = "23:50:00";
	$dep_mon_am_end = "23:50:01";
	$dep_mon_pm_start = "08:00:01";
	$dep_mon_pm_end = "08:00:02";
}else{
	$dep_mon_am_start = $_POST['dep_mon_am_start'];
	$dep_mon_am_end = $_POST['dep_mon_am_end'];
	$dep_mon_pm_start = $_POST['dep_mon_pm_start'];
	$dep_mon_pm_end = $_POST['dep_mon_pm_end'];
}


$dep_tue = $_POST['dep_tue'];
if($dep_tue == 1) {
	$dep_tue_am_start = "23:50:00";
	$dep_tue_am_end = "23:50:01";
	$dep_tue_pm_start = "08:00:01";
	$dep_tue_pm_end = "08:00:02";
}else{
	$dep_tue_am_start = $_POST['dep_tue_am_start'];
	$dep_tue_am_end = $_POST['dep_tue_am_end'];
	$dep_tue_pm_start = $_POST['dep_tue_pm_start'];
	$dep_tue_pm_end = $_POST['dep_tue_pm_end'];
}

$dep_wed = $_POST['dep_wed'];
if($dep_wed == 1) {
	$dep_wed_am_start = "23:50:00";
	$dep_wed_am_end = "23:50:01";
	$dep_wed_pm_start = "08:00:01";
	$dep_wed_pm_end = "08:00:02";
}else{
	$dep_wed_am_start = $_POST['dep_wed_am_start'];
	$dep_wed_am_end = $_POST['dep_wed_am_end'];
	$dep_wed_pm_start = $_POST['dep_wed_pm_start'];
	$dep_wed_pm_end = $_POST['dep_wed_pm_end'];
}

$dep_thu = $_POST['dep_thu'];
if($dep_thu == 1) {
	$dep_thu_am_start = "23:50:00";
	$dep_thu_am_end = "23:50:01";
	$dep_thu_pm_start = "08:00:01";
	$dep_thu_pm_end = "08:00:02";
}else{
	$dep_thu_am_start = $_POST['dep_thu_am_start'];
	$dep_thu_am_end = $_POST['dep_thu_am_end'];
	$dep_thu_pm_start = $_POST['dep_thu_pm_start'];
	$dep_thu_pm_end = $_POST['dep_thu_pm_end'];
}

$dep_fri = $_POST['dep_fri'];
if($dep_fri == 1) {
	$dep_fri_am_start = "23:50:00";
	$dep_fri_am_end = "23:50:01";
	$dep_fri_pm_start = "08:00:01";
	$dep_fri_pm_end = "08:00:02";
}else{
	$dep_fri_am_start = $_POST['dep_fri_am_start'];
	$dep_fri_am_end = $_POST['dep_fri_am_end'];
	$dep_fri_pm_start = $_POST['dep_fri_pm_start'];
	$dep_fri_pm_end = $_POST['dep_fri_pm_end'];
}

$dep_sat = $_POST['dep_sat'];
if($dep_sat == 1) {
	$dep_sat_am_start = "23:50:00";
	$dep_sat_am_end = "23:50:01";
	$dep_sat_pm_start = "08:00:01";
	$dep_sat_pm_end = "08:00:02";
}else{
	$dep_sat_am_start = $_POST['dep_sat_am_start'];
	$dep_sat_am_end = $_POST['dep_sat_am_end'];
	$dep_sat_pm_start = $_POST['dep_sat_pm_start'];
	$dep_sat_pm_end = $_POST['dep_sat_pm_end'];

}
$dep_sun = $_POST['dep_sun'];
if($dep_sun == 1) {
	$dep_sun_am_start = "23:50:00";
	$dep_sun_am_end = "23:50:01";
	$dep_sun_pm_start = "08:00:01";
	$dep_sun_pm_end = "08:00:02";
}else{
	$dep_sun_am_start = $_POST['dep_sun_am_start'];
	$dep_sun_am_end = $_POST['dep_sun_am_end'];
	$dep_sun_pm_start = $_POST['dep_sun_pm_start'];
	$dep_sun_pm_end = $_POST['dep_sun_pm_end'];
}




$img = $_FILES["dep_img"]["tmp_name"];

if($img != '') {
$filename = $_FILES["dep_img"]["name"];

// Traitement images
$path_img = "../img/depanneurs";
move_uploaded_file ($img, "../img/depanneurs/temp".$filename);
$fileextension = substr($filename, -3);

$newfilename = time();

redimage("../img/depanneurs/temp".$filename, $path_img."/".$newfilename.".".strtolower($fileextension), "280", "140");

unlink("../img/depanneurs/temp".$filename);

	$sqlImg = $newfilename;

}else{
	$sqlImg = "";
}

switch ($_POST['dep_assist']) {
	case 0:
	case 3:		
		if ($id > 0) {
			$sql = "UPDATE assist_depaneurs";
			$sql .= " SET dep_mail = '".$dep_mail."', dep_nom = '".$dep_nom."', dep_address = '".$dep_address."', dep_cp = '".$dep_cp."', dep_city = '".$dep_city."', dep_code = '".$dep_code."', dep_tel = '".$dep_tel."', dep_fax = '".$dep_fax."', dep_lat = ".$dep_lat.", dep_long = ".$dep_long.", dep_img = '".$sqlImg."'";
			
			if ($dep_pwd) {
				$sql .= ", dep_pwd = '".md5($dep_pwd)."'";
			}
			
			
			$sql .= " , dep_relassistance = '".$dep_rel."'";	
				
			$sql .= " , dep_mon_am_start = '".$dep_mon_am_start."'";
			$sql .= " , dep_mon_am_end = '".$dep_mon_am_end."'";
			$sql .= " , dep_mon_pm_start = '".$dep_mon_pm_start."'";
			$sql .= " , dep_mon_pm_end = '".$dep_mon_pm_end."'";
			
			$sql .= " , dep_tue_am_start = '".$dep_tue_am_start."'";
			$sql .= " , dep_tue_am_end = '".$dep_tue_am_end."'";
			$sql .= " , dep_tue_pm_start = '".$dep_tue_pm_start."'";
			$sql .= " , dep_tue_pm_end = '".$dep_tue_pm_end."'";
			
			$sql .= " , dep_wed_am_start = '".$dep_wed_am_start."'";
			$sql .= " , dep_wed_am_end = '".$dep_wed_am_end."'";
			$sql .= " , dep_wed_pm_start = '".$dep_wed_pm_start."'";
			$sql .= " , dep_wed_pm_end = '".$dep_wed_pm_end."'";
			
			$sql .= " , dep_thu_am_start = '".$dep_thu_am_start."'";
			$sql .= " , dep_thu_am_end = '".$dep_thu_am_end."'";
			$sql .= " , dep_thu_pm_start = '".$dep_thu_pm_start."'";
			$sql .= " , dep_thu_pm_end = '".$dep_thu_pm_end."'";
			
			$sql .= " , dep_fri_am_start = '".$dep_fri_am_start."'";
			$sql .= " , dep_fri_am_end = '".$dep_fri_am_end."'";
			$sql .= " , dep_fri_pm_start = '".$dep_fri_pm_start."'";
			$sql .= " , dep_fri_pm_end = '".$dep_fri_pm_end."'";
			
			$sql .= " , dep_sat_am_start = '".$dep_sat_am_start."'";
			$sql .= " , dep_sat_am_end = '".$dep_sat_am_end."'";
			$sql .= " , dep_sat_pm_start = '".$dep_sat_pm_start."'";
			$sql .= " , dep_sat_pm_end = '".$dep_sat_pm_end."'";
			
			$sql .= " , dep_sun_am_start = '".$dep_sun_am_start."'";
			$sql .= " , dep_sun_am_end = '".$dep_sun_am_end."'";
			$sql .= " , dep_sun_pm_start = '".$dep_sun_pm_start."'";
			$sql .= " , dep_sun_pm_end = '".$dep_sun_pm_end."'";
			
			$sql .= " WHERE id=".$id."";
		}else{
			$sql = "INSERT INTO assist_depaneurs (dep_mail, dep_pwd, dep_nom, dep_address, dep_cp, dep_city, dep_code, dep_tel, dep_fax, dep_lat, dep_long, dep_assist, dep_img, dep_relassistance, dep_mon_am_start, dep_mon_am_end, dep_mon_pm_start, dep_mon_pm_end, dep_tue_am_start, dep_tue_am_end, dep_tue_pm_start, dep_tue_pm_end, dep_wed_am_start, dep_wed_am_end, dep_wed_pm_start, dep_wed_pm_end, dep_thu_am_start, dep_thu_am_end, dep_thu_pm_start, dep_thu_pm_end, dep_fri_am_start, dep_fri_am_end, dep_fri_pm_start, dep_fri_pm_end, dep_sat_am_start, dep_sat_am_end, dep_sat_pm_start, dep_sat_pm_end, dep_sun_am_start, dep_sun_am_end, dep_sun_pm_start, dep_sun_pm_end)";
			
			$sql .= " VALUES ('".$dep_mail."', '".$dep_pwd."', '".$dep_nom."', '".$dep_address."', '".$dep_cp."', '".$dep_city."', '".$dep_code."', '".$dep_tel."', '".$dep_fax."', ".$dep_lat.", ".$dep_long.", '".$dep_assist."', '".$dep_img."', '".$dep_rel."', '".$dep_mon_am_start."', '".$dep_mon_am_end."', '".$dep_mon_pm_start."', '".$dep_mon_pm_end."', '".$dep_tue_am_start."', '".$dep_tue_am_end."', '".$dep_tue_pm_start."', '".$dep_tue_pm_end."', '".$dep_wed_am_start."', '".$dep_wed_am_end."', '".$dep_wed_pm_start."', '".$dep_wed_pm_end."', '".$dep_thu_am_start."', '".$dep_thu_am_end."', '".$dep_thu_pm_start."', '".$dep_thu_pm_end."', '".$dep_fri_am_start."', '".$dep_fri_am_end."', '".$dep_fri_pm_start."', '".$dep_fri_pm_end."', '".$dep_sat_am_start."', '".$dep_sat_am_end."', '".$dep_sat_am_end."', '".$dep_sat_pm_end."', '".$dep_sun_am_start."', '".$dep_sun_am_end."', '".$dep_sun_pm_start."', '".$dep_sun_pm_end."')";
		}
		break;
	case 1:
		if ($id > 0) {
			$sql = "UPDATE assist_depaneurs";
			$sql .= " SET dep_mail = '".$dep_mail."', dep_pwd = '".$dep_pwd."', dep_nom = '".$dep_nom."', dep_img = '".$sqlImg."'";
			$sql .= " WHERE id=".$id."";
		}else{
			$sql = "INSERT INTO assist_depaneurs (dep_mail, dep_pwd, dep_nom, dep_assist, dep_img)";
$sql .= " VALUES ('".$dep_mail."', '".$dep_pwd."', '".$dep_nom."', 1, '".$dep_img."')";
		}
		break;
	}
	
include('../inc/connexion.php');

$statement = $db->prepare($sql);
$statement->execute();

$statement = null; $db = null;

header("Location: /admin/list.php?dep_assist=".$_POST['dep_assist']);



?>