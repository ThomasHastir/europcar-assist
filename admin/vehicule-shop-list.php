<?PHP
session_name('SESSION2');
session_start();

if ($_SESSION['admin_connected'] == 1) {
	

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='../css/layout.css' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
</head>
<body>
<div id="header">
	<ul>
		<li><a href="index.php">Accueil</a></li>
		<li><a href="list.php?dep_assist=0">Dépanneurs</a></li>
		<li><a href="list.php?dep_assist=3">Agences EC</a></li>
		<li><a href="list.php?dep_assist=1">Assistances</a></li>
		<li><a href="status-vehicules.php">Statut véhicules</a></li>
		<li><a href="vehicules.php">Véhicules en flotte</a></li>
		<li><a href="ea-list.php">Véhicules EA</a></li>
		<li><a href="vehicule-shop-list.php">Véhicules BB/SHOP</a></li>
		<li><a href="logout.php">Se déconnecter</a></li>
	</ul>
</div>
<div id="container">
<div id="content">
	<h1>VEHICULE VENTE &amp; SHOP</h1>
	<p>Ci-dessous, vous trouverez la liste de tous les véhicules placés en VENTE ou SHOP. Pour enlever un véhicule, cliquez sur la croix.</p>
	<?=$errorMsg?>
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<th>Plaque d'immatriculation</th>
			<th>Marque</th>
			<th>Modèle</th>
			<th>Dépanneur</th>
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
		<?PHP
		include('../inc/connexion.php');
		
		$sql = "SELECT `assist_cars`.id, car_immatriculation, car_marque, car_model, assist_depaneurs.`dep_nom`, `assist_cars`.`car_status` FROM `assist_cars` LEFT JOIN assist_depaneurs ON `assist_cars`.car_depaneur = assist_depaneurs.`id` WHERE (car_status_BB = 1 AND car_status = 5) OR car_status = 6;";
		
		$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		mysql_close($link);
		
		$altRow = "";
		while ($row = mysql_fetch_array($result)) {
			
		switch ($row['car_status']) {
			case 5:
				$status = "Vente";
				break;
			case 6:
				$status = "Shop";
				break;
			}
			
		?>
		<tr>
			<td<?=$altRow?>><?=strtoupper($row['car_immatriculation'])?></td>
			<td<?=$altRow?>><?=strtoupper($row['car_marque'])?></td>
			<td<?=$altRow?>><?=strtoupper($row['car_model'])?></td>
			<td<?=$altRow?>><?=strtoupper($row['dep_nom'])?></td>
			<td<?=$altRow?>><?=strtoupper($status)?></td>
			<td<?=$altRow?>><a href="vehicule-shop-delete.php?id=<?=$row['id']?>" onclick="return confirm('Etes vous sûr?')"><img src="/img/delete.png" alt="" /></a></td>
		</tr>
		<?PHP
			if ($altRow == "") {
				$altRow = " class=\"altrow\"";
			}else{
				$altRow = "";
			}
		}
		?>
	</table>
	
	<p>&nbsp;</p>
</div><!-- end div content -->
</div><!-- end div container -->
<div id="footer"></div>
</body>
</html>
<?PHP
}
?>