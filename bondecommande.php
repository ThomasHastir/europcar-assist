<?PHP
session_name('SESSION1');
session_start();

include('inc/dictionnary.php');
include('inc/functions.php');

include('inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!--<link href='css/layout.css' rel='stylesheet' type='text/css'>-->
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/print.css" type="text/css" media="print"/>
<script type="text/javascript">
lgJS = '<?=$lgstring?>';
</script>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/additional-methods.min.js"></script>
<script type="text/javascript" src="js/localization/messages_<?=$lgstring?>.js"></script>
<script type="text/javascript" src="js/send.js"></script>
<script type="text/javascript">
jQuery(function($){
   $("#rental_start").mask("99/99/9999 99:99");
});


function endDate() {
	numberDays = parseInt($("#rental_days").val()); 
	var startDate = $('#rental_start').val().split(" ");
	
	//console.log(startDate);
	var startDate = startDate[0].split("/");
	var startDate = new Date(startDate[2],startDate[1]-1,startDate[0]);
	var endDate = new Date(startDate);
	endDate.setDate(endDate.getDate() + numberDays);
	var dd = endDate.getDate();
	var mm = endDate.getMonth() + 1;
	var y = endDate.getFullYear();
	$('#rental_end').text(dd+"/"+mm+"/"+y);
	}

 $(document).on("focusout","#rental_days, #rental_start",function(){
	endDate();  
    });
$( document ).ready(function() {
    endDate();
});

</script>
<style>
body { font-family:Arial; font-size:13px; }
h1, h2, h3 { font-family: 'Open Sans Condensed', sans-serif; }
h1 {
	margin:0;
	padding:0;
}
h2 {
	margin:0;
	padding:10px 0 5px 0;
}
h3 {
	margin:0;
	padding:10px 0 5px 0;
}
p {
	margin:0;
	padding:2px 0;
}
.label {
	display:inline;
	float:left;
	width:85px;
	}
input, textarea {
	font-family: Arial;
    font-size: 13px;
}
textarea {
	width:80%;
	height:100px;
}
input.error {
	border:1px solid #ff0000;
}
label.error {
	color:#ff0000;
	font-weight:bold;
}
.tohide {
 background: rgba(0, 0, 0, 0) url("../img/select_img.png") no-repeat scroll right center;
    color: #037b00;
    display: inline-block;
    font-size: 12px;
    margin-top: 5px;
    padding-right: 25px;
    text-decoration: none;
    }
   .toolbar {
	   position:fixed;bottom:0; width:100%; background:#fff; padding:10px; background-color: rgba(255, 255, 255, 0.8);
   }
</style>
</head>
<body style="padding-bottom:50px;">
<form>

<?php
$id = $_GET['id'];
$isAgency = 0;

if ($id) {
	

	
 $statement = $db->prepare("SELECT `bondecommande`.`bdc_datetime`,
`depanneur`.`dep_nom` AS `dep_nom`, `depanneur`.`dep_address` AS `dep_address`, `depanneur`.`dep_cp` AS `dep_cp`, `depanneur`.`dep_city` AS `dep_city`, `depanneur`.`dep_tel` AS `dep_tel`,
`assistance`.`dep_nom` AS `assist_nom`, `assistance`.`dep_img` AS `assist_img`, `assistance`.`dep_tel` AS `assist_tel`,
`assist_cars`.`car_dossier`, `assist_cars`.`car_acriss`, `assist_cars`.`car_immatriculation`,
`bondecommande`.`client_lastname`, `bondecommande`.`client_firstname`, `bondecommande`.`client_address`, `bondecommande`.`client_cp`, `bondecommande`.`client_city`, `bondecommande`.`client_tel`, `bondecommande`.`client_plate`,
`bondecommande`.`rental_days`, `bondecommande`.`rental_start`, `bondecommande`.`rental_remarks`, `bondecommande`.`rental_caution_binary`, `bondecommande`.`rental_caution`, `bondecommande`.`bdc_sent`
FROM `bondecommande`
LEFT JOIN `assist_cars` ON `assist_cars`.`bondecommande` = `bondecommande`.`bdc_id`
LEFT JOIN `assist_depaneurs` AS `depanneur` ON `depanneur`.`id` = `assist_cars`.`car_depaneur`
LEFT JOIN `assist_depaneurs` AS `assistance` ON `assistance`.`id` = `assist_cars`.`car_assistance`
WHERE `bondecommande`.`bdc_id` = :id;");


	
}else{
	
	$id = $_POST['agency_id'];
	$isAgency = 1;
	
	$statement = $db->prepare("SELECT `dep_nom`, `dep_address`, `dep_cp`, `dep_city`, `dep_tel` FROM `assist_depaneurs` WHERE `id` =  :id;");
	
}

$statement->execute(array('id' => $id));

$row = $statement->fetch(PDO::FETCH_ASSOC);
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td width="50%"><img src="/img/depanneurs/<?=$row['assist_img']?>.jpg"></td>
		<td width="50%">
			<h1><?=$dic_bdc_bondecommande?></h1>
			<?php if (!$isAgency) { ?><p><?=SQLDatetoToHuman($row['bdc_datetime'])?></p><?php }else{ echo date('d/m/Y H:i'); } ?>
			
			<h3><?=$dic_bdc_numerodedossier?></h3>
			<p><?php if (!$isAgency) { ?><?=$row['car_dossier']?><?php }else{ ?><?=$_POST['num_dossier']?><?php } ?></p>
			
		</td>
	</tr>
</table>

<?php if (!$isAgency) { ?>
<h2><?=$dic_bdc_depanneur?></h2>
<?php }else{ ?>
<h2><?=_("Agence de location")?></h2>
<?php	} ?>
<p><?=$row['dep_nom']?><br>
<?=$row['dep_address']?><br>
<?=$row['dep_cp']?> <?=$row['dep_city']?><br>
<?=$row['dep_tel']?></p>

<h2><?=$dic_bdc_assistance?></h2>

<?php
	if ($isAgency) {
		
		$statement = $db->prepare("SELECT `dep_nom`, `dep_tel` FROM `assist_depaneurs` WHERE `id` =  :id;");
		$statement->execute(array('id' => $_SESSION['myid']));
		$row2 = $statement->fetch(PDO::FETCH_ASSOC);
		?>
		<p><?=$row2['dep_nom']?><br>
		<?=$row2['dep_tel']?></p>
		<?php

	}else{
	?>
	<p><?=$row['assist_nom']?><br>
	<?=$row['assist_tel']?></p>
	<?php
	}
?>



<h3><?=$dic_bdc_client?></h3>

<p><label for="client_lastname" class="label"><?=$dic_bdc_nom?> : <span style="color:#ff0000;">*</span></label> <input type="text" name="client_lastname" id="client_lastname" value="<?=encrypt_decrypt('decrypt', $row['client_lastname'])?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>> <label for="client_firstname"><?=$dic_bdc_prenom?> : <span style="color:#ff0000;">*</span></label> <input type="text" name="client_firstname" id="client_firstname" value="<?=encrypt_decrypt('decrypt', $row['client_firstname'])?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>></p>
<p><label for="client_address" class="label"><?=$dic_bdc_adresse?> :</label> <input type="text" name="client_address" id="client_address" value="<?=encrypt_decrypt('decrypt', $row['client_address'])?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>></p>
<p><label for="client_cp" class="label"><?=$dic_bdc_codepostal?> :</label> <input type="text" maxlength="4" size="4" name="client_cp" id="client_cp" value="<?=$row['client_cp']?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>> <label for="dep_city"><?=$dic_bdc_ville?> :</label> <input type="text" name="client_city" id="client_city" value="<?=encrypt_decrypt('decrypt', $row['client_city'])?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>></p>
<p><label for="dep_tel" class="label"><?=$dic_bdc_telephone?> : <span style="color:#ff0000;">*</span></label> <input type="text" name="client_tel" id="client_tel" value="<?=encrypt_decrypt('decrypt', $row['client_tel'])?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>></p>
<p><label for="client_plate"><?=$dic_bdc_plaqueimmatriculation?> : <span style="color:#ff0000;">*</span></label> <input type="text" name="client_plate" id="client_plate" style="text-transform: uppercase" maxlength="10" value="<?=encrypt_decrypt('decrypt', $row['client_plate'])?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>></p>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td width="50%">
			<h3><?=$dic_bdc_categorievehicule?></h3>
			<?php if (!$isAgency) { ?><?=$row['car_acriss']?><?php }else{ ?><?=$_POST['accriss']?><?php } ?>
		</td>
		<td width="50%">
			<?php if (!$isAgency) { ?>
			<h3><?=$dic_bdc_plaqueimmatriculation?></h3>
			<?=$row['car_immatriculation']?>
			<?php } ?>
		</td>
	</tr>
</table>

<h3><?=$dic_bdc_nombredejours?></h3>
<input type="text" name="rental_days" id="rental_days" value="<?php if (!$isAgency) { ?><?=$row['rental_days']?><?php }else{ ?><?=$_POST['num_jours']?><?php } ?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>>

<h3><?=$dic_bdc_datedebut?></h3>
<input type="text" name="rental_start" id="rental_start" value="<?php if (!$isAgency) { ?><?=SQLDatetoToHuman($row['rental_start'])?><?php }else{ ?><?=date('d/m/Y H:i');?><?php } ?>"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>>

<h3><?=$dic_bdc_datefin?></h3>
<p id="rental_end"></p>

<h3><?=$dic_bdc_remarques?></h3>
<textarea name="rental_remarks" id="rental_remarks"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>><?=$row['rental_remarks']?></textarea>

<?php 
if (!$isAgency) {
?>
<h3><?=$dic_bdc_caution?></h3>
<input type="radio" name="caution" id="caution_1"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>> <label for="caution_1">Specified</label> / <input type="radio" name="caution" id="caution_2"<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>> <label for="caution_2">full</label>

<p><?=$dic_bdc_sommecaution?></p>
<p><input type="text" value=""<?php if($row['bdc_sent'] == 1) { ?> disabled="disabled"<?php } ?>></p>
<?php } ?>

<div class="toolbar tohideprint">
	
	<?php 
		if (!$isAgency) {
	?>
	
	<?php if($row['bdc_sent'] != 1) { ?>
	<a href="javascript:bondecommande('<?=$id?>','send','');" class="tohide"><?=$dic_bdc_cloturer?></a>
	<br><a href="javascript:bondecommande('<?=$id?>','back','<?=$_SERVER['HTTP_REFERER']?>');" class="tohide"><?=$dic_bdc_retour?></a>
	<?php }else{ ?>
	<a href="javascript:window.print();" class="tohide"><?=$co_imprimer?></a>
	<br><a href="javascript:history.back();" class="tohide"><?=$dic_bdc_retour?></a>
	<?php } ?>
	
	<?php
		}else{
	?>
	
	<input type="hidden" name="num_dossier" id="num_dossier" value="<?=$_POST['num_dossier']?>">
	<input type="hidden" name="accriss" id="accriss" value="<?=$_POST['accriss']?>">
	<input type="hidden" name="agency_id" id="agency_id" value="<?=$_POST['agency_id']?>">
	<input type="hidden" name="assist_id" id="assist_id" value="<?=$_SESSION['myid']?>">
	
	<a href="javascript:bondecommandeAgency();" class="tohide"><?=_('Clôturer et envoyer à l\'agence')?></a>
	<br><a href="javascript:history.back();" class="tohide"><?=$dic_bdc_retour?></a>
	<?php } ?>
	


</div>
<p id="loadingMsg"></p>
</form>
<?php $statement = null; $db = null; ?>
</body>
</html>