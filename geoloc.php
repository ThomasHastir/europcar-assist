<?PHP
session_name('SESSION1');
session_start();

include('inc/dictionnary.php');

include('inc/connexion-pdo.php');

try {
	$db = new PDO('mysql:host='.$servername.';dbname='.$dbname.';charset=utf8', $username, $password);
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

$address = urlencode($_POST['in_address']);
$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&region=be&sensor=false&key=AIzaSyAX5gsnqeQ2O803gs62dj3Do926UUocVo4");

//echo $json;

// filtre pour détecter les depanneurs ayant encore du stock
$filtre = $_POST['filtre'];


$decoded = json_decode($json);

//print_r($decoded);


$lat =  str_replace(",",".", $decoded->results[0]->geometry->location->lat);
$long = str_replace(",",".", $decoded->results[0]->geometry->location->lng);

//echo '$long: '.$long;

if ($_SESSION['zeType'] == 'admin' || $_SESSION['myid'] == 40) {
	// on inclus les véhicules EA si on est ADMIN ou EA
	$sqlEA = "";
}else{
	// on inclus pas les véhicules EA
	$sqlEA = ") AND assist_cars.car_immatriculation NOT IN (SELECT ea_immatriculation FROM assist_ea";
}
if ($_SESSION['zeType'] == 'assistant') {
	// on vérifie les heures d'ouverture.
	$sqlstring = " AND dep_".strtolower(date('D'))."_".date('a')."_start < CURTIME() AND dep_".strtolower(date('D'))."_".date('a')."_end > CURTIME() AND assist_depaneurs.id NOT IN (SELECT horaire_dep FROM assist_horaires WHERE horaire_jour = CURDATE()) AND (dep_relassistance LIKE '".$_SESSION['myid'].",%' or dep_relassistance LIKE '%,".$_SESSION['myid']."' or dep_relassistance LIKE '%,".$_SESSION['myid'].",%' or dep_relassistance  = '".$_SESSION['myid']."')";
}else{
	$sqlstring = "";
}

if ($filtre == 0 ) {
	

	$sql = "SELECT DISTINCT(assist_depaneurs.id), assist_depaneurs.dep_nom, assist_depaneurs.dep_tel, assist_depaneurs.dep_cp, assist_depaneurs.dep_city, ( 6371 * acos( cos( radians($lat) ) * cos( radians( assist_depaneurs.dep_lat ) ) * cos( radians( assist_depaneurs.dep_long ) - radians($long) ) + sin( radians($lat) ) * sin( radians( assist_depaneurs.dep_lat ) ) ) )
AS distance, ";
	$sql .= "assist_depaneurs.dep_assist, assist_depaneurs.dep_mon_am_start, assist_depaneurs.dep_mon_am_end, assist_depaneurs.dep_mon_pm_start, assist_depaneurs.dep_mon_pm_end, assist_depaneurs.dep_tue_am_start, assist_depaneurs.dep_tue_am_end, assist_depaneurs.dep_tue_pm_start, assist_depaneurs.dep_tue_pm_end, assist_depaneurs.dep_wed_am_start, assist_depaneurs.dep_wed_am_end, assist_depaneurs.dep_wed_pm_start, assist_depaneurs.dep_wed_pm_end, assist_depaneurs.dep_thu_am_start, assist_depaneurs.dep_thu_am_end, assist_depaneurs.dep_thu_pm_start, assist_depaneurs.dep_thu_pm_end, assist_depaneurs.dep_fri_am_start, assist_depaneurs.dep_fri_am_end, assist_depaneurs.dep_fri_pm_start, assist_depaneurs.dep_fri_pm_end, assist_depaneurs.dep_sat_am_start, assist_depaneurs.dep_sat_am_end, assist_depaneurs.dep_sat_pm_start, assist_depaneurs.dep_sat_pm_end, assist_depaneurs.dep_sun_am_start, assist_depaneurs.dep_sun_am_end, assist_depaneurs.dep_sun_pm_start, assist_depaneurs.dep_sun_pm_end, ";
	$sql .= "COUNT(CASE WHEN ((assist_cars.car_status = 0 OR (ADDTIME(assist_cars.car_date, '4:00:00') < NOW() AND assist_cars.car_status = 1) OR assist_cars.car_status = 3 OR assist_cars.car_status = 4".$sqlEA.") AND REPLACE(assist_cars.car_immatriculation, '-', '') IN (SELECT registration_number FROM vehicules_ec)) AND (assist_cars.contrat = 0 OR assist_cars.contrat IS NULL) THEN 1 ELSE NULL END) AS vehicules, ";
	$sql .= "COUNT(CASE WHEN ((assist_cars.car_status = 0 OR (ADDTIME(assist_cars.car_date, '4:00:00') < NOW() AND assist_cars.car_status = 1) OR assist_cars.car_status = 3 OR assist_cars.car_status = 4 OR (assist_cars.car_status = 1 AND assist_cars.car_assistance = ".$_SESSION['myid'].")".$sqlEA.") AND REPLACE(assist_cars.car_immatriculation, '-', '') IN (SELECT registration_number FROM vehicules_ec)) THEN 1 ELSE NULL END) AS vehiculesCount ";
	// une assistance doit pouvoir aller sur la fiche du dépanneur où elle a bloqué un véhicule même si plus de véhicule dispo chez le depanneur.
	$sql .= "FROM assist_depaneurs LEFT JOIN assist_cars ON assist_cars.car_depaneur = assist_depaneurs.id ";
	$sql .= "WHERE (assist_depaneurs.dep_assist = 0 OR assist_depaneurs.dep_assist = 3) AND (`assist_depaneurs`.`active` = 1)";
	$sql .= $sqlstring;
	$sql .= "GROUP BY assist_depaneurs.dep_nom HAVING distance < 1000 ORDER BY distance LIMIT 0 , 10;";
	



}else{
	
	
	$sql = "SELECT DISTINCT(assist_depaneurs.id), assist_depaneurs.dep_nom, assist_depaneurs.dep_tel, assist_depaneurs.dep_cp, assist_depaneurs.dep_city, ( 6371 * acos( cos( radians($lat) ) * cos( radians( assist_depaneurs.dep_lat ) ) * cos( radians( assist_depaneurs.dep_long ) - radians($long) ) + sin( radians($lat) ) * sin( radians( assist_depaneurs.dep_lat ) ) ) )
AS distance, ";
	$sql .= "COUNT(CASE WHEN ((assist_cars.car_status = 0 OR (ADDTIME(assist_cars.car_date, '4:00:00') < NOW() AND assist_cars.car_status = 1) OR assist_cars.car_status = 3 OR assist_cars.car_status = 4".$sqlEA.") AND REPLACE(assist_cars.car_immatriculation, '-', '') IN (SELECT registration_number FROM vehicules_ec)) AND (assist_cars.contrat = 0 OR assist_cars.contrat IS NULL) THEN 1 ELSE NULL END) AS vehicules, ";
	$sql .= "COUNT(CASE WHEN ((assist_cars.car_status = 0 OR (ADDTIME(assist_cars.car_date, '4:00:00') < NOW() AND assist_cars.car_status = 1) OR assist_cars.car_status = 3 OR assist_cars.car_status = 4 OR (assist_cars.car_status = 1 AND assist_cars.car_assistance = ".$_SESSION['myid'].")".$sqlEA.") AND REPLACE(assist_cars.car_immatriculation, '-', '') IN (SELECT registration_number FROM vehicules_ec)) THEN 1 ELSE NULL END) AS vehiculesCount, ";
	 // une assistance doit pouvoir aller sur la fiche du dépanneur où elle a bloqué un véhicule même si plus de véhicule dispo chez le depanneur.
	$sql .= "assist_depaneurs.dep_assist, assist_depaneurs.dep_mon_am_start, assist_depaneurs.dep_mon_am_end, assist_depaneurs.dep_mon_pm_start, assist_depaneurs.dep_mon_pm_end, assist_depaneurs.dep_tue_am_start, assist_depaneurs.dep_tue_am_end, assist_depaneurs.dep_tue_pm_start, assist_depaneurs.dep_tue_pm_end, assist_depaneurs.dep_wed_am_start, assist_depaneurs.dep_wed_am_end, assist_depaneurs.dep_wed_pm_start, assist_depaneurs.dep_wed_pm_end, assist_depaneurs.dep_thu_am_start, assist_depaneurs.dep_thu_am_end, assist_depaneurs.dep_thu_pm_start, assist_depaneurs.dep_thu_pm_end, assist_depaneurs.dep_fri_am_start, assist_depaneurs.dep_fri_am_end, assist_depaneurs.dep_fri_pm_start, assist_depaneurs.dep_fri_pm_end, assist_depaneurs.dep_sat_am_start, assist_depaneurs.dep_sat_am_end, assist_depaneurs.dep_sat_pm_start, assist_depaneurs.dep_sat_pm_end, assist_depaneurs.dep_sun_am_start, assist_depaneurs.dep_sun_am_end, assist_depaneurs.dep_sun_pm_start, assist_depaneurs.dep_sun_pm_end ";
	$sql .= "FROM assist_depaneurs LEFT JOIN assist_cars ON assist_cars.car_depaneur = assist_depaneurs.id ";
	$sql .= "WHERE (assist_cars.car_status = 0 OR (ADDTIME(assist_cars.car_date, '4:00:00') < NOW() AND assist_cars.car_status = 1) OR assist_cars.car_status = 3 OR assist_cars.car_status = 4 OR (assist_cars.car_status = 1 AND assist_cars.car_assistance = ".$_SESSION['myid'].") AND assist_depaneurs.dep_assist = 0".$sqlEA.") ";
	$sql .= $sqlstring;
	$sql .= " OR (assist_depaneurs.dep_assist = 3 AND `assist_depaneurs`.`active` = 1) ";
	$sql .= "GROUP BY assist_depaneurs.dep_nom HAVING distance < 1000 ORDER BY distance LIMIT 0 , 10;";

//echo $sql;

	//envoyer une alert DB pour signaler que le depanneur selectionné est vide
	echo 'alert("', $dic_plusdevehiculealert, '");';
	echo 'document.getElementById(\'ajaxDep\').innerHTML = "', $dic_plusdevehicule, '";';

	$sql_refus = "INSERT INTO assist_refus (refus_assistance, refus_depaneur, refus_date)";
	$sql_refus .= "VALUES (".$_SESSION['myid'].", ".$filtre.", NOW())";
	$statement = $db->prepare($sql_refus);
	$statement->execute();
}



$statement = $db->prepare($sql);
$statement->execute();

echo 'document.getElementById(\'resultats\').innerHTML = "<dl>';

foreach($statement as $row) {
	
	$dep_assist = $row['dep_assist'];
	$dep_tel = $row['dep_tel'];
	$dep_mon_am_start = $row['dep_mon_am_start'];
	$dep_mon_am_end = $row['dep_mon_am_end'];
	$dep_mon_pm_start = $row['dep_mon_pm_start'];
	$dep_mon_pm_end = $row['dep_mon_pm_end'];
	$dep_tue_am_start = $row['dep_tue_am_start'];
	$dep_tue_am_end = $row['dep_tue_am_end'];
	$dep_tue_pm_start = $row['dep_tue_pm_start'];
	$dep_tue_pm_end = $row['dep_tue_pm_end'];
	$dep_wed_am_start = $row['dep_wed_am_start'];
	$dep_wed_am_end = $row['dep_wed_am_end'];
	$dep_wed_pm_start = $row['dep_wed_pm_start'];
	$dep_wed_pm_end = $row['dep_wed_pm_end'];
	$dep_thu_am_start = $row['dep_thu_am_start'];
	$dep_thu_am_end = $row['dep_thu_am_end'];
	$dep_thu_pm_start = $row['dep_thu_pm_start'];
	$dep_thu_pm_end = $row['dep_thu_pm_end'];
	$dep_fri_am_start = $row['dep_fri_am_start'];
	$dep_fri_am_end = $row['dep_fri_am_end'];
	$dep_fri_pm_start = $row['dep_fri_pm_start'];
	$dep_fri_pm_end = $row['dep_fri_pm_end'];
	$dep_sat_am_start = $row['dep_sat_am_start'];
	$dep_sat_am_end = $row['dep_sat_am_end'];
	$dep_sat_pm_start = $row['dep_sat_pm_start'];
	$dep_sat_pm_end = $row['dep_sat_pm_end'];
	$dep_sun_am_start = $row['dep_sun_am_start'];
	$dep_sun_am_end = $row['dep_sun_am_end'];
	$dep_sun_pm_start = $row['dep_sun_pm_start'];
	$dep_sun_pm_end = $row['dep_sun_pm_end'];
	
	// 04/02/2016 : Détecter si le dépanneur est ouvert ou non. S'il est fermé, une assistance ne peut plus bloquer un véhicule via la plateforme, mais est obligé de contacter le dépanneur par téléphone
	$HourStartToday = strtotime(${"dep_" . strtolower(date('D')) . "_" . strtolower(date('a')) . "_start"});
	$HourEndToday = strtotime(${"dep_" . strtolower(date('D')) . "_" . strtolower(date('a')) . "_end"});
	$HourNowToday = strtotime(date('H:i:s'));
	$depIsOpen = true;
	if ($HourNowToday < $HourStartToday || $HourNowToday > $HourEndToday) {
		$depIsOpen = false;
	}

	if ($dep_assist == 3) {
		$iconAgence = '<img src=\"img/icon-agence.png\">';
	}else{
		$iconAgence = '';
	}
	
	echo '<dt><span>', $row['dep_nom'], '</span> ', round($row['distance'], 1), ' km ', $iconAgence, '</dt><dd>', $row['dep_cp'], ' ', $row['dep_city'], '<br />';
  
  
  
  if ($dep_assist == 3) {
	  if ($depIsOpen == false && $_SESSION['zeType'] == 'assistant'){
		  echo '<br><strong>'.$dic_agency_closed.'</strong><br>';
	  }else{
	  	//echo '<a href=\"https://www.resacar.com\" target=\"_blank\">'.$dic_agency_book.'</a>';
	  	echo '<a href=\"depanneur_details.php?id=', $row['id'], '\">'.$dic_agency_book.'</a>';
	  }
  }else{
	  if ($depIsOpen == true || $_SESSION['zeType'] != 'assistant') {
	  		
			  echo '<br />'.$row['vehicules'].' '.$dic_vehiculesdispos.'<br />';  

		  
		  
		  }
	  
	  if ($depIsOpen == false && $_SESSION['zeType'] == 'assistant'){
		  echo '<br>'.str_replace('$tel$', $dep_tel, $dic_dep_closed_2).'<br>';
	  }
	  
	
	 
	  if ($row['vehiculesCount'] > 0 || $_SESSION['zeType'] == 'admin') {
	  	echo '<a href=\"depanneur_details.php?id=', $row['id'], '\">', $dic_selectionnerdepanneur, '</a>';
	  }else{
	  	echo '<a href=\"javascript:get(document.getElementById(\'address\'), ', $row['id'], ');\">', $dic_selectionnerdepanneur, '</a>';
	  }
  }
  
  
  echo '</dd>';
}
echo '</dl>"';


//attention ligne précédente modifiée depuis echo '</dl>";';
$statement = null; $db = null;
?>