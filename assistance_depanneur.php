<?PHP
if ($_SESSION['connected'] == 1) {
?>
	<?PHP
	//echo $_SESSION['zeType'];
	if ($_SESSION['zeType'] == 'admin' || $_SESSION['zeType'] == 'depanneur') {
		$sqlstring = " assist_cars.car_status != 5 AND assist_cars.car_status != 6 AND";
		
		
		
		?>
		<script type="text/javascript">
		jQuery(function($){
		   $("#add_immatriculation").mask("9-aaa-999");
		});
		</script>
		<?PHP
	}
	
	if ($_SESSION['zeType'] == 'depanneur') {
		$depaneur = $_SESSION['myid'];
	}elseif ($_SESSION['zeType'] == 'admin') {
		$depaneur = $_GET['id'];
	}else{
		$depaneur = $_GET['id'];
		// une société d'assistance ne peut voir que les véhicules en stock et uniquement les véhicules qu'elle a bloqué. Elle ne peut pas voir les véhicules bloqués par les autres.
		$sqlstring = " (assist_cars.car_status = 0 OR (assist_cars.car_status = 1 AND assist_cars.car_assistance = ".$_SESSION['myid'].") OR assist_cars.car_status = 3 OR assist_cars.car_status = 4) AND";
		
		// Si tu n'est pas Europ Assistance (ID=40), alors tu ne peux pas voir les véhicules Europ Assistance
		if ($_SESSION['myid'] != 40) {
			$sqlstring .= " assist_cars.car_immatriculation NOT IN (SELECT ea_immatriculation FROM assist_ea) AND";
		}
		
	}
	
	
	
	// on récupère les infos sur le dépanneur (adresse, etc...)
	include('inc/connexion.php');
	
	mysql_query("SET SQL_BIG_SELECTS=1");
	
	$sql = "SELECT dep_nom, dep_address, dep_cp, dep_city, dep_tel, dep_fax, dep_mail, dep_assist, dep_lat, dep_long, dep_mon_am_start, dep_mon_am_end, dep_mon_pm_start, dep_mon_pm_end, dep_tue_am_start, dep_tue_am_end, dep_tue_pm_start, dep_tue_pm_end, dep_wed_am_start, dep_wed_am_end, dep_wed_pm_start, dep_wed_pm_end, dep_thu_am_start, dep_thu_am_end, dep_thu_pm_start, dep_thu_pm_end, dep_fri_am_start, dep_fri_am_end, dep_fri_pm_start, dep_fri_pm_end, dep_sat_am_start, dep_sat_am_end, dep_sat_pm_start, dep_sat_pm_end, dep_sun_am_start, dep_sun_am_end, dep_sun_pm_start, dep_sun_pm_end, dep_relassistance FROM assist_depaneurs WHERE assist_depaneurs.id = ".$depaneur.";";
	$result = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	mysql_close($link);
	
	if ($row = mysql_fetch_array($result)) {
		$strDep_name = $row['dep_nom'];
		$strDep_address = $row['dep_address'];
		$dep_assist = $row['dep_assist'];
		$strDep_cp = $row['dep_cp'];
		$strDep_city = $row['dep_city'];
		$strDep_tel = $row['dep_tel'];
		$strDep_fax = $row['dep_fax'];
		$strDep_mail = $row['dep_mail'];
		$strDep_lat = $row['dep_lat'];
		$strDep_long = $row['dep_long'];
		$dep_mon_am_start = $row['dep_mon_am_start'];
		$dep_mon_am_end = $row['dep_mon_am_end'];
		$dep_mon_pm_start = $row['dep_mon_pm_start'];
		$dep_mon_pm_end = $row['dep_mon_pm_end'];
		$dep_tue_am_start = $row['dep_tue_am_start'];
		$dep_tue_am_end = $row['dep_tue_am_end'];
		$dep_tue_pm_start = $row['dep_tue_pm_start'];
		$dep_tue_pm_end = $row['dep_tue_pm_end'];
		$dep_wed_am_start = $row['dep_wed_am_start'];
		$dep_wed_am_end = $row['dep_wed_am_end'];
		$dep_wed_pm_start = $row['dep_wed_pm_start'];
		$dep_wed_pm_end = $row['dep_wed_pm_end'];
		$dep_thu_am_start = $row['dep_thu_am_start'];
		$dep_thu_am_end = $row['dep_thu_am_end'];
		$dep_thu_pm_start = $row['dep_thu_pm_start'];
		$dep_thu_pm_end = $row['dep_thu_pm_end'];
		$dep_fri_am_start = $row['dep_fri_am_start'];
		$dep_fri_am_end = $row['dep_fri_am_end'];
		$dep_fri_pm_start = $row['dep_fri_pm_start'];
		$dep_fri_pm_end = $row['dep_fri_pm_end'];
		$dep_sat_am_start = $row['dep_sat_am_start'];
		$dep_sat_am_end = $row['dep_sat_am_end'];
		$dep_sat_pm_start = $row['dep_sat_pm_start'];
		$dep_sat_pm_end = $row['dep_sat_pm_end'];
		$dep_sun_am_start = $row['dep_sun_am_start'];
		$dep_sun_am_end = $row['dep_sun_am_end'];
		$dep_sun_pm_start = $row['dep_sun_pm_start'];
		$dep_sun_pm_end = $row['dep_sun_pm_end'];
		$dep_relassistance = $row['dep_relassistance'];
		

		include('inc/connexion.php');
		$sql2 = "SELECT * FROM assist_depaneurs WHERE id IN ($dep_relassistance);";
		$result2 = mysql_query($sql2) 
			or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
		mysql_close($link);
		
		$selectAssistances = '';
		
		while ($row2 = mysql_fetch_array($result2)) {
			$assist_id = $row2['id'];
			$assist_nom = $row2['dep_nom'];
			
			$selectAssistances .= '<option value="'.$assist_id.'">'.$assist_nom.'</option>';
			}
	}

	// 04/02/2016 : Détecter si le dépanneur est ouvert ou non. S'il est fermé, une assistance ne peut plus bloquer un véhicule via la plateforme, mais est obligé de contacter le dépanneur par téléphone
	$HourStartToday = strtotime(${"dep_" . strtolower(date('D')) . "_" . strtolower(date('a')) . "_start"});
	$HourEndToday = strtotime(${"dep_" . strtolower(date('D')) . "_" . strtolower(date('a')) . "_end"});
	$HourNowToday = strtotime(date('H:i:s'));
	$depIsOpen = true;
	if ($HourNowToday < $HourStartToday || $HourNowToday > $HourEndToday) {
		$depIsOpen = false;
	}
	
	// on récupère la liste des véhicules chez le dépanneur
	include('inc/connexion.php');
	

	// Requête pour lister tous les véhicules EA et non EA:
	 $sql = "SELECT TB1.dep_nom AS dep_nom, TB1.dep_city, assist_cars.car_immatriculation, assist_cars.car_date_in, assist_cars.car_date_out, assist_cars.car_acriss, assist_cars.car_marque, assist_cars.car_model, assist_cars.car_date_BB, assist_cars.car_status_BB, assist_cars.id, assist_cars.car_status, assist_cars.car_date, assist_cars.car_block_time, assist_cars.car_dossier, assist_cars.car_assistance,assist_cars.contrat,assist_cars.bondecommande, TB2.dep_nom AS assist_nom, CASE WHEN TB3.id IS NOT NULL THEN 1 ELSE 0 END AS EA, TB4.`bdc_sent` AS `bdc_sent` FROM ((((assist_depaneurs AS TB1 LEFT JOIN assist_cars ON assist_cars.car_depaneur = TB1.id) LEFT JOIN assist_depaneurs AS TB2 ON assist_cars.car_assistance = TB2.id) LEFT JOIN assist_ea AS TB3 ON TB3.ea_immatriculation = assist_cars.car_immatriculation) LEFT JOIN bondecommande AS TB4 ON TB4.`bdc_id` = assist_cars.`bondecommande`) WHERE".$sqlstring." TB1.id = ".$depaneur." AND REPLACE(assist_cars.car_immatriculation, '-', '') IN (SELECT registration_number FROM vehicules_ec) ORDER BY assist_cars.car_date_in ASC;";
	
	$result2 = mysql_query($sql) 
		or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	mysql_close($link);
	?>
	<div id="leftcol">
	<h1><?=$dic_depanneur?> <?=$strDep_name?></h1>
	
	<?php
		// Si le dépanneur est fermé alors on empêche l'assistance de bloquer un véhicule autrement que par téléphone
		if ($depIsOpen == false && $_SESSION['zeType'] == 'assistant'){
			echo '<p>'.str_replace('$tel$', $strDep_tel, $dic_dep_closed_2).'</p>';
		}else{

		?>
	
	<p><?=str_replace('$repl_city$', $strDep_city, str_replace('$repl_depanneur$', $strDep_name, $dic_decouvrezliste))?></p>
	<dl>
	<?PHP
	$countBlockedCars = 0;
		
	while ($row = mysql_fetch_array($result2)) {


	

		if ($_SESSION['zeType'] == 'assistant') 
		{
			
			switch ($row['car_acriss']) {
			    case 'ECMV':
			        $model = ' - Renault Clio'.$dic_ouequivalent;
			        break;
			    case 'CCMV':
			        $model = ' - Opel Astra'.$dic_ouequivalent;
			        break;
			}
			
			
		}
		else
		{
			$model = ' - '.$row['car_marque'].' '.$row['car_model'];
		}
		
	$addClass = '';
	if($row['car_status'] == 2) {$addClass=" class='blocked'";$countBlockedCars++;}
	
	if ($countBlockedCars == 1) {
		echo '<span class="sepblock">'._('Voiture(s) en cours d\'utilisation:').'</span>';
	}
	
	  echo '<dt'.$addClass.'><span>', $row['car_immatriculation'], '</span> ', $row['car_acriss'], $model , '</dt><dd'.$addClass.'><span id="lien', $row['id'], '">';
	  
	  if ($row['car_status'] == 0 || $row['car_status'] == 3 || $row['car_status'] == 4 || $row['car_status'] == 2) {
  			if ($row['contrat'] == 0)
  			{
	  			
	  			
	  			
	  			// on récupère les BA pour l'assistance
	  			
	  			if ($_SESSION['zeType'] == 'assistant') {
	  			
	  				$baselectstring = "<br><br />".$dic_selectionnezunba."<br>";
	  			
		  			include('inc/connexion.php');
		  			
		  			$sql = "SELECT `id`, `dep_gw_name`, `dep_ba_id` FROM `assist_depaneurs_conditions` WHERE `assist_depaneurs_conditions`.`dep_id` = ".$_SESSION['myid']." ORDER BY `dep_gw_name` ASC";
		  			$result = mysql_query($sql) 
						or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
					mysql_close($link);
					
					
					$baselectstring .= '<select name="ba_'.$row['id'].'" id="ba_'.$row['id'].'">';
					$baselectstring .= '<option></option>';
					while ($rowBA = mysql_fetch_array($result)) {
						$baselectstring .= '<option value="'.$rowBA['id'].'">'.$rowBA['dep_gw_name'].' ('.$rowBA['dep_ba_id'].')</option>';
						}
					$baselectstring .= '</select>';
					
					$baselectstring .= '<input type="hidden" name="assistance_'.$row['id'].'" id="assistance_'.$row['id'].'" value="'.$_SESSION['myid'].'">';
					
					
					
				}else{
					
					
					// Si c'est un dépanneur on doit lui laisser aussi la possibilité de charger un BA mais avant il doit sélectionner un dépanneur.
					
					$baselectstring = '<br><br>Sélectionner une assistance:';
					$baselectstring .= '<br><select name="assistance_'.$row['id'].'" id="assistance_'.$row['id'].'" onchange="loadBA('.$row['id'].')">';
					$baselectstring .= '<option></option>';
					$baselectstring .= $selectAssistances;
					$baselectstring .= '</select>';
					
					$baselectstring .= '<br><br>'.$dic_selectionnezunba.'<br><select name="ba_'.$row['id'].'" id="ba_'.$row['id'].'">';
		
					$baselectstring .= '<option></option>';
						
					$baselectstring .= '</select>';
				}
	  			
	  			
	  			
  				echo '<a href="javascript:showDossier(\'dossier', $row['id'], '\');">', $dic_bloquervehicule, '</a><br /><br />';
  				
  				if ($_SESSION['zeType'] != 'assistant' && $row['car_status'] != 2) {
	  				
	  				$car_date_in = strtotime($row['car_date_in']);
	  				
	  				$datediff = time() - $car_date_in;
	  				$diffdate = floor($datediff / (60 * 60 * 24));
	  				if ($diffdate > 0) {
  						echo sprintf(_('Ce véhicule n\'est plus en mouvement depuis %s jour(s)'), $diffdate).'<br><br>';
  					}
  				}
  				
  				echo '<span style="display:none;" id="dossier', $row['id'], '">', $dic_veuillezprecisernumero, '<br><input type="text" name="num_dossier', $row['id'], '" id="num_dossier', $row['id'], '" value="" />'.$baselectstring.'<br><br />', $dic_veuillezpreciserjours, '<br><input type="text" name="num_jours', $row['id'], '" id="num_jours', $row['id'], '" value="" /><br><br><input type="button" value="', $dic_valider, '" onclick="blok(\'', $row['id'], '\', document.getElementById(\'num_dossier', $row['id'], '\').value, document.getElementById(\'num_jours', $row['id'], '\').value, \'block\', document.getElementById(\'ba_', $row['id'], '\').value, document.getElementById(\'assistance_', $row['id'], '\').value);" /></br></span>';
  				
  				
  				
  			}
  			
			if (($_SESSION['zeType'] == 'depanneur' && $row['contrat'] > 0 OR $_SESSION['zeType'] == 'admin' && $row['contrat'] > 0))
			{
				// Le véhicule est en cours d'utilisation
				if ($row['car_date_out']) {
					
					include('inc/connexion.php');
					$sql = "SELECT date_start, date_fin FROM contrats WHERE id = ".$row['contrat'].";";
					$result = mysql_query($sql) 
					or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
					if ($contrat = mysql_fetch_array($result))
					{
					
		
					 	if ($contrat['date_fin']) {
								if (strtotime($contrat['date_fin']) > time()) {
								
									echo '<p>'.sprintf(_("Véhicule en cours de location du %s au %s"), SQLDatetoToHuman($contrat['date_start']), SQLDatetoToHuman($contrat['date_fin'])).'</p>';
								}else{
									echo '<p>'.sprintf(_("Véhicule en cours de location du %s au <span style='color:#ff0000; font-weight:bold;'>%s</span>"), SQLDatetoToHuman($contrat['date_start']), SQLDatetoToHuman($contrat['date_fin'])).'</p>';
								}
							}else{
								echo '<p>'._("Véhicule en cours de location").'</p>';
							}
							}
					
					
					echo '<a href="contrat.php?contratid='.$row['contrat'].'&amp;retour=1" style="color:red;"><strong>', _('Encoder les données au retour du véhicule'), '</strong></a><br>';
				}else{
					// Le contrat doit encore être finalisé
					echo '<a href="contrat.php?contratid='.$row['contrat'].'">', _('Finaliser le contrat'), '</a><br>';
				}
				
				//echo '<a target="" href="javascript:cloture(', $row['id'], ');">', $dic_cloturer, '</a><br>';
			
			}

			if ($row['car_status_BB'] == 1) 
			{	  
				echo $dic_bloqued.$row['car_date_BB']."<br>";		  		  
			}
		 
		 }else{
			
			
			
			$date = $row['car_date'];
			$block_time = $row['car_block_time'];
			$block_time = explode(':', $block_time);
			
			$timestamp = strtotime(date("Y-m-d H:i:s", strtotime($date)) . " + ".$block_time[0]." hours");
			$expired = date('d/m/Y G:i', $timestamp);
			
			if (isset($row['assist_nom'])) {
				$quiabloque = $row['assist_nom'];
			}else{
				$quiabloque = $dic_bloquejusquau3;
			}
			if ($row['car_status_BB'] == 1) 
			{	  
				echo $dic_bloqued.$row['car_date_BB']."<br>";		  		  
			}
			echo $dic_bloquejusquau1, ' ', $expired, ' ', $dic_bloquejusquau2, ' ', $quiabloque, ' (ref: ', $row['car_dossier'], ')<br />';
			if ($_SESSION['zeType'] != 'assistant') {
				echo '<a href="javascript:blok(\'', $row['id'], '\', \'', $row['car_dossier'], '\', \'0\', \'cancel\', \'\');">', $dic_debloquer, '</a><br />';
			}
			
			// Lien vers le bon de commande éditable par assitance et consultable par dépanneur.
  			if ($row['bondecommande']) 
			{
				if ($row['bdc_sent'] == 0 && $_SESSION['zeType'] != 'depanneur') {
					echo '<a href="bondecommande.php?id='.$row['bondecommande'].'">'.$dic_completer_bdc.'</a><br>';
				}else if ($row['bdc_sent'] == 1) {
					echo '<a href="bondecommande.php?id='.$row['bondecommande'].'">'.$dic_consulter_bdc.'</a><br>';
				}
			}
			
			if ($_SESSION['zeType'] == 'depanneur' && $row['contrat'] > 0 OR $_SESSION['zeType'] == 'admin' && $row['contrat'] > 0)
			{
				echo '<strong><a style="color:red;" href="contrat.php?contratid='.$row['contrat'].'">'._('Editer le contrat').'</a></strong>';
				echo '<br><strong style="color:red;"> '._('Attention: vous devez encore finaliser le contrat et le valider avant de donner le véhicule au client').'</strong>';
				
				echo '<br>';
			}

			

	
			
		  
		}
		
		if (($_SESSION['zeType'] == 'admin' || $_SESSION['zeType'] == 'depanneur') && $row['car_status'] != 9 && $row['car_status'] != 2) {

		  	//echo '<br /><a href="javascript:deleteCar(', $row['id'], ', 5);">', $dic_blocbb, '</a><br />';
		  	if ($row['car_status_BB'] == 0) 
			{	  
				
				
				echo '<a href="javascript:showBuyback(\'buyback', $row['id'], '\');">', $dic_blocbb, '</a><br /><span style="display:none;" id="buyback', $row['id'], '">', $dic_veuillezpreciserdate, ' <input type="text" name="BB_date', $row['id'], '" id="BB_date', $row['id'], '" class="validateDate" value="" /> <input type="button" value="', $dic_valider, '" onclick="buyBack(document.getElementById(\'BB_date', $row['id'], '\').value, '.$row['id'].');" /></br></span>';		  		  
			}
		  	echo '<a href="javascript:deleteCar(', $row['id'], ', 6);">', $dic_blocshop, '</a><br />';
			echo '<a href="javascript:deleteCar(', $row['id'], ', 9);">', $dic_supprimervehicule,'</a><br />'; // On efface de la DB
	
		  }
		  
		  

		if($row['EA'] == 1) {
			echo '<br />', $dic_retourmemeendroit, '<br /><img src="img/ea.png" alt="" /><br />';
		}
		
	   echo '</span></dd>';
	}
	
	/*
		12/04/2017: commenté car je constate que ça ne sert à rien??
	include('inc/connexion.php');
	$sql2 = "SELECT * FROM contrats WHERE car_id = ".$row['id'].";";
	$result2 = mysql_query($sql) 
	or die('ERREUR: La requête n\'est pas valide:'.mysql_error());
	if ($contrat = mysql_fetch_array($result2))
	{
		$car_id = $contrat['car_id'];
		$contrat_id = $contrat['id'];
	}
	*/
	
	// Si Agence EC, on liste 10 voitures bidons			     
	if ($dep_assist == 3) {		     
		for ($i = 1; $i <= 10; $i++) {
			if ($i > 5) {
				echo '<dt><span>ECMV</span> - Renault Clio'.$dic_ouequivalent.'</dt>';
				echo '<dd><span>';
				echo '<a href="javascript:showDossier(\'form'.$i.'\');">'._('Compléter un bon de commande').'</a><br>';
				
				echo '<form name="form'.$i.'" id="form'.$i.'" method="post" action="bondecommande.php" style="display:none;">';
				echo '<br><span>', $dic_veuillezprecisernumero, '<br><input type="hidden" name="accriss" value="ECMV"><input type="hidden" name="agency_id" value="'.$depaneur.'"><input type="text" name="num_dossier" value="" /><br><br />', $dic_veuillezpreciserjours, '<br><input type="text" name="num_jours" value="" /><br><br><input type="submit" value="', $dic_valider, '"</span>';
				echo '</form>';

				
				echo '</span></dd>';
			}else{
				echo '<dt><span>CCMV</span> - Opel Astra'.$dic_ouequivalent.'</dt>';
				echo '<dd><span>';
				echo '<a href="javascript:showDossier(\'form'.$i.'\');">'._('Compléter un bon de commande').'</a><br>';
				
				echo '<form name="form'.$i.'" id="form'.$i.'" method="post" action="bondecommande.php" style="display:none;">';
					echo '<br><span>', $dic_veuillezprecisernumero, '<br><input type="hidden" name="accriss" value="CCMV"><input type="hidden" name="agency_id" value="'.$depaneur.'"><input type="text" name="num_dossier" value="" /><br><br />', $dic_veuillezpreciserjours, '<br><input type="text" name="num_jours" value="" /><br><br><input type="submit" value="', $dic_valider, '"</span>';
				echo '</form>';
				
				echo '</span></dd>';
			}
			
			
			
			
			}
		}

	?>
	</dl>
	<?PHP
	if ($_SESSION['zeType'] == 'depanneur' || $_SESSION['zeType'] == 'admin') {
	?>
	<div id="addcar_box">
		<form name="addcar" id="addcar" method="post">
			<h3><?=_("Ajouter un véhicule")?></h3>
			<p><label for=""><?=_("Numéro d'immatriculation")?>:</label><br><input type="text" name="add_immatriculation" id="add_immatriculation" value="" style="text-transform: uppercase;" class="big" />  <a href="#" id="search_vehicle" class="btn big"><?=_("Chercher")?></a></p>
			
			
			
			<div id="results" style="display:none;">
			
				<p><strong><?=_("Le véhicule suivant a été trouvé:")?></strong></p>
				<p><input type="text" name="add_marque" id="add_marque" value="" readonly="readonly" class="big" /> <input type="text" name="add_model" id="add_model" value="" readonly="readonly" style="text-transform: capitalize;" class="big" /> <input type="hidden" name="add_acriss" id="add_acriss" value="" /> <input type="hidden" name="add_km" id="add_km" value="" /></p>
				<p>
					<input type="hidden" name="add_depanneur" id="add_depanneur" value="<?=$depaneur?>" />
					<a href="javascript:addCar(document.getElementById('addcar'));" class="btn big"><?=_("Ajouter ce véhicule")?></a>
				</p>
			
			</div>
			<div id="no_results" style="display:none;">
				<p><?=_("Désolé, ce véhicule n'existe pas ou plus.")?></p>
			</div>
			
		</form>
	</div>
	
	<script type="text/javascript">
	jQuery(function($){
	   $(".validateDate").mask("99/99/9999");
	});
	</script>
	
	<script type="text/javascript">
		$('#search_vehicle').on('click', function(e) {
			e.preventDefault();
			$.ajax({
				method: "GET",
				dataType: 'json',
				url: "/ajax/get_immat_vehicule_ec.php",
				data: { immat: $('#add_immatriculation').val() }
			})
			.done(function( data ) {
				
				if (!data["make_description"]) {
					// Véhicule n'existe pas chez Europcar
					$('#results').hide();
					$('#no_results').show();
				}else{
					// Véhicule trouvé en DB
					$('#no_results').hide();
					$('#results').show();
					$("#add_marque").val(data["make_description"]);
					$("#add_model").val(data["short_ctry_description"]);
					$("#add_acriss").val(data["sipp_code"]);
					$("#add_km").val(data["driven_mileage"]);
				}
				
			});
		});
		

	</script>
	<?PHP
	}
	} // end if $depIsOpen
	?>
	</div><!-- end div leftcol -->
	<div id="rightcol">
		<h2><?=$dic_cooordonnees?></h2>
		<p><img src="https://assist.europcar.be/openstreetmap/staticmap.php?center=<?=$strDep_lat?>,<?=$strDep_long?>&zoom=13&size=280x140&markers=<?=$strDep_lat?>,<?=$strDep_long?>,lightblue1"></p>
		<h3><?=$strDep_name?></h3>
		<p><?=$strDep_address?><br /><?=$strDep_cp?> <?=utf8_encode($strDep_city)?></p>
		<p>T: <?=$strDep_tel?><br />F: <?=$strDep_fax?><br />E: <a href="mailto:<?=$strDep_mail?>"><?=$strDep_mail?></a></p>
		<h4><?=$dic_horaires?></h4>
		<p><?=$dic_horaire_info?></p>
		<p><strong><?=$dic_horaires_lun?></strong><br /><i>
		<?PHP
		function displayHoraire($amstart, $amend, $pmstart, $pmend, $dic_horaires_ferme) {
			if ($amend == '11:59:59') {
				$amend = '12:00:00';
			}
			if ($pmend == '23:59:59') {
				$pmend = '24:00:00';
			}
			if($pmend == '08:00:02') {
				echo $dic_horaires_ferme;
			}else{
				$amstart = substr($amstart, 0, -3);
				$amend = substr($amend, 0, -3);
				$pmstart = substr($pmstart, 0, -3);
				$pmend = substr($pmend, 0, -3);
				
				if ($amend == $pmstart) {
					echo $amstart, ' - ', $pmend;
				#start update 16/04/2013 si le dépanneur ferme le matin ou l'aprem mais pas toute la journée
				}else{
					if ($amstart == $amend || $pmstart == $pmend) {
						if ($amstart != $amend) {
							echo $amstart, ' - ', $amend;
						}
						if ($pmstart != $pmend) {
							echo $pmstart, ' - ', $pmend;
						}
					#end update 16/04/2013
					}else{
						echo $amstart, ' - ', $amend, '<br />', $pmstart, ' - ', $pmend;
					}
				}
			}
		}
		displayHoraire($dep_mon_am_start, $dep_mon_am_end, $dep_mon_pm_start, $dep_mon_pm_end, $dic_horaires_ferme);
		?></i>
		<br /><strong><?=$dic_horaires_mar?></strong><br /><i><?PHP displayHoraire($dep_tue_am_start, $dep_tue_am_end, $dep_tue_pm_start, $dep_tue_pm_end, $dic_horaires_ferme); ?></i>
		<br /><strong><?=$dic_horaires_mer?></strong><br /><i><?PHP displayHoraire($dep_wed_am_start, $dep_wed_am_end, $dep_wed_pm_start, $dep_wed_pm_end, $dic_horaires_ferme); ?></i>
		<br /><strong><?=$dic_horaires_jeu?></strong><br /><i><?PHP displayHoraire($dep_thu_am_start, $dep_thu_am_end, $dep_thu_pm_start, $dep_thu_pm_end, $dic_horaires_ferme); ?></i>
		<br /><strong><?=$dic_horaires_ven?></strong><br /><i><?PHP displayHoraire($dep_fri_am_start, $dep_fri_am_end, $dep_fri_pm_start, $dep_fri_pm_end, $dic_horaires_ferme); ?></i>
		<br /><strong><?=$dic_horaires_sam?></strong><br /><i><?PHP displayHoraire($dep_sat_am_start, $dep_sat_am_end, $dep_sat_pm_start, $dep_sat_pm_end, $dic_horaires_ferme); ?></i>
		<br /><strong><?=$dic_horaires_dim?></strong><br /><i><?PHP displayHoraire($dep_sun_am_start, $dep_sun_am_end, $dep_sun_pm_start, $dep_sun_pm_end, $dic_horaires_ferme); ?></i></p>
	
	</div><!-- end div rightcol -->
<?PHP
};
?>
<p id="loadingMsg"></p>