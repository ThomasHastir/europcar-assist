<?PHP
session_name('SESSION1');
session_start();

session_destroy();
		
setcookie("rememberme", 0, time()-3600);
setcookie("login_usr", "", time()-3600);
setcookie("login_pwd", "", time()-3600);
setcookie("login_who", "", time()-3600);
setcookie("login_lg", "", time()-3600);

header('Location: /index.php');
?>